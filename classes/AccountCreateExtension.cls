public with sharing class AccountCreateExtension /*implements Database.AllowsCallouts*/{
    
//    public Account acct; 
//    public String acctid {get;set;}
//    public static final String DML_EXCEPTION_CODE = '1000';
//    public static final String QUERY_EXCEPTION_CODE = '1001';
//    public static final String CALLOUT_EXCEPTION_CODE = '1002';
//    public static final String FOUND_DUP_NOT_ALLOW_CREATE = '1011';
//    public boolean isIDTypeHasValue {get;set;}
//    public boolean isIDNumberHasValue {get;set;}
//    public boolean isFirstNameHasValue {get;set;}
//    public boolean isLastNameHasValue {get;set;}
//    public boolean isLastNameDisabled {get;set;}
//    public boolean isOfficeNummerHasValue {get;set;}
//    public boolean isMobileNumberHasValue {get;set;}
//    public boolean isCalloutAllow {get;set;}
//    public boolean isCreateSuccess {get;set;}
//    public boolean isIDValid {get;set;}
//    public boolean isSameOwner {get;set;}
//    public String oldIDType ;  
//    public String oldFirstName {get;set;}
//    public String oldLastName {get;set;}
//    private String accttype;
//    private String acctname;
//    private Type_of_ID__c typeid;
//    public string selectedCountry {get;set;}
//    public Map<String,String> CountryMap {get;set;}
//    public  List<SelectOption> CountriesOptionList {get;set;}
//    public  List<Country__c> CountriesList {get;set;}
//    private List<ID> SfIdList;
//    public Map<ID,Account> accountMap;
//    public boolean isHasPermission {get;set;}
//    //constructor
//    public AccountCreateExtension(ApexPages.StandardController controller) {
//        acct = (Account)controller.getRecord();
//        CountryMap = new Map<String,String>();
//        isHasPermission =Schema.sObjectType.Account.isCreateable();
//        CountriesOptionList = new List<SelectOption>();
//        CountriesOptionList.add(new SelectOption('','--None--'));
//        CountriesList = new List<Country__c>( [SELECT ID,Name,Code__c FROM Country__c WHERE Name != '' AND Code__c != null ORDER BY Name]);
//        for(Country__c coun : CountriesList){
//            CountriesOptionList.add(new SelectOption(coun.Code__c,coun.Name));
//            CountryMap.put(coun.Code__c, coun.Name);
//        }
//        acct.customer_type__c = ApexPages.currentPage().getParameters().get('customer_type');
//        //acct.Company_Name_Temp__c = ApexPages.currentPage().getParameters().get('company_name');
//        acct.First_Name__c = ApexPages.currentPage().getParameters().get('first_name');
//        acct.Last_Name__c = ApexPages.currentPage().getParameters().get('last_name');
//        acct.Mobile_Number_Temp__c = ApexPages.currentPage().getParameters().get('mobile_number');
//        acct.Office_Number_Temp__c = ApexPages.currentPage().getParameters().get('office_number');
//        acct.ID_Type_Temp__c = ApexPages.currentPage().getParameters().get('id_type');
//        acct.ID_Number_Temp__c = ApexPages.currentPage().getParameters().get('id_number');
//        oldIDType = ApexPages.currentPage().getParameters().get('id_type');
//        oldFirstName =ApexPages.currentPage().getParameters().get('first_name');
//        oldLastName = ApexPages.currentPage().getParameters().get('last_name');
//        selectedCountry = 'TH';    
//        isLastNameDisabled= false;
//        if(acct.customer_type__c=='Juristic'){
//            acct.ID_Type_Temp__c ='BRN ID';
//            isLastNameDisabled=true;
//        }
        
//        if(acct.ID_Type_Temp__c == null || acct.ID_Type_Temp__c == ''){
//            acct.ID_Type_Temp__c = '--None--';
//            isIDTypeHasValue = false; 
//        }else{
//            isIDTypeHasValue = true;
//        }
        
//        if(acct.ID_Number_Temp__c != null){
//            isIDNumberHasValue = true;
//        }else{
//            isIDNumberHasValue = false;
//        }
//        if(acct.First_Name__c != null){
//            isFirstNameHasValue = true;
//        }else{
//            isFirstNameHasValue = false;
//        }
//        if(acct.Last_Name__c != null){
//            isLastNameHasValue = true;
//        }else{
//            isLastNameHasValue = false;
//        }
//        if(acct.Office_Number_Temp__c != null){
//            isOfficeNummerHasValue = true;
//        }else{
//            isOfficeNummerHasValue = false;
//        }
//        if(acct.Mobile_Number_Temp__c != null){
//            isMobileNumberHasValue = true;
//        }else{
//            isMobileNumberHasValue = false;
//        }
        
//    } 
    
//    public boolean isDuplicate(){ 
//        boolean dup = false;
//        isSameOwner = false;
//        if((oldIDType==null)&&(oldIDType != acct.ID_Type_Temp__c) || oldFirstName!= acct.First_name__c || oldLastName != acct.Last_name__c){
//            TMBServiceProxy.TMBServiceProxySoap tmbService = new TMBServiceProxy.TMBServiceProxySoap();
//            tmbService.timeout_x = 120000 ;
//            // ktc add for rsa soap service call
//            tmbService = SoapRsa.setSecureHeader(tmbService);
//            accttype = acct.Customer_Type__c.substring(0,1);
//            if(acct.ID_Type_Temp__c !=null){
//                typeid = [SELECT Name,Value__c FROM Type_of_ID__c WHERE Name =:acct.ID_Type_Temp__c LIMIT 1];
//            }else{
//                typeid = [SELECT Name,Value__c FROM Type_of_ID__c WHERE Name ='--None--' LIMIT 1];
//            }
            
//            if(acct.Customer_Type__c =='Juristic'){
//                acctname = acct.First_name__c;
//            }
//            else if(acct.First_name__c != null && acct.Last_Name__c==null   ){
//                acctname = acct.First_name__c+' *';
//            }
//            else if(acct.First_name__c == null && acct.Last_Name__c!=null){
                
//                acctname ='* '+acct.Last_Name__c;
                
//            }
//            else{
//                acctname = acct.First_name__c+' '+acct.Last_Name__c;
//            }
            
//            System.debug('acctname: '+acctname);
//            System.debug('accttype: '+accttype);
//            System.debug('id type: '+typeid.Value__c);
//            System.debug('CUSID : '+acct.TMB_Customer_ID_Temp__c);
            
            
//            try{
//                /*
//TMBServiceProxy.SearchResultDTO searchResult = tmbService.Search_x(acctname,
//accttype,
//typeid.Value__c,
//(acct.ID_Number_Temp__c==null)?'':acct.ID_Number_Temp__c,
//(acct.Mobile_Number_Temp__c==null)?'':acct.Mobile_Number_Temp__c,
//(acct.TMB_Customer_ID_Temp__c==null)?'':acct.TMB_Customer_ID_Temp__c+'',
//(acct.Office_Number_Temp__c==null)?'':acct.Office_Number_Temp__c);

//*/
//                TMBServiceProxy.SearchResultDTO searchResult;
                
//                if(
//                    (acct.ID_Number_Temp__c != null ||acct.ID_Number_Temp__c !='') 
//                    && 
//                    (typeid.Value__c != null || typeid.Value__c != '')
//                )
//                {
//                    System.debug(':::: Search Only Id ::::');
//                    searchResult = tmbService.Search_x('',accttype,typeid.Value__c,acct.ID_number_Temp__c,'','','',  UserInfo.getUserId(), UserInfo.getUserName()); 
//                    // KTC Add
//                    System.debug(searchResult);
//                    if(searchResult.totalrecord != '0' && Test.isRunningTest() ==false)
//                        return true;
//                }
                
//                System.debug('ATYPE :'+acct.ID_Number_Temp__c);
//                System.debug('ANUM  :'+typeid.Value__c);
//                System.debug('AFirstName : '+oldFirstName+' : '+ acct.First_name__c);
//                System.debug('ALastName : '+oldLastName+' : '+ acct.Last_name__c);
//                if(acct.Last_name__c == null){
//                    acct.Last_name__c ='';
//                }
//                if(oldLastName == null){
//                    oldLastName ='';
//                }
                
                
//                if(((acct.ID_Number_Temp__c == null ||acct.ID_Number_Temp__c =='')
//                    && 
//                    (typeid.Value__c == null || typeid.Value__c == '')
//                    &&(oldFirstName!= acct.First_name__c || oldLastName != acct.Last_name__c))|| Test.isRunningTest()) {
                        
//                        System.debug(':::: Search Only Name ::::');
//                        System.debug('::::NAME:::::'+acctname+' OLD: '+oldFirstName+' '+acct.Last_name__c);
//                        System.debug('Search Value : '+acctname+' : '+accttype+' : '+acct.Office_Number_Temp__c+' : '+acct.Mobile_Number_Temp__c+' : '+UserInfo.getUserId()+' : '+UserInfo.getUserName());
                        
//                        searchResult = tmbService.Search_x(acctname,accttype,'','','','','',  UserInfo.getUserId(), UserInfo.getUserName());
                        
//                        System.debug('TOTAL : '+searchResult.totalrecord);
//                        if(searchResult.totalrecord != '0' || Test.isRunningTest()){
//                            TMBServiceProxy.ArrayOfSearchDataDTO arrayOfSearch = searchResult.Datas;
//                            System.debug('array Of Search'+arrayOfSearch);
//                            TMBServiceProxy.SearchDataDTO[] searchArr = arrayOfSearch.SearchDataDTO;
//                            System.debug('search Arr'+searchArr);
                            
//                            //if Datas return data(duplication occurs)
//                            if(searchArr!=null){
//                                SfIdList = new List<ID>();
//                                for(TMBServiceProxy.SearchDataDTO search : searchArr){
//                                    System.debug('ID : '+search.SF_ID);
//                                    SfIdList.add(search.SF_ID);                        
//                                }
                                
                                
//                                try{                   
//                                    accountMap = new Map<ID,Account>([SELECT First_name__c,Last_name__c,OwnerId,Owner.Name ,Owner.Phone , Owner.MobilePhone FROM Account WHERE ID IN:sfIDList]);                        
//                                }catch(QueryException E){
                                    
//                                    ApexPages.addmessage(ErrorHandler.Messagehandler(QUERY_EXCEPTION_CODE, Status_Code__c.GetValues('1001').Status_Message__c));
//                                    System.debug(logginglevel.ERROR,'Query Exception: '+e.getMessage());
//                                    return null;
//                                }
                                
//                                System.debug('SIZE : '+accountMap.size());
//                                for(TMBServiceProxy.SearchDataDTO search : searchArr){
//                                    if(accountMap.size()>0){
//                                        Account newAcct = accountMap.get(search.SF_ID);
//                                        if(newAcct!=null){
//                                            if(acct.Last_name__c == null){
//                                                acct.Last_name__c ='';
//                                            }
//                                            if(newAcct.Last_name__c == null){
//                                                newAcct.Last_name__c ='';
//                                            }
                                            
                                            
//                                            System.debug('Current User : '+UserInfo.getUserId());
//                                            System.debug('Account Owner : '+newAcct.OwnerId);
//                                            System.debug('Create FName :'+acct.First_name__c +' - '+ newAcct.First_name__c);
//                                            System.debug('Create LName :'+acct.Last_name__c +' - '+ newAcct.Last_name__c);   
                                            
//                                            if(UserInfo.getUserId()==newAcct.OwnerId
//                                               &&acct.Last_name__c==newAcct.Last_name__c
//                                               &&acct.First_name__c==newAcct.First_name__c){
//                                                   System.debug('Same Owner');
                                                   
//                                                   isSameOwner = true;
//                                                   return true;
//                                               }
//                                        }
//                                    }
//                                }
//                            } 
//                        } 
//                    }
                
//            }catch(CalloutException e){
//                ApexPages.addmessage(ErrorHandler.Messagehandler(CALLOUT_EXCEPTION_CODE, Status_Code__c.GetValues('1002').Status_Message__c));
//                System.debug(logginglevel.ERROR,'Callout Error: '+e.getMessage());
//            }
//        }
//        return dup;
//    }
    
//    public PageReference save(){ 
//        System.debug('ID Type : '+acct.ID_Type_Temp__c+' ID Number : '+acct.ID_Number_Temp__c);        
//        if(acct.ID_Number_Temp__c==''){
//            System.debug('Empty String');
//            acct.ID_Number_Temp__c=null;
//        }
//        if(acct.Mobile_Number_Temp__c ==''){
//            acct.Mobile_Number_Temp__c=null;
//        }
//        if(acct.Office_Number_Temp__c ==''){
//            acct.Office_Number_Temp__c=null;
//        }
        
//        if(acct.Customer_Type__c =='Juristic'){
//            if(acct.ID_Type_Temp__c==null&&(acct.ID_Number_Temp__c==null||acct.ID_Number_Temp__c=='')){
//                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3012').Status_Message__c));
//                return null;  
//            }else if (acct.ID_Number_Temp__c==null||acct.ID_Number_Temp__c==''){
//                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3004').Status_Message__c));
//                return null;  
//            }
//        }
        
//        if(acct.First_Name__c == null || acct.First_Name__c == ''){
//            isCalloutAllow = false;
//            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3003').Status_Message__c));
//            return null;
//        }
        
        
//        if(acct.ID_Type_Temp__c!=null&&acct.ID_Number_Temp__c==null){
//            isCalloutAllow = false;
//            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3004').Status_Message__c));
//            return null;
//        }
        
//        if(!isIDValid&&acct.ID_Type_Temp__c=='Citizen ID'){
//            isCalloutAllow = false;
//            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3001').Status_Message__c));
//            return null;
//        }
//        if(acct.Mobile_Number_Temp__c == null && acct.Office_Number_Temp__c == null){
//            isCalloutAllow = false;
//            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3013').Status_Message__c));
//            return null;
//        }
        
//        if(acct.Primary_Address_Line_1_Temp__c != null){
//            if(acct.Primary_Address_Line_1_Temp__c.length() > 40 ){
//                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Address No/Moo/Soi/Street '+ Status_Code__c.GetValues('3021').Status_Message__c));
//                return null;
                
//            } 
//        }
//        if(acct.Primary_Address_Line_2_Temp__c != null){                
//            if(acct.Primary_Address_Line_2_Temp__c.length() > 40 ){
//                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sub District '+ Status_Code__c.GetValues('3021').Status_Message__c));
//                return null;
                
//            }
//        }
//        if(acct.Primary_Address_Line_3_Temp__c != null){
//            if(acct.Primary_Address_Line_3_Temp__c.length() > 40 ){
//                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'District '+ Status_Code__c.GetValues('3021').Status_Message__c));
//                return null;
                
//            }
//        }
        
//        //System.debug('DUPLICATE RESULT :'+isDuplicate());
//        if(isDuplicate()){
//            if(isSameOwner){
//                isCalloutAllow = false;
//                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3008').Status_Message__c));                    
//                return null;
//            }else{
//                isCalloutAllow = false;
//                ApexPages.addmessage(ErrorHandler.Messagehandler(FOUND_DUP_NOT_ALLOW_CREATE, Status_Code__c.GetValues('3008').Status_Message__c));
//                return null;
//            }
//        }
        
//        if((acct.ID_Type_Temp__c == null ) && (acct.ID_Number_Temp__c != null)){
//            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3006').Status_Message__c));
//            return null;
//        }
//        //Populated
//        if((acct.ID_Type_Temp__c != null )&&(acct.ID_Number_Temp__c != null)){
//            acct.ID_populated__c = true;
//        }
//        if((acct.Primary_Address_Line_1_Temp__c != null && acct.Primary_Address_Line_1_Temp__c !='') 
//           && (acct.Province_Primary_Temp__c != null && acct.Province_Primary_Temp__c !='')
//           && (acct.Zip_Code_Primary_Temp__c != null && acct.Zip_Code_Primary_Temp__c !='')){
//               acct.Address_populate__c = true;
//           }
        
        
//        try{
//            //set account name N/A before insert
//            acct.Name ='N/A';
//            //  KTC Add Fax Temp to Fax Text In Salesforce
            
//            insert acct;
//            acctid = acct.id;
//            isCalloutAllow = true;
//        }catch(DMLException e){
//            isCalloutAllow = false;
//            ApexPages.addMessage(ErrorHandler.Messagehandler(DML_EXCEPTION_CODE, Status_Code__c.GetValues('1000').Status_Message__c));
//            System.debug(logginglevel.ERROR,'DML Exception: '+e.getMessage());
//            return null;
//        }
//        return null;
        
//    }
    
//    public PageReference insertAccountCallOut(){
//        System.debug('account object:'+acct);
//        isCreateSuccess = true;
//        Branch_and_Zone__c branch;
        
//        if(selectedCountry !=null){
//            acct.Country__c = selectedCountry;
//        }
//        if( acct.Branch_and_Zone__c != null){
//            branch = [SELECT ID,Name,Branch_Code__c 
//                      FROM Branch_and_Zone__c WHERE ID =:acct.Branch_and_Zone__c LIMIT 1];
//        }
//        String accttype = acct.Customer_Type__c.substring(0,1);
        
//        Account tmp_Account = [select Id,OwnerId,Owner.Name,Owner.Employee_ID__c,CreatedBy.Employee_ID__c FROM Account WHERE Id =: acct.Id LIMIT 1];
//        //User tmp_User = [select id,Name,Employee_ID__c FROM User WHERE ID =: Acct.OwnerId LIMIT 1];
        
        
//        System.Debug('Account Owner Id : '+tmp_Account.OwnerId );
//        System.Debug('Account Created By Id : '+tmp_Account.CreatedBy.Employee_ID__c );  
        
        
        
//        try{
//            if(acct.ID_Type_Temp__c !=null){
//                typeid = [SELECT Name,Value__c FROM Type_of_ID__c WHERE Name =:acct.ID_Type_Temp__c LIMIT 1];
//            }else{
//                typeid = [SELECT Name,Value__c FROM Type_of_ID__c WHERE Name ='--None--' LIMIT 1];
//            }
            
            
            
//            TMBServiceProxy.ProspectResultDTO Insertresult = null;
//            if(Test.isRunningTest() && acctid == ''){
//                Insertresult = new TMBServiceProxy.ProspectResultDTO(); 
//                InsertResult.status = '0000';
//                InsertResult.totalrecord = '1';
//                InsertResult.massage = '';
               
//            }
//            else{
                
//                TMBServiceProxy.TMBServiceProxySoap tmbService = new TMBServiceProxy.TMBServiceProxySoap(); 
//                tmbService.timeout_x = 120000;
//                // ktc add for rsa soap service call
//                tmbService = SoapRsa.setSecureHeader(tmbService);
//                Insertresult = tmbService.InsertPospect(acct.Id,
//                                                        (acct.First_Name__c==null)?'':acct.First_Name__c,
//                                                        (acct.Last_Name__c==null)?'':acct.Last_Name__c,
//                                                        (acct.ID_Type_Temp__c==null)?'':typeid.Value__c,
//                                                        (acct.ID_Number_Temp__c==null)?'':acct.ID_Number_Temp__c,
//                                                        //(acct.OwnerId==null)?'':acct.OwnerId,
//                                                        (tmp_Account.CreatedBy.Employee_ID__c==null)?'':tmp_Account.CreatedBy.Employee_ID__c,
//                                                        (acct.Customer_Type__c==null)?'':accttype,
//                                                        acct.Rating+'',
//                                                        (acct.Mobile_Number_Temp__c==null)?'':acct.Mobile_Number_Temp__c,
//                                                        (acct.Office_Number_Temp__c==null)?'':acct.Office_Number_Temp__c,
//                                                        (acct.Business_Type__c==null)?'':acct.Business_Type__c,
//                                                        (acct.No_of_years_business_run__c==null)?0+'':acct.No_of_years_business_run__c+'',
//                                                        (acct.Sales_amount_per_year__c==null)?0+'':acct.Sales_amount_per_year__c+'',
//                                                        (branch==null)?'':branch.Branch_Code__c,
//                                                        acct.Customer_Age__c+'',
//                                                        (acct.Group_Coverage__c==null)?'':acct.Group_Coverage__c,
//                                                        (acct.Primary_Address_Line_1_Temp__c==null)?'':acct.Primary_Address_Line_1_Temp__c,
//                                                        (acct.Primary_Address_Line_2_Temp__c==null)?'':acct.Primary_Address_Line_2_Temp__c,
//                                                        (acct.Primary_Address_Line_3_Temp__c==null)?'':acct.Primary_Address_Line_3_Temp__c,
//                                                        (acct.Province_Primary_Temp__c==null)?'':acct.Province_Primary_Temp__c,
//                                                        (acct.Zip_Code_Primary_Temp__c==null)?'':acct.Zip_Code_Primary_Temp__c,
//                                                        tmp_Account.CreatedBy.Employee_ID__c+'',
//                                                        (acct.Country__c==null)?'':acct.Country__c/*primaryCountry*/,
//                                                        (acct.M_C_Email__c==null)?'':acct.M_C_Email__c/*email*/,
//                                                        UserInfo.getUserId(), UserInfo.getUserName());
                
                
//            }
            
//            System.debug('Insertresult status : '+Insertresult.status);
//            System.debug('Insertresult total record : '+Insertresult.totalrecord);
//            System.debug('Insertresult message : '+Insertresult.massage);
            
//            if(Insertresult.status == '0000' ){
//                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO,Status_Code__c.GetValues('3010').Status_Message__c));
//                acct.ID_Number_Temp__c = null;
//                //acct.ID_Type_Temp__c = null;
//                acct.Name = 'Cust Info';
//                acct.Office_Number_Temp__c = null;
//                acct.Mobile_Number_Temp__c =null;
//                acct.Primary_Address_Line_1_Temp__c = null;
//                acct.Primary_Address_Line_2_Temp__c = null;
//                acct.Primary_Address_Line_3_Temp__c = null;
//                acct.Province_Primary_Temp__c = null;
//                acct.Zip_Code_Primary_Temp__c = null;  
//                acct.Country__c = null;
//                // KTC :  Add 2014-11-21
//                acct.Fax = acct.Fax_No_Temp__c;
//                update acct;
                
                
                
//            }else{
//                isCreateSuccess = false;
//                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,Insertresult.massage+'    '+acct));
//                delete acct;
//            }
//        }catch(CalloutException e){
            
//            system.Debug('CalloutException Cause : '+e);
//            Disqualified_Reason__c reason = Disqualified_Reason__c.getValues('Webservice_Timeout');         
//            ID rect = [SELECT id,Name FROM RecordType WHERE Name='Disqualified prospect' AND sObjectType='Account' LIMIT 1].id;
//            acct.RecordTypeId = rect;
//            acct.isDisqualified__c = true;
//            acct.Disqualified_Reason__c = reason.Value__c;    
//            update acct;
//            ApexPages.addmessage(ErrorHandler.Messagehandler(CALLOUT_EXCEPTION_CODE, Status_Code__c.GetValues('1002').Status_Message__c));
//            System.debug(logginglevel.ERROR,'Callout Error: '+e.getMessage());
//            return null; 
//        }catch(DMLException e){
//            ApexPages.addMessage(ErrorHandler.Messagehandler(DML_EXCEPTION_CODE, Status_Code__c.GetValues('1000').Status_Message__c));
//            System.debug(logginglevel.ERROR,'DML Exception: '+e.getMessage());
//            return null;
//        }
        
//        return null;
        
//    }
    
//    public PageReference viewAccount(){
//        PageReference accountPage = new ApexPages.StandardController(acct).view();
//        accountPage.setRedirect(true);
//        return accountPage;
//    }
    
}