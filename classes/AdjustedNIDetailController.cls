public class AdjustedNIDetailController {}
    /*Comment Cleansing Code private ApexPages.StandardController standardController;
    public String SEQ {get;set;}
    //public List<AdjustedWrapper> AdjustedWrapperList {get;set;}
    public Map<id,AcctPlanProdStrategy__c> mapProductStrategy {get;set;}
    public Map<id,AcctPlanProdStrategyPort__c> mapProductStrategyPort {get;set;}
    public String GroupID {get;set;}
    public Decimal Total {get;set;}
    public AcctPlanGroupProfile__c groupProfile {get;set;}
    /*******************************************************
        Start Pagination
    *******************************************************/
   /*Comment Cleansing Code  public class AdjustedWrapper{
        public String AccountId {get;set;}
        public String AccountName {get;set;}
        public Decimal AdjustNI {get;set;}
        public Boolean isAccountPlan {get;set;}
        public AcctPlanProdStrategy__c ProdStrategy {get;set;}
        public AcctPlanProdStrategyPort__c ProdStrategyPort {get;set;}
    }
    Public Integer noOfRecords{get; set;}
    Public Integer size {get;set;}
    public ApexPages.StandardSetController setCon {get{
        if(setCon ==null){
            size = 10; 
            setCon = new ApexPages.StandardSetController([SELECT ID,Name,Group__r.Name,Group__c
                                                          FROM Account
                                                          WHERE Group__c = :groupProfile.Group__c ]);
            setCon.setPageSize(size);
            noOfRecords = setCon.getResultSize();
        } 
        return setCon; 
        
    }set;} 
    
    /*Section 2 Wallet Sizing Item for Pagination*/
    
    /*Comment Cleansing Code public List<AdjustedWrapper> AdjustedWrapperList {
        get{ 
            AdjustedWrapperList = new List<AdjustedWrapper>();
            //AdjustedWrapperList.addAll((List<AdjustedWrapper>)setcon.getRecords());
            for (Account item : (List<Account>)setcon.getRecords()){
            AdjustedWrapper adjust = new AdjustedWrapper();
            /*adjust.ProdStrategy = item;
            adjust.isAccountPlan = true;
            AdjustedWrapperList.add(adjust); */
                //SObject temp = mapProductStrategy.get(item.Id);
                /*Comment Cleansing Code if(mapProductStrategy.containsKey(item.Id)){
                    adjust.ProdStrategy = mapProductStrategy.get(item.Id);
                    adjust.isAccountPlan = true;
                }else if(mapProductStrategyPort.containsKey(item.Id)){
                    adjust.ProdStrategyPort = mapProductStrategyPort.get(item.Id);
                    adjust.isAccountPlan = false;
                }
         AdjustedWrapperList.add(adjust);
            /*AdjustedWrapper adjust = new AdjustedWrapper();
            adjust.ProdStrategyPort = item;
            adjust.isAccountPlan = false;
            AdjustedWrapperList.add(adjust);*/
           /*Comment Cleansing Code  }
            return AdjustedWrapperList;
        }set;
    } 
    
    public Boolean hasNext {
        get {
            return setCon.getHasNext();
        }
        set;
    }
    public Boolean hasPrevious {
        get {
            return setCon.getHasPrevious();
        }
        set;
    }  
    public Integer pageNumber {
        get {
            return setCon.getPageNumber();
        }
        set;
    }  
    public void first() {
        setCon.first();
    }  
    public void last() {
        setCon.last();
    }  
    public void previous() {
        setCon.previous();
    }  
    public void next() {
        setCon.next();
    }
    /*******************************************************
        End Pagination
    *******************************************************/
    
    /*Comment Cleansing Code public AdjustedNIDetailController(ApexPages.StandardController controller){
        GroupID = ApexPages.currentPage().getParameters().get('GroupID');
        SEQ = ApexPages.currentPage().getParameters().get('SEQ');
        Decimal sequence = Decimal.valueOf(SEQ);
        
        groupProfile = [SELECT Id,Name,Year__c,Group__c,Group__r.Name  FROM AcctPlanGroupProfile__c WHERE Id = :GroupID];
        
        List<AccountPlanWalletSizing__c> SumWalletSizingList = [SELECT CashFee__c,CreditFee__c,DerivativeFee__c,Fee__c,
                                                                FXFee__c,Group__c,ASFee__c ,BAFee__c,
                                                                IBFee__c,Id,LGFee__c,Name,NIIc__c,NIId__c,
                                                                IsProductStrategy__c,NonRecurringFee__c,
                                                                RecurringFee__c,TFFee__c,Year__c,SummaryType__c
                                                                FROM AccountPlanWalletSizing__c
                                                                WHERE Group__c = :groupProfile.Group__c
                                                                AND Year__c = :groupProfile.Year__c
                                                                AND SummaryType__c  = 'AdjustedNI' LIMIT 1];
        Total = 0;
        if(SumWalletSizingList.size()>0){
            AccountPlanWalletSizing__c  item = SumWalletSizingList.get(0);
            if(SEQ == '1'){
                Total =  item.NIIc__c;
            }else if(SEQ == '2'){
                Total =  item.NIId__c;
            }else if(SEQ == '5'){
                Total =  item.TFFee__c;
            }else if(SEQ == '6'){
                Total =  item.FXFee__c;
            }else if(SEQ == '7'){
                Total =  item.LGFee__c;
            }else if(SEQ == '8'){
                Total =  item.CashFee__c;
            }else if(SEQ == '9'){
                Total =  item.ASFee__c;
            }else if(SEQ == '11'){
                Total =  item.CreditFee__c;
            }else if(SEQ == '12'){
                Total =  item.BAFee__c;
            }else if(SEQ == '13'){
                Total =  item.DerivativeFee__c;
            }else if(SEQ == '14'){
                Total =  item.IBFee__c;
            }
        }
        List<AcctPlanProdStrategy__c> listProductStrategy =[SELECT Id,SEQ__c,Name,Adjust_NI__c,AccountId__c, 
                                                            AcctPlanCompanyProfileID__r.AccountName__c,
                                                            AcctPlanCompanyProfileID__r.Account__c,
                                                            AcctPlanCompanyProfileID__r.Account__r.Name,
                                                            AcctPlanGroupProfileID__c
                                                            FROM AcctPlanProdStrategy__c  
                                                            WHERE AcctPlanCompanyProfileID__r.Year__c = :groupProfile.Year__c
                                                            AND AcctPlanGroupProfileID__c = :groupProfile.id
                                                            AND SEQ__c = :sequence];
        
        List<AcctPlanProdStrategyPort__c> listProductStrategyPort =[SELECT Id,SEQ__c,Name,AccountId__c,AccountName__c,
                                                                    AccountId_PE__c,Adjust_NI__c,GroupCompany__c
                                                                    FROM AcctPlanProdStrategyPort__c  
                                                                    WHERE Year__c = :groupProfile.Year__c 
                                                                    AND GroupCompany__c  = :groupProfile.Group__c
                                                                    AND SEQ__c = :sequence];
        
        List<AdjustedWrapper> AdjustedWrapperList = new  List<AdjustedWrapper>();
        mapProductStrategy = new Map<id,AcctPlanProdStrategy__c>();
        mapProductStrategyPort = new Map<id,AcctPlanProdStrategyPort__c>();

        for(AcctPlanProdStrategy__c item : listProductStrategy){
            /*AdjustedWrapper adjust = new AdjustedWrapper();
            adjust.ProdStrategy = item;
            adjust.isAccountPlan = true;
            AdjustedWrapperList.add(adjust); */
            /*Comment Cleansing Code mapProductStrategy.put(item.AcctPlanCompanyProfileID__r.Account__c,item);
            
        }
        for(AcctPlanProdStrategyPort__c item : listProductStrategyPort){
            /*AdjustedWrapper adjust = new AdjustedWrapper();
            adjust.ProdStrategyPort = item;
            adjust.isAccountPlan = false;
            AdjustedWrapperList.add(adjust);*/
            /*Comment Cleansing Code mapProductStrategyPort.put(item.AccountId__c,item);            
        }
         //setcon = new ApexPages.StandardSetController(AdjustedWrapperList);
         
    }
}*/