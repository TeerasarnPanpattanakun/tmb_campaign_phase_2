global without sharing  class CompanyProfileEmbedEx {
	public ApexPages.StandardController ctrl;
    public Account acct {get;set;}
    public String ownerId {get;set;}
    public List <AcctPlanCompanyProfile__c> companyList {get;set;} 
    public Account tmpAcct {get;set;}
    public List<UserRecordAccess> accessList {get;set;}
    public CompanyProfileEmbedEx(ApexPages.StandardController controller){
          ctrl = controller;
     	  acct= (Account)controller.getRecord();
        
        companyList = [SELECT ID,AccountName__c,Account__c,AcctPlanGroup__c,Name,AcctPlanGroup__r.Name,LastModifiedDate,
                       Year__c,Status__c,OwnerID,Owner.Name,LastModifiedByID,LastModifiedBy.name  FROM AcctPlanCompanyProfile__c
                      WHERE Account__c =:acct.id];

        tmpAcct = [select owner.UserRoleId from Account where id =: acct.ID]; 
        
        accessList = [SELECT RecordId, 
            HasAllAccess,HasEditAccess, HasReadAccess  
            FROM UserRecordAccess 
            WHERE UserId =: Userinfo.getUserId()
            AND RecordId =: tmpAcct.id
           ];

    }

    public boolean getCheckAuthorize()
    {
        boolean validate = false;
        boolean continueVal = true;
        boolean validAccess = false;

        if (accessList.size() > 0) {
            if (accessList.get(0).HasEditAccess) {
                validAccess = true;
            }
        }
        
        List<AccountTeamMember> acctTeam = [SELECT ID,AccountAccessLevel,AccountId,
                             IsDeleted, TeamMemberRole, UserId FROM  AccountTeamMember
                             WHERE AccountId =: tmpAcct.id
                             AND USerId=: Userinfo.getUserId()
                             LIMIT 1];
            
            if(acctTeam.size()>0){
                // if(acctTeam.get(0).AccountAccessLevel=='Edit' || acctTeam.get(0).AccountAccessLevel=='All'){
                        validAccess = true;
                  //  }
            }

        if (validAccess) {
            validate = true;
        } else {
            user u = [select UserRoleId from user where id =: UserInfo.getUserId()];
            string accOwnerRole = tmpAcct.owner.UserRoleId;
            string currentUserRole = '';
            if (u != null) currentUserRole = u.UserRoleId;

            if (currentUserRole != '' && currentUserRole != null)
            {
                while (continueVal)
                {
                    string accOwnerParentRole = '';
                    if (accOwnerRole != '' && accOwnerRole != null) {
                        UserRole ur = [select ParentRoleId from UserRole where id =: accOwnerRole];
                        if (ur != null) accOwnerParentRole = ur.ParentRoleId;
                    }
                    
                    if (accOwnerParentRole != '' && accOwnerParentRole != null) {
                        if (accOwnerParentRole != currentUserRole) {
                            accOwnerRole = accOwnerParentRole;
                        } else {
                            validate = true;
                            continueVal = false;
                        }
                    } else {
                        continueVal = false;
                    }
                }
            }
        }

        return validate;
    }
    
}