@isTest
global class ContactExtensionTest {}
/*

    static testmethod void WebservicemockupTest(){
        TestUtils.createIdType();
        TestUtils.createAppConfig();
        TestUtils.createStatusCode();
        TestUtils.createDisqualifiedReason();
        TestUtils.createDateOfBirth();
         Map<String,ID> addressmap = TestUtils.CreateAddress();
        
        Account acct = TestUtils.createAccounts(1,'webviceTESTACCOUNT','Individual', true).get(0);
        Contact cont = TestUtils.createContacts(1, acct.id, false).get(0);

        
        ApexPages.StandardController sc = new ApexPages.StandardController(cont);
        ContactExtension ConEX = new ContactExtension(sc);
         conEx.Provinceselected();
        conEx.DistrictSelected();
        ConEx.SubDistrictSelected();
                ConEx.selectedProvince = addressmap.get('Province');
        ConEx.selectedDistrict = addressmap.get('District');
        ConEx.selectedSubDistrict =  addressmap.get('Sub District');
        conEx.isIDValid = true;
        conEx.Provinceselected();
        conEx.DistrictSelected();
        Sub_District__c subdistrict = new Sub_District__c();
        subdistrict.Name = 'ห้วยขวาง';
        subdistrict.Zip_code__c ='10230';
        
        insert subdistrict;
         ConEx.selectedSubDistrict = subdistrict.id;
        System.debug('selectedSubDistrict : '+ConEx.selectedSubDistrict);
        ConEx.SubDistrictSelected();
        ConEx.selectedCountry='TH';
        ConEx.CheckCountry();
        ConEx.selectedCountry='EN';
        ConEx.CheckCountry();
        
        ContactExtension.calculateAge(Date.newInstance(1980, 2, 17));
        
        Test.setMock(WebServiceMock.class, new TMBServiceProxyMockImpl());
        Test.setMock(WebServiceMock.class, new TMBInsertContactMock());
        ConEX.save(); 
       
        Test.startTest();
        
        ConEx.viewContact();
        ConEx.insertContactCallout();
        Test.stopTest();
    }

    
    static testmethod void ExceptionTest(){
        TestUtils.createIdType();
        TestUtils.createAppConfig();
        TestUtils.createStatusCode();
        TestUtils.createDisqualifiedReason();
        TestUtils.createDateOfBirth();
        TestUtils.CreateAddress();
        Account acct = TestUtils.createAccounts(1,'exceptionTESTACCOUNT','Individual', true).get(0);
        Contact cont = TestUtils.createContacts(1, acct.id, false).get(0);
    
        ApexPages.StandardController sc = new ApexPages.StandardController(cont);
        ContactExtension ConEX = new ContactExtension(sc);
        conEx.isIDValid = true;
        Test.setMock(WebServiceMock.class, new TMBServiceProxyMockImpl());
        Test.setMock(WebServiceMock.class, new TMBInsertContactMock());
        
        ConEX.save();          
        Test.startTest();
        ConEx.viewContact();
        cont.id = null;
        ConEx.insertContactCallout();
        Test.stopTest();
    }
    
    
    static testmethod void CalloutExceptionTest(){
        Test.startTest();
        TestUtils.createIdType();
        TestUtils.createAppConfig();
        TestUtils.createStatusCodE();
        TestUtils.createDateOfBirth();
        TestUtils.createDisqualifiedReason();
        Account acct = TestUtils.createAccounts(1,'Negative2TESTACCOUNT','Individual', true).get(0);
        Contact cont = TestUtils.createContacts(1, acct.id, true).get(0);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(cont);
        ContactExtension ConEX = new ContactExtension(sc);
        conEx.isIDValid = true;
                Test.setMock(WebServiceMock.class, new TMBServiceProxyMockImpl());
                Test.setMock(WebServiceMock.class, new TMBQueryContactMock());
               
                ConEx.insertContactCallout();
        
                Test.setMock(WebServiceMock.class, new TMBServiceProxyMockImpl());
                Test.setMock(WebServiceMock.class, new TMBUpdateContactMock());
                ConEX.save();
                ConEx.viewContact();
                Test.stopTest();
    }
    
    
}*/