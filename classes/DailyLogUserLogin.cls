//Create by: Danudath Leebandit
//This class for set schedul to run daily at 23.55 pm
global class DailyLogUserLogin implements Schedulable{

    global void execute(SchedulableContext sc){        
        //UserLoginHistory loguser = new UserLoginHistory();
        UserLoginHistory_Delete logdelete = new UserLoginHistory_delete();
        UserLoginHistory_Insert loginsert = new UserLoginHistory_Insert();
        // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        // Set schedul run daily at 23.50,23.55 pm
        String sch = '00 50 23 * * ?';
        String sch2 = '00 55 23 * * ?';
        String jobID = System.schedule('LogUserHistoryloginDelete', sch, logdelete);
        String jobID2 = System.schedule('LogUserHistoryloginInsert', sch2, loginsert);
    }

}