global class IDMUserBatch implements Database.Batchable<sObject> ,
    Database.Stateful {
    /**
    * @author Keattisak Chinburarat
    * @date 2016-11-24
    * @description
    */
    global string batchname = 'IDMUserBatch';
    global string batchtype = 'HCM';
    global BatchHistory__c m_batchHistory;
    global String query;
    global List<String> fields = new List<String> {
        'FirstName',
        'LastName',
        'Email',
        'Employee_ID__c',
        'Manager_Employee_ID__c',
        'ProfileId',
        'IsActive',
        'Profile.Name'
    };
    global IDMUserBatch() {
        system.debug('=>4: Invoked IDMUserBatch ');
        
        query = 'Select Id ,' + String.join(fields, ',') + ' From User Where LastModifiedDate = TODAY AND Manager_Employee_ID__c != null AND IsActive = true';
        if(Test.isRunningTest()){
            query += ' LIMIT 200 '; // prevent batch run more than 1 loop
        }    
    }
    //Lazy Load Pattern
    global BatchHistory__c BatchHistory {
        get{
            if (null == m_batchHistory) {
                //  has record this day
                List<BatchHistory__c> hts = [Select Id, Name,LastModifiedDate From BatchHistory__c Where CreatedDate = today ]; 
                if (hts.size() > 0) {
                    m_batchHistory = hts[0];
                } else {
                    m_batchHistory = (BatchHistory__c)BatchHistory__c.sObjectType.newSObject(null, true);
                    m_batchHistory.Batch_Name__c = batchname;
                    m_batchHistory.Batch_Type__c = batchtype;
                    insert m_batchHistory;
                }
            }
            return m_batchHistory;
        }
    }
    global map<string, User> getManagerUser(List<sObject> dataset) {
        Map<string, User> mapOfManagerUserByEmployeeId = new Map<string, User>();
        //Find Manager by Employee Id
        Set<string> setManagerEmployeeCode = new Set<string>();
        for (User u : (List<User>)dataset) {
            if (String.isNotEmpty(u.Manager_Employee_ID__c)) {
                setManagerEmployeeCode.add(u.Manager_Employee_ID__c);
            }
        }
        //Flow 1): Setup Manager salesforce id to User object
        if (setManagerEmployeeCode.size() > 0) {
            // Make Manager data set
            for (List<User> users : [Select Id, Name, Email , Employee_ID__c , IsActive,Manager.IsActive,Manager_Employee_ID__c
                                     From User
                                     Where Employee_ID__c IN: setManagerEmployeeCode]) {
                for (User u : users) { 
                    if (!mapOfManagerUserByEmployeeId.containsKey(u.Employee_ID__c)) {
                        mapOfManagerUserByEmployeeId.put(u.Employee_ID__c, u);
                    }
                }
            }
        }

        return mapOfManagerUserByEmployeeId;
    }
    global BatchHistoryDetail__c createlog(BatchHistoryDetail__c log, User usr, string errorMessage, string status) {
        //3.  Do tmb want success log and error log ?
        //Yes, Prefer to get separate result both of success log and error log
        //Field : profile , Sfid (object user) , employeeID 5 digits , employeename , managerid 5 digits, status , error description.
        List<String> msgs = new List<String>();
        msgs.add(usr.Profile.Name);
        msgs.add(usr.Id);
        msgs.add(usr.Employee_ID__c);
        msgs.add(usr.FirstName + ' ' + usr.LastName);
        msgs.add(usr.Manager_Employee_ID__c);
        msgs.add(status);
        msgs.add(errorMessage);
        log.User__c = usr.Id;
        log.Status__c = status;
        log.Message__c = String.join(msgs, ',');
        log.MessageDetails__c = 'Profile: '+msgs[0]+' ,UserId: '+msgs[1]+' ,EmployeeId: '+msgs[2]+' ,UserName: '+msgs[3]+' ,ManagerId: '+msgs[4]+' ,Status: '+msgs[5]+' ,Message: '+msgs[6];
        log.Batch_History__c = BatchHistory.Id;
        return log;
    }
    /**
    * Batch Implementation
    */
    global Database.QueryLocator start(Database.BatchableContext BC) {

        return Database.getQueryLocator(query);
    }
        // Loop 200 records
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        //Apply SRC
        Map<string, User> mapOfManagerUserByEmployeeId = getManagerUser(scope);
        List<BatchHistoryDetail__c> idmlogs = new List<BatchHistoryDetail__c>();
        
        //Flow 1.1): Map Manager salesforce id to user
        if (mapOfManagerUserByEmployeeId.size() > 0) {
            List<User> listUserNeedToUpdateMangerId = new  List<User>();
            for (User u : (List<User>)scope) {
                boolean exp1 = String.isNotEmpty(u.Manager_Employee_ID__c);
                boolean exp2 = mapOfManagerUserByEmployeeId.containsKey(u.Manager_Employee_ID__c);
                System.debug('u.Manager_Employee_ID__c :'+u.Manager_Employee_ID__c);
                System.debug('manager is inactive or not? : '+((User)mapOfManagerUserByEmployeeId.get(u.Manager_Employee_ID__c)));
                if (exp1 && exp2 ) {
                    if(((User)mapOfManagerUserByEmployeeId.get(u.Manager_Employee_ID__c)).IsActive){
                        u.ManagerId = ((User)mapOfManagerUserByEmployeeId.get(u.Manager_Employee_ID__c)).Id;
                        u.DelegatedApproverId = ((User)mapOfManagerUserByEmployeeId.get(u.Manager_Employee_ID__c)).Id;
                        listUserNeedToUpdateMangerId.add(u);
                    }else{
                        BatchHistoryDetail__c log  = (BatchHistoryDetail__c)BatchHistoryDetail__c.sObjectType.newSObject(null, true);
                        log = createlog(log, u , statuscodeMap().get('HCM0002').Status_Message_EN__c, 'Fail'); //Manager is inactive
                        idmlogs.add(log);  
                    }  
                }else {
                    //No Manager
                    BatchHistoryDetail__c log  = (BatchHistoryDetail__c)BatchHistoryDetail__c.sObjectType.newSObject(null, true);
                    log = createlog(log, u , statuscodeMap().get('HCM0003').Status_Message_EN__c, 'Fail'); //  Can not find Manager for this user
                    idmlogs.add(log);
                }
            }
            if (listUserNeedToUpdateMangerId.size() > 0) {
                try {
                    // version 1 =>  update listUserNeedToUpdateMangerId;
                    // version 2 =>  Partial Insert
                    Database.SaveResult[] srList = Database.update(listUserNeedToUpdateMangerId, false);
                    User retUser = new User();
                    // Iterate through each returned result
                    List<string> logs = new list<string>();
                    for(integer i = 0; i < listUserNeedToUpdateMangerId.size() ; i++){
                    Database.SaveResult sr = srList[i];
                        
                        BatchHistoryDetail__c log  = (BatchHistoryDetail__c)BatchHistoryDetail__c.sObjectType.newSObject(null, true);
                        
                            retUser = (User)listUserNeedToUpdateMangerId[i];
                            if (!sr.isSuccess()) {
                                //Fail Case :
                                string error = '';
                                
                                for (Database.Error err : sr.getErrors()) {
                                    error = 'Fail to update cause : ' +
                                        'The following error has occurred.' + err.getStatusCode() + ': ' + err.getMessage() +
                                        'User fields that affected this error: ' + err.getFields();
                                }
                                log = createlog(log, retUser , error, 'Fail');
                            } else {
                                //Success Case :
                                log = createlog(log, retUser , statuscodeMap().get('HCM0001').Status_Message_EN__c, 'Success'); // Update Manager successfully
                            }
                            idmlogs.add(log);
                        //}
                    
                    }
                    //Insert Log Details
                } catch (Exception ex) {
                }
            }
        }
        //>End Flow 1.1): Map Manager salesforce id to user
        else {
            // No Manager for all user
            for (User u : (List<User>)scope) {
                BatchHistoryDetail__c log  = (BatchHistoryDetail__c)BatchHistoryDetail__c.sObjectType.newSObject(null, true);
                log = createlog(log, u , statuscodeMap().get('HCM0003').Status_Message_EN__c, 'Fail'); //Can not find Manager for this user
                idmlogs.add(log);
            }
        }
        insert idmlogs;
        system.debug('## idmlogs : '+idmlogs);
        
        // get userId that had created log already
        List<Id> userinlogs = new List<Id>();
        List<User> clearManagerIdTemp = new List<User>();
        for(BatchHistoryDetail__c batchlogs : idmlogs){
            userinlogs.add(batchlogs.User__c);
        }
        
        // find user for clear Manager_Employee__ID__c to Blank and update
        List<User> userToclear = new List<User>([SELECT Id, Manager_Employee_ID__c FROM User WHERE Id IN: userinlogs]);
        if(userToclear.size() > 0){
            for(User clearMngTemp: userToclear){
                clearMngTemp.Manager_Employee_ID__c = '';
                clearManagerIdTemp.add(clearMngTemp);
            }
            update clearManagerIdTemp;
        }

    }
    global void finish(Database.BatchableContext BC) {
        
        //1) Send mail
        
        try {
        
            list<string> ListIdUser = new list<string>();
            list<string> ListIdNewUser = new list<string>();
            list<string> ListIdMovementUser = new list<string>();
            list<BatchHistoryDetail__c> ListBatchDetail = new list<BatchHistoryDetail__c>();
    
            for(BatchHistoryDetail__c b : [select id,Batch_History__c,User__c,IsSendMail__c,Status__c from BatchHistoryDetail__c 
                                           where createddate = TODAY and Status__c = 'Success' and IsSendMail__c = false])
            {
                ListIdUser.add(b.User__c);
                ListBatchDetail.add(b);
            } 
            
            for(user u: [Select Id From User Where LastModifiedDate = TODAY and CreatedDate = TODAY and id in: ListIdUser])
            {
                ListIdNewUser.add(u.id);
            }
            for(user u: [Select Id From User Where LastModifiedDate = TODAY and CreatedDate != TODAY and id in: ListIdUser])
            {
                ListIdMovementUser.add(u.Id);
            }
            
            IDMUserEmailServices callMail = new IDMUserEmailServices();
        
            if (ListIdNewUser.size() > 0)
                callMail.sendMailToUser(ListIdNewUser, 'HCM:Email Template for New User');
            if (ListIdMovementUser.size() > 0)
                callMail.sendMailToUser(ListIdMovementUser, 'HCM:Email Template for User Movement');
            
            for (BatchHistoryDetail__c b : ListBatchDetail) 
            {
                b.IsSendMail__c = true;
            }
            
            update ListBatchDetail;
        
        }catch(exception ex){
            system.debug('Line - '+ex.getLineNumber()+' Error - '+ex.getMessage());
        }
        // Send mail result
        IDMUserEmailServices callMail = new IDMUserEmailServices();
        List<BatchHistory__c> bht = new List<BatchHistory__c>([SELECT Id,LastModifiedDate FROM BatchHistory__c WHERE CreatedDate = TODAY]);
        List<String> endTimeList = new List<String>();
        if(AppConfig__c.getValues('HCM_BatchEndTime').Value__c != null){
            endTimeList = (AppConfig__c.getValues('HCM_BatchEndTime').Value__c).split(':');
            Integer hour = Integer.valueOf(endTimeList[0]);
            Integer minute = Integer.valueOf(endTimeList[1]);
            Time endTime = Time.newInstance(hour, minute, 0, 0);
            if(bht.size() > 0){
                  if(bht[0].LastModifiedDate.time() > endTime || System.now().time() > endTime){
                    callMail.sendMailResult(); 
                }  
            }       
        }else{
            System.debug('## HCM_BatchEndTime is Blank');
        }
        //2) purge log
        PurgeBatchHistoryServices purgeService = new PurgeBatchHistoryServices();
        purgeService.Purgelog();
    }
    
        public static Map<String,Host_Status_Code__mdt> statuscodeMap(){
            Map<String,Host_Status_Code__mdt> statuscodeMap = new Map<String,Host_Status_Code__mdt>();
            for(Host_Status_Code__mdt statuscode : [SELECT Id,DeveloperName,Status_Message_EN__c,Status_Message_TH__c 
                                                     FROM Host_Status_Code__mdt 
                                                     WHERE Host_System__c =: 'HCM']){
                statuscodeMap.put(statuscode.DeveloperName, statuscode);
            }
            return statuscodeMap;
        }
    
    /**
    * Batch Exception
    */
    public class IDMUserBatchException extends Exception {}
}