@isTest

public class IDMUserBatch_Test {
    public static final Id SYSADMIN_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1].Id;
    public static String sch1 = '0 0 10,15,20 * * ?';  
    public static String jobID = null;
    static{
        getCustomSetting();
    }
    public static testmethod void PositiveTest(){
        //create user
        List<User> userList = new List<User>();
        system.debug('## userList : '+userList);
        userList = TestUtils.createUsers(10, 'testfirstname', 'testlastname', 'test@tmbbank.com', true);
        userList[0].Manager_Employee_ID__c = 'Test1';
        userList[1].Manager_Employee_ID__c = 'MM002';
        userList[2].Manager_Employee_ID__c = '';
        userList[3].Manager_Employee_ID__c = 'ZZZZZ';
        update userList;
        try{
            
            TEST.startTest();
            //run schedule and batch
            UserBatchSchedulable batch = new UserBatchSchedulable();
            jobID = system.schedule('testBatch1', sch1, batch);
            TEST.stopTest();

        }catch(UnexpectedException uex){
            system.assertEquals('No more than one executeBatch can be called from within a test method.  Please make sure the iterable returned from your start method matches the batch size, resulting in one executeBatch invocation.',uex.getMessage());
        }
        System.abortJob(jobID);
    }

    public static testmethod void NegativeTest(){
        List<User> userList = new List<User>();
        userList = TestUtils.createUsers(10, 'testfirstname', 'testlastname', 'test@tmbbank.com', true);
        userList[9].Manager_Employee_ID__c = 'MM010';
        update userList;
        String query = '';
        try{
            
            TEST.startTest();
            //run schedule and batch
            UserBatchSchedulable batch = new UserBatchSchedulable();
            jobID = system.schedule('testBatch2', sch1, batch);
            TEST.stopTest();
            
        }catch(UnexpectedException uex){
            System.assertEquals('No more than one executeBatch can be called from within a testmethod. Please make sure the iterable returned from ' +
                                'your start method matches the batch size, resulting in one executeBatch invocation.', uex.getMessage());
        }
        System.abortJob(jobID);
        
    }
    
    public static testmethod void bulkTest(){
        AggregateResult[]  buserCount =  [Select Count(ID) total From User where Employee_ID__c != null];        
        if(null != buserCount){
            // System will see all user in db
            system.debug('=> Existing User '+ ((integer)buserCount[0].get('total')) +' Items '); 
        }
        List<User> managerList = new List<User>();
        managerList = TestUtils.createUsers(5, 'testfirstname', 'testlastname', 'test@tmbbank.com', true);
        
        List<User> userList = new List<User>();
        for(Integer i = 100;i<299;i++){
            User newUser = new User(FirstName = 'test'+i , LastName = 'test'+i, 
                                    UserName='test'+i+'@tmbbank.com',
                                    Email='test'+i+'@tmbbank.com', Alias='T'+i,
                                    LocaleSidKey='en_US', LanguageLocaleKey='en_US', EmailEncodingKey='ISO-8859-1', 
                                    CommunityNickname='test'+i+'_'+'TMBTesttmb'+i,
                                    ProfileId = SYSADMIN_PROFILE_ID, TimeZoneSidKey='America/New_York',isActive = true,
                                    Employee_ID__c ='MM'+i,Manager_Employee_ID__c = 'MM000');  
            userList.add(newUser);
        }  
        
        try{            
            insert userList; 
            AggregateResult[]  userCount =  [Select Count(ID) total From User where Employee_ID__c != null ];        
            if(null != userCount){
                // System will see all user in db
                system.debug('=> User After Insert 200 Users is  '+ ((integer)userCount[0].get('total')) +' Items '); 
            } 
            
            TEST.startTest();
            system.debug('## userList size '+userList.size());
            // run schedule and batch
            UserBatchSchedulable batch = new UserBatchSchedulable();
            jobID = system.schedule('testBatch3', sch1, batch);
            TEST.stopTest(); 
            
        }catch(UnexpectedException uex){
            system.assertEquals('No more than one executeBatch can be called from within a test method.  Please make sure the iterable returned from your start method matches the batch size, resulting in one executeBatch invocation.',uex.getMessage());
        }
        System.abortJob(jobID);
    }
    
    public static testmethod void BatchUserBatchDispatcherExp(){
        Boolean isError = false; 
        // isError = fasle;
        try{
            UserBatchDispatcher.emode =1 ; //force error case
            UserBatchDispatcher.execute();
        }
         catch(UserBatchDispatcher.UserBatchDispatcherException ex){
            isError = true;
        }
        system.assert(isError);
    }
 
    /*** Create User Test Data ***/
    public static void createUser(Integer size,String managerID,Id profileId){
        
        List<User> userList = new List<User>();
        for(Integer i = 100;i<(100+size);i++){
            User newUser = new User(FirstName = 'test'+i , LastName = 'test'+i, 
                                    UserName='test'+i+'@tmbbank.com',
                                    Email='test'+i+'@tmbbank.com', Alias='T'+i,
                                    LocaleSidKey='en_US', LanguageLocaleKey='en_US', EmailEncodingKey='ISO-8859-1', 
                                    CommunityNickname='test'+i+'_'+'TMBTesttmb'+i,
                                    ProfileId = SYSADMIN_PROFILE_ID, TimeZoneSidKey='America/New_York',isActive = true,
                                    Employee_ID__c ='MM'+i,Manager_Employee_ID__c = managerID);
            userList.add(newUser);
        }
        insert userList;
    }
    
    
    /*** Get Custom Setting for HCM Project from AppConfig__c and  HCM&SLOS Messages ***/
    public static void getCustomSetting(){
        List<AppConfig__c> appconfigList = new List<AppConfig__c>();
        AppConfig__c HCM_CRM = new AppConfig__c(Name = 'HCM_CRM',Value__c = 'test1@ii.co.th ; test2@ii.co.th');
        AppConfig__c HCM_ITG = new AppConfig__c(Name = 'HCM_ITG',Value__c = 'test3@ii.co.th;test4@ii.co.th');
        AppConfig__c HCM_OperationTeam = new AppConfig__c(Name = 'HCM_OperationTeam',Value__c = 'test5@ii.co.th');
        AppConfig__c Days_of_Delete_BatchHistory = new AppConfig__c(Name = 'Days_of_Delete_BatchHistory',Value__c = '90');
        AppConfig__c HCM_BatchEndTime = new AppConfig__c(Name = 'HCM_BatchEndTime',Value__c = String.valueOf(System.now().time()));
        appconfigList.add(HCM_CRM);
        appconfigList.add(HCM_ITG);
        appconfigList.add(HCM_OperationTeam);
        appconfigList.add(Days_of_Delete_BatchHistory);
        appconfigList.add(HCM_BatchEndTime);
        insert appconfigList;
        
        List<Host_Status_Code__mdt> statuscode = new List<Host_Status_Code__mdt>([SELECT Id,DeveloperName,Status_Message_EN__c,Status_Message_TH__c 
                                                     FROM Host_Status_Code__mdt 
                                                     WHERE Host_System__c =: 'HCM']);
    }
}