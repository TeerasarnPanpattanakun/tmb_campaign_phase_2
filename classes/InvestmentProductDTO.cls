global class InvestmentProductDTO {
	/*----------------------------------------------------------------------------------
	  Author:        Keattisak Chinburarat
	  Company:       I&I Consulting 
	  Description:   Data Transfer Objects
					 Investment Accounts: Multiple Blocks of Data Fields No. 33 - 41
	  Inputs:        None
	  Base Class:    -
	  Test Class:    -
	  History
	  <Date>      <Authors Name>     <Brief Description of Change>
	  ----------------------------------------------------------------------------------*/
	global string UnitHolderNo { get; set; }
	global string SubProductGroup { get; set; }
	global string ProductName { get; set; }
    global string SeqGrp {get;set;}
	global string AssetClass { get; set; }
	global string FundCode { get; set; }
	global string IssuerFundHouse { get; set; }
	global Date OpenedDate { get; set; }
	global Decimal Units { get; set; }
	global Decimal InitialValue { get; set; }
	global Decimal MarketValue { get; set; }
	global Decimal UnrealizedGL { get; set; }
    global Decimal NumberOfUnit { get; set; }
	global Decimal CostOfInvestment { get; set; }
	global string Params {
		get {

			string p = UnitHolderNo + '&' + FundCode;
			return UrlHelper.encryptParams(p);

		}
	}
	global String convertedOpenedDate{
        get{
            return NumberHelper.DateFormat(OpenedDate);
        }
    }

	// Error Handler message
	global String SoapStatus { get; set; }
	global String SoapMessage { get; set; }
}