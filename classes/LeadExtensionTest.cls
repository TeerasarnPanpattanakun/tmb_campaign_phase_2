@isTest
public class LeadExtensionTest {
/*
    static{
        TestUtils.createAppConfig();
        TestUtils.createStatusCode(); 
        TestUtils.createDisqualifiedReason(); 
        TestUtils.createObjUserPermission();
        TestInit.createCustomSettingTriggerMsg();     
     	Sub_Purpose_Mapping__c subpurpose = new Sub_Purpose_Mapping__c();

     	subpurpose.Main_purpose__c = 'CB';
     	subpurpose.Sub_Purpose__c = 'Sell product';
     	subpurpose.Name = 'Subpurpose1';
     insert subpurpose;
        
    }
    
    
     public static testmethod void LeadCreation(){
        Lead leadRec = new Lead();
        leadRec.Phone = '0877874871';
        leadRec.Company = 'LeadExtension';
        leadRec.ID_Type__c = 'Passport ID';
        leadRec.ID_Number__c = '13255555';
        leadRec.Mobile_No__c = '053532198';
        leadRec.Customer_Type__c = 'Individual';
        leadRec.FirstName =  'LeadExFirst';
        leadRec.LastName = 'LeadExLast';
        
        
         
         
         PageReference leadCreation = Page.LeadCreation;
        Test.setCurrentPage(leadCreation);
         
         
       	ApexPages.StandardController controllerinsert = new ApexPages.StandardController(leadRec);
            LeadExtension LeadExten = new LeadExtension(controllerinsert);
         	LeadExten.leadObj = Leadrec;
			
        	LeadExten.doAction();
			LeadExten.doActionMobile();
    }
    
     
     public static testmethod void LeadUpdate(){
        Lead leadRec = new Lead();
        leadRec.Phone = '0877874871';
        leadRec.Company = 'LeadExtension';
        leadRec.ID_Type__c = 'Passport ID';
        leadRec.ID_Number__c = '13255555';
        leadRec.Mobile_No__c = '053532198';
        leadRec.Customer_Type__c = 'Individual';
        leadRec.FirstName =  'LeadExFirst';
        leadRec.LastName = 'LeadExLast';
        
        
         insert leadRec;
         
          LisT<Account> acct =  TestUtils.createAccounts(2,'testDisqualified','Individual', false);
            Account firstAcct = acct.get(0);
            firstAcct.ID_Type_PE__c = 'Passport ID';
            firstAcct.ID_Number_PE__c = '13255555';
            firstAcct.Phone = '0877874871';
            insert firstacct;
             Id [] fixedSearchResults= new Id[1];
           fixedSearchResults[0] = firstacct.id;
           Test.setFixedSearchResults(fixedSearchResults);
            
         
         
         
         PageReference leadCreation = Page.LeadCreation;
        Test.setCurrentPage(leadCreation);
         
         
       	ApexPages.StandardController controllerinsert = new ApexPages.StandardController(leadRec);
            LeadExtension LeadExten = new LeadExtension(controllerinsert);
         	LeadExten.leadobj = Leadrec;
         	leadExten.beforeChangeLead.ID_Type__c =null;
         	leadExten.beforeChangeLead.ID_Number__c =null;
        	LeadExten.doAction();
         	LeadExten.doActionMobile();
         	LeadExten.viewDetail();
    }

    
     public static testmethod void LeadDuplicated(){
        Lead leadRec = new Lead();
        leadRec.Phone = '0877874871';
        leadRec.Company = 'LeadExtension';
        leadRec.ID_Type__c = 'Passport ID';
        leadRec.ID_Number__c = '13255555';
        leadRec.Mobile_No__c = '053532198';
        leadRec.Customer_Type__c = 'Individual';
        leadRec.FirstName =  'LeadExFirst';
        leadRec.LastName = 'LeadExLast';
        
        
         insert leadRec;
           LisT<Account> acct =  TestUtils.createAccounts(1,'LeadCommercial','Individual', false);
            Account firstAcct = acct.get(0);
            firstacct.Name='TestExtensionTest';
            firstacct.ID_Number_PE__c ='13255555';
            firstacct.ID_Type_PE__c ='Passport ID';
         	firstacct.phone ='053234123';
         	firstacct.Mobile_Number_PE__c = '0877874871';
         	firstacct.OwnerID = Userinfo.getUserId();
           // insert firstacct;
         
         
         
         Lead OriginalleadRec = new Lead();
        OriginalleadRec.Phone = '0877874871';
        OriginalleadRec.Company = 'LeadExtension';
        //OriginalleadRec.Account__c = firstacct.id;
        OriginalleadRec.ID_Type__c = 'Passport ID';
        OriginalleadRec.ID_Number__c = '13255555';
        OriginalleadRec.Mobile_No__c = '053532198';
        OriginalleadRec.Customer_Type__c = 'Individual';
        OriginalleadRec.FirstName =  'LeadExFirst';
        OriginalleadRec.LastName = 'LeadExLast';
            insert OriginalleadRec;
             Id [] fixedSearchResults= new Id[1];
           fixedSearchResults[0] = OriginalleadRec.id;
           Test.setFixedSearchResults(fixedSearchResults);
            
         
         
         
         PageReference leadCreation = Page.LeadCreation;
        Test.setCurrentPage(leadCreation);
         
         
       	ApexPages.StandardController controllerinsert = new ApexPages.StandardController(leadRec);
            LeadExtension LeadExten = new LeadExtension(controllerinsert);
         	LeadExten.leadobj = leadRec;
         	leadExten.beforeChangeLead.ID_Type__c =null;
         	leadExten.beforeChangeLead.ID_Number__c =null;
        	LeadExten.doAction();
         	LeadExten.doActionMobile();
         	LeadExten.viewDetail();
    }

    
    public static testmethod void LeadCommercialAccount(){
         try{
             
             LisT<Account> acct =  TestUtils.createAccounts(1,'LeadCommercial','Individual', false);
            Account firstAcct = acct.get(0);
            firstacct.Name='TestExtensionTest';
            firstacct.ID_Number_PE__c ='13255555';
            firstacct.ID_Type_PE__c ='Passport ID';
            insert firstacct;
             
             
             Lead leadRec = new Lead();
        leadRec.Phone = 'Testmobileerror';
        leadRec.Company = 'LeadExtension';
        leadRec.ID_Type__c = 'Passport ID';
        leadRec.ID_Number__c = '13255555';
        leadRec.Mobile_No__c = '23423423';
        leadRec.Customer_Type__c = 'Individual';
        leadRec.FirstName =  'LeadExFirst';
        leadRec.LastName = 'LeadExLast';
        leadRec.Account__c = firstacct.id;
         insert leadRec;
             
             
             Id [] fixedSearchResults= new Id[1];
           fixedSearchResults[0] = leadRec.id;
           Test.setFixedSearchResults(fixedSearchResults);
             
         
         PageReference leadCreation = Page.LeadCreation;
        Test.setCurrentPage(leadCreation);
         
         
       	ApexPages.StandardController controllerinsert = new ApexPages.StandardController(leadRec);
            LeadExtension LeadExten = new LeadExtension(controllerinsert);
         	LeadExten.leadobj = Leadrec;
         	leadExten.beforeChangeLead.ID_Type__c =null;
         	leadExten.beforeChangeLead.ID_Number__c =null;
        	LeadExten.doAction();     
             
         }catch(Exception e){
             System.debug(e.getMessage());
         }
        
         	
    }
    
    
    
     public static testmethod void LeadCommercialAccountMobile(){
         try{
             
             LisT<Account> acct =  TestUtils.createAccounts(1,'LeadCommercial','Individual', false);
            Account firstAcct = acct.get(0);
            firstacct.Name='TestExtensionTest';
            firstacct.ID_Number_PE__c ='13255555';
            firstacct.ID_Type_PE__c ='Passport ID';
            insert firstacct;
             
             
             Lead leadRec = new Lead();
        leadRec.Phone = 'Testmobileerror';
        leadRec.Company = 'LeadExtension';
        leadRec.ID_Type__c = 'Passport ID';
        leadRec.ID_Number__c = '13255555';
        leadRec.Mobile_No__c = '23423423';
        leadRec.Customer_Type__c = 'Individual';
        leadRec.FirstName =  'LeadExFirst';
        leadRec.LastName = 'LeadExLast';
        leadRec.Account__c = firstacct.id;
         insert leadRec;
             
             
             Id [] fixedSearchResults= new Id[1];
           fixedSearchResults[0] = leadRec.id;
           Test.setFixedSearchResults(fixedSearchResults);
             
         
         PageReference leadCreation = Page.LeadCreation;
        Test.setCurrentPage(leadCreation);
         
             
              	ApexPages.StandardController controllerinsertMobile = new ApexPages.StandardController(leadRec);
            LeadExtension LeadExten2 = new LeadExtension(controllerinsertMobile);
         	LeadExten2.leadobj = Leadrec;
         	LeadExten2.beforeChangeLead.ID_Type__c =null;
         	LeadExten2.beforeChangeLead.ID_Number__c =null;
			LeadExten2.doActionMobile();             
             
         }catch(Exception e){
             System.debug(e.getMessage());
         }
        
         	
    }
*/
    
}