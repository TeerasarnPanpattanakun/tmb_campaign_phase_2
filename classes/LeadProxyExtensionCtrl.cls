public class LeadProxyExtensionCtrl {    
    private final ApexPages.StandardController std ;
    private final Lead Leadrec;
    private RecordType recTypes;
    private string recordTypeId;
    private string referralId;
    public boolean isCommercial;

        
    public LeadProxyExtensionCtrl(ApexPages.StandardController controller){
        std = controller;  
        isCommercial = false;
        Leadrec = (Lead)std.getRecord();
        recordTypeId = ApexPages.currentPage().getParameters().get('RecordType');  
        referralId = ApexPages.currentPage().getParameters().get('referralId'); 

        
        // Get Record TypeID and Name
       
    }
    public PageReference redirectPage(){         
        system.debug(':::: Lead Id  = ' + Lead.Id);
        string url ='/';
        
        //if is Edit Mode
         if(LeadRec.id!=null){
              recTypes = [select Id, Name, sobjectType,DeveloperName from RecordType   
                    where Id  =:Leadrec.RecordTypeId
                   LIMIT 1];
             
             //Commercial
             //
             //
             if(recTypes.DeveloperName.containsIgnoreCase('Commercial')){
                 url = '/apex/LeadCreation?id='+ Leadrec.Id; 
                 if(referralId != null) url += '&referralId='+referralId;
                 isCommercial =true;
             }
             
             //Retail
             if(recTypes.DeveloperName.containsIgnoreCase('Retail')){
                 url = '/apex/RetailLeadEdit?id='+ Leadrec.Id; 
                 if(referralId != null) url += '&referralId='+referralId;
             }
    
         }else{ // Create Mode
            Schema.DescribeSObjectResult dsr = Lead.SObjectType.getDescribe();
            Schema.RecordTypeInfo defaultRecordType;
            
             for(Schema.RecordTypeInfo rti : dsr.getRecordTypeInfos()) {
                if(rti.isDefaultRecordTypeMapping()) {
                    defaultRecordType = rti;
                }
            }
            
             if(null == recordTypeId){
                 recTypes = [select Id, Name, sobjectType,DeveloperName from RecordType   
                         where Id  =:defaultRecordType.getRecordTypeId()
                         LIMIT 1];
             }else{
                 recTypes = [select Id, Name, sobjectType,DeveloperName from RecordType   
                         where Id  =:recordTypeId
                         LIMIT 1];
             }
             
             //Commercial 
             if(recTypes.DeveloperName.containsIgnoreCase('Commercial')){
                url = '/apex/LeadCreation'; 
                if(referralId != null) url += '?referralId='+referralId;
                isCommercial =true;
             }
             
             //Retail
             if(recTypes.DeveloperName.containsIgnoreCase('Retail')){
                url = '/apex/RetailLeadCreation?RecordType='+recTypes.Id;
                if(referralId != null) url += '&referralId='+referralId;
             }
                          
        }   
        
        System.debug('URL : '+url);
        if(isCommercial){ 
            PageReference page = new PageReference(url); 
            page.setRedirect(true); 
            return page; 
        }else{
            PageReference page = new PageReference(url); 
            page.setRedirect(true); 
            return page; 
        }
    }

    
 public PageReference redirectMobile(){         
        system.debug(':::: Lead Id  = ' + LeadRec.Id);
         
        string url ='/';
        //Edit Mode
         if(LeadRec.id !=null){
              recTypes = [select Id, Name, sobjectType,DeveloperName from RecordType   
                    where Id  =:Leadrec.RecordTypeId
                   LIMIT 1];
             
             //Commercial
             //
             //
             if(recTypes.DeveloperName.containsIgnoreCase('Commercial')){
                 url = '/apex/LeadCreation?id='+ Leadrec.Id;
                 if(referralId != null) url += '&referralId='+referralId;
                 isCommercial =true;
             }
             
             //Retail
             if(recTypes.DeveloperName.containsIgnoreCase('Retail')){
                url = '/apex/RetailLeadEdit?id='+ Leadrec.Id;
                if(referralId != null) url += '&referralId='+referralId;
             }     
            
         }
     //Create Mode
     else{
             
            Schema.DescribeSObjectResult dsr = Lead.SObjectType.getDescribe();
            Schema.RecordTypeInfo defaultRecordType;
            for(Schema.RecordTypeInfo rti : dsr.getRecordTypeInfos()) {
                if(rti.isDefaultRecordTypeMapping()) {
                    defaultRecordType = rti;
                }
            }
            
            if(null == recordTypeId){
                 recTypes = [select Id, Name, sobjectType,DeveloperName from RecordType   
                         where Id  =:defaultRecordType.getRecordTypeId()
                         LIMIT 1];
             }else{
                 recTypes = [select Id, Name, sobjectType,DeveloperName from RecordType   
                         where Id  =:recordTypeId
                         LIMIT 1];
             }
            //Commercial
            if(recTypes.DeveloperName.containsIgnoreCase('Commercial')){
                url = '/apex/LeadCreation'; 
                if(referralId != null) url += '?referralId='+referralId;
                isCommercial=true;
            }
            //Retail
            if(recTypes.DeveloperName.containsIgnoreCase('Retail')){
                url = '/apex/RetailLeadCreation?RecordType='+recTypes.Id; 
                if(referralId != null) url += '&referralId='+referralId;
            }    
        }   
        
         if(isCommercial){
             PageReference result = Page.LeadCreateMobile;
             if(LeadRec.id!=null){
                   result.getParameters().put('id',LeadRec.id);
             }
              
             if (recTypes != null ){
                  result.getParameters().put('RecordType',recTypes.id);
             }
               
         
            result.setRedirect(true); 
            return result;
         }else{
            PageReference page = new PageReference(url); 
            page.setRedirect(true); 
            return page; 
         }
    }

    
}