public class RTL_AccountUtility {
	
	/*
	Method for Auto Create Commercial Contact from Commercial Account
	*/
	public static void createCommercialContactFromAccount( Map<ID,Account> newAccountMap)
	{

		//Set<ID> newAccountIdList = newAccountMap.keySet();
		List<Account> newAccountList = newAccountMap.values();

		//Map<ID,Contact> accountExistingContact = new Map<ID,Contact>();
		//Map<ID,Contact> accountContactSameId = new Map<ID,Contact>();

		//List<Contact> existingAllContacts = [Select Id,AccountId,FirstName,LastName 
		//							FROM Contact WHERE AccountId in :newAccountIdList ];

		//for( Contact ct : existingAllContacts )
		//{
		//	Account acct = newAccountMap.get(ct.AccountId);

		//	// If contact existing in new Account and first name / last name match , add to update list
		//	if( acct.First_name_PE__c == ct.FirstName && acct.Last_name_PE__c == ct.LastName )
		//	{
		//		if( ct.TMB_Customer_ID__c == '' || ct.TMB_Customer_ID__c == null )
		//		{
		//			if(! accountExistingContact.containsKey(ct.AccountId) )
		//			{
		//				accountExistingContact.put(ct.AccountId,ct);
		//			}
		//		}
		//	}

		//	// If Account already have contact that have same tmb cust id , add to ignore update list
		//	if( ct.TMB_Customer_ID__c  == acct.TMB_Customer_ID_PE__c )
		//	{
		//		if(! accountContactSameId.containsKey(ct.AccountId) )
		//		{
		//			accountContactSameId.put(ct.AccountId,ct);
		//		}
		//	}

		//}

		Id contactRT = Schema.Sobjecttype.Contact.getRecordTypeInfosByName().get('Retail Contact').getRecordTypeId();
		List<Contact> nContacts = new List<Contact>();
		//List<Contact> uContacts = new List<Contact>();

		for(Account acct : newAccountList) 
		{

			// Check if contact existing and tmb cust id already same
			//if( accountContactSameId.containsKey(acct.id) )
			//{
			//	continue;
			//}

			// Check if contact exist and need to update tmb cust id
			//if( accountExistingContact.containsKey(acct.id) )
			//{
			//	Contact uc = accountExistingContact.get(acct.id);
			//	uc.TMB_Customer_ID__c = acct.TMB_Customer_ID_PE__c;
			//	uContacts.add(uc);
			//}
			//else // Or need to create new contact
			//{
				Contact nc = autoNewContactFromAccount(acct,contactRT);
				nContacts.add(nc);
			//}			
		}

		if(nContacts.size() > 0)
		{
            insert nContacts;    
		}

		//if(uContacts.size() > 0)
		//{
		//	update uContacts;
		//}

	}

	/*
	Method for Auto Update Commercial Contact from Commercial Account
	*/
	public static void updateCommercialContactFromAccount(List<Account> newAccountList,List<Account> oldAccountList)
	{
		Id contactRT = Schema.Sobjecttype.Contact.getRecordTypeInfosByName().get('Retail Contact').getRecordTypeId();

		Map<Id,Account> listAccsOld = new Map<Id,Account>();
		listAccsOld.putall(oldAccountList);

		Map<Id,Account> uAccounts = new Map<Id,Account>{};
		Map<Id,Account> oAccounts = new Map<Id,Account>{};

		for(Account acct : newAccountList) 
		{
			Account nAcct = acct;
            Account oAcct = listAccsOld.get(acct.Id);
            
            // Field changed
            boolean updateContact = nAcct.Email_Address_PE__c != oAcct.Email_Address_PE__c || nAcct.First_name_PE__c != oAcct.First_name_PE__c || nAcct.Last_name_PE__c != oAcct.Last_name_PE__c || nAcct.Mobile_Number_PE__c != oAcct.Mobile_Number_PE__c;
            // Stage first changed
            boolean stageChange = nAcct.Account_Type__c == 'Existing Customer' && oAcct.Account_Type__c != 'Existing Customer';

            if( acct.Customer_Type__c == 'Individual' )
            {
	            if( updateContact || stageChange ) 
	            {
	                uAccounts.put(nAcct.Id, nAcct);
	                oAccounts.put(nAcct.Id, oAcct);
	            }
        	}
		}

		//=========== Update/Create Contact Process ======================
		List<Contact> uContacts = new List<Contact>{};
		Map<ID,Contact> accountContactTmbCustId = new Map<ID,Contact>();
		Map<ID,Contact> accountContactName = new Map<ID,Contact>();

		if(uAccounts.size() > 0){
            uContacts = [select Id, AccountId, Email, MobilePhone,FirstName, LastName , TMB_Customer_ID__c
            				from Contact where AccountId in :uAccounts.keySet()];

            for(Contact contact: uContacts) {
                Account account = uAccounts.get(contact.AccountId);
                Account accountOld = oAccounts.get(contact.AccountId);

                Boolean newNameMatch = contact.LastName == account.Last_name_PE__c && contact.FirstName == account.First_name_PE__c;
                Boolean oldNameMatch = contact.LastName == accountOld.Last_name_PE__c && contact.FirstName == accountOld.First_name_PE__c;

                // Check for match TMB cust ID
                if( contact.TMB_Customer_ID__c == account.TMB_Customer_ID_PE__c )
                {
                	if( ! accountContactTmbCustId.containsKey( account.id ) )
                	{
                		accountContactTmbCustId.put( account.id , contact );
                	}
                }
                // Check if First Name/ Last Name match
                else if( newNameMatch || oldNameMatch )
                {
                	if( ! accountContactName.containsKey( account.id ) )
                	{
                		accountContactName.put( account.id , contact );
                	}
                }

            }

        }

        List<Contact> updateContacts = new List<Contact>();
        List<Contact> newContact = new List<Contact>();

        List<Account> updatedAccountList = uAccounts.values();

        if( updatedAccountList.size() > 0 )
        {
	        for(Account acct : updatedAccountList) 
	        {
	        	// If tmb cust id on contact already existing
	        	if( accountContactTmbCustId.containsKey( acct.id ) )
	        	{
	        		Contact ct = accountContactTmbCustId.get(acct.id);
	        		ct.LastName = acct.Last_name_PE__c;
	        		ct.FirstName = acct.First_name_PE__c;
	        		ct.Email = acct.Email_Address_PE__c;
	                ct.MobilePhone = acct.Mobile_Number_PE__c;
	                updateContacts.add(ct);
	        	}
	        	// If tmb contact with same name as account present
	        	else if ( accountContactName.containsKey( acct.id ) )
	        	{
	        		Contact ct = accountContactName.get(acct.id);
	        		ct.TMB_Customer_ID__c = acct.TMB_Customer_ID_PE__c;
	        		ct.LastName = acct.Last_name_PE__c;
	        		ct.FirstName = acct.First_name_PE__c;
	        		ct.Email = acct.Email_Address_PE__c;
	                ct.MobilePhone = acct.Mobile_Number_PE__c;
	        		updateContacts.add(ct);
	        	}
	        	// If no suitable contact present , create new contact
	        	else 
	        	{
	        		Contact ct = autoNewContactFromAccount(acct,contactRT);
	        		newContact.add(ct);
	        	}

	        	
	        }
    	}

    	if( updateContacts.size() > 0 )
    	{
    		update updateContacts;
    	}

    	if( newContact.size() > 0 )
    	{
    		insert newContact;
    	}

	}

	public static Contact autoNewContactFromAccount(Account acct, Id contactRT)
	{
		Contact contact = new Contact();
        contact.AccountId = acct.Id;
        contact.RecordTypeId = contactRT;
        contact.TMB_Customer_ID__c = acct.TMB_Customer_ID_PE__c;                 
        contact.Email = acct.Email_Address_PE__c;
        contact.MobilePhone = acct.Mobile_Number_PE__c;
        contact.LastName = acct.Last_name_PE__c ;
        contact.FirstName = acct.First_name_PE__c;
        //contact.LastName = (acct.Last_name_PE__c != null? acct.Last_name_PE__c : acct.First_name_PE__c);

        return contact;
                    
	}

	//public static Contact autoUpdateContactFromAccount( Account account , Contact contact )
	//{
	//	contact.Email = account.Email_Address_PE__c;
 //       contact.MobilePhone = account.Mobile_Number_PE__c;
 //       contact.LastName = (account.Last_name_PE__c != null? account.Last_name_PE__c : account.First_name_PE__c);

 //       return contact;
	//}
}