global class RTL_BatchEmailService {
	global static void SendEmail(Id jobId, String subject, String body) {
		if (Test.isRunningTest()) return;
		OrgWideEmailAddress owea = [select Id from OrgWideEmailAddress limit 1];
		// Get the list of email address from AppConfig
		String emailOpsTeamA = AppConfig__c.getValues('Operation Team A').Value__c;
		String emailOpsTeamB = AppConfig__c.getValues('Operation Team B').Value__c;
		String emailOpsTeamC = AppConfig__c.getValues('Operation Team C').Value__c;
		String emailOpsTeamD = AppConfig__c.getValues('Operation Team D').Value__c;
		String emailOpsTeamAppMon = AppConfig__c.getValues('Operation Team App monitoring').Value__c;
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		String[] toAddresses = mergeEmailAddr(emailOpsTeamA, emailOpsTeamB, emailOpsTeamC, emailOpsTeamD, emailOpsTeamAppMon);
		mail.setToAddresses(toAddresses);
		if (owea != null) {
    		mail.setOrgWideEmailAddressId(owea.Id);
		}			
		mail.setSubject(subject);
		mail.setHtmlBody(body);
		Messaging.SendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		if (results[0].success) {
		    System.debug('The purging email was sent successfully.');
		} else {
		    System.debug('The purging email failed to send: ' + results[0].errors[0].message);
		}
	}    
	
	public static String[] mergeEmailAddr(String emailOpsTeamA, String emailOpsTeamB, String emailOpsTeamC, String emailOpsTeamD, String emailOpsTeamAppMon) {
		String emailAddress = ''; //split by ';'
		if (emailOpsTeamA != null && emailOpsTeamA != '') {
			emailAddress += emailOpsTeamA + ';';
		}
		if (emailOpsTeamB != null && emailOpsTeamB != '') {
			emailAddress += emailOpsTeamB + ';';
		}
		if (emailOpsTeamC != null && emailOpsTeamC != '') {
			emailAddress += emailOpsTeamC + ';';
		}	
		if (emailOpsTeamD != null && emailOpsTeamD != '') {
			emailAddress += emailOpsTeamD + ';';
		}	
		if (emailOpsTeamAppMon != null && emailOpsTeamAppMon != '') {
			emailAddress += emailOpsTeamAppMon;
		}
		if (emailAddress.endswith(';')) {
			emailAddress = emailAddress.substring(0, emailAddress.length()-1);
		}
		String[] emailAddrTmp = emailAddress.split(';');
		Set<String> emailAddrSet = new Set<String>();
		for (String email: emailAddrTmp) {
			if (email != '') {//filter blank or duplicate email
				emailAddrSet.add(email);
			}
		}
		List<String> emailAddrList = new List<String>();
		emailAddrList.addAll(emailAddrSet);		
		return emailAddrList;
	}
}