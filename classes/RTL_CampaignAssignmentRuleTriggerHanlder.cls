public with sharing class RTL_CampaignAssignmentRuleTriggerHanlder {
	public RTL_CampaignAssignmentRuleTriggerHanlder() {
		
	}

	public static void afterInsert(List<RTL_Campaign_Assignment_Rule__c> newList, Map<Id, RTL_Campaign_Assignment_Rule__c> oldMap){
		system.debug('RTL_CampaignAssignmentRuleTriggerHanlder - Start After Insert ');
		assignCampaignMemberToCampaign(newList,oldMap);
		system.debug('RTL_CampaignAssignmentRuleTriggerHanlder - End After Insert ');
	}

	public static void afterUpdate(List<RTL_Campaign_Assignment_Rule__c> newList, Map<Id, RTL_Campaign_Assignment_Rule__c> oldMap){
		system.debug('RTL_CampaignAssignmentRuleTriggerHanlder - Start After Update ');
		assignCampaignMemberToCampaign(newList,oldMap);
		system.debug('RTL_CampaignAssignmentRuleTriggerHanlder - End After Update ');
	}

	public static void assignCampaignMemberToCampaign (List<RTL_Campaign_Assignment_Rule__c> newList, Map<Id, RTL_Campaign_Assignment_Rule__c> oldMap){

		String source = '';
		Date startdate = null;
		Date enddate = null;

		List<RTL_Campaign_Assignment_Rule__c> carList = new List<RTL_Campaign_Assignment_Rule__c>();
		List<CampaignMember> dummyCMList = new List<CampaignMember>();
		List<Campaignmember> tempCMList = new List<CampaignMember>();
		List<Campaignmember> newCMList = new List<CampaignMember>();
		List<Campaignmember> delCMList = new List<CampaignMember>();
		List<String> camId = new List<String>();

		for(RTL_Campaign_Assignment_Rule__c car : newList){
			if(car.RTL_Campaign_Lead_Source__c != null 
				&& car.RTL_Start_Date__c != null 
				&& car.RTL_End_Date__c != null
				&& car.RTL_Active__c == true){
				carList.add(car);
			}
		}

		if(carList.size() > 0 ){
			source = carList.get(0).RTL_Campaign_Lead_Source__c;
			startdate = carList.get(0).RTL_Start_Date__c;
			enddate = carList.get(0).RTL_End_Date__c;

			RTL_Campaign_Assignment_Rule__c newCar = carList.get(0);

			Id recordTypeId  = [select Id from RecordType where SobjectType = 'Campaign' and Name=: 'Dummy Campaign' and IsActive=true ].Id;

			dummyCMList = [Select id,Name,CampaignId,CreatedDate from Campaignmember 
					WHERE CreatedDate >: startdate and CreatedDate <: enddate
					AND RTL_TMB_Campaign_Source__c =: source
					AND Campaign.Recordtypeid =: recordTypeId];

			
			if(dummyCMList.size() > 0){
				for(Campaignmember c : dummyCMList){
					if(c.CampaignId != newCar.RTL_Campaign__c){
						camId.add('\''+String.valueOf(c.id)+'\'');
					}	
				}
			}

			if(camId.size() > 0){
				DescribeSObjectResult describeResult = Campaignmember.getSObjectType().getDescribe();		
				List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );	

				//Get unused field campaign member before insert campaign member
				List<RTL_Web_Drop_Lead_Unused_Field__mdt> listUnusedField = [SELECT RTL_API_Name__c FROM RTL_Web_Drop_Lead_Unused_Field__mdt];

				for(integer i=0;i<fieldNames.size();i++){
					for(RTL_Web_Drop_Lead_Unused_Field__mdt f : listUnusedField){
						if(fieldNames.get(i) == f.RTL_API_Name__c){
							fieldNames.remove(i);
						}
					}
				}

				String query =	' SELECT ' + String.join( fieldNames, ',' ) +' FROM ' +	describeResult.getName() + ' Where id in ' + camId;		
				system.debug(query);
				tempCMList = Database.query( query );		
			}

			if(tempCMList.size() >0){
				for(CampaignMember m : tempCMList){
		            CampaignMember campMemb = m.clone(false, true);
		            campMemb.Campaignid = newCar.RTL_Campaign__c;
		            newCMList.add(campMemb);
	        	}
			}

			try{
				system.debug('CAR Trigger : Campaign member dummy - '+dummyCMList);
				system.debug('CAR Trigger : Campaign member insert to new campaign - '+newCMList);
				
				List<Id> idList = new List<Id>();

				if(newCMList.size() >0){
					Database.SaveResult[] lsr = Database.insert(newCMList, false);
		            // Iterate through each returned result
		            for (Database.SaveResult sr : lsr) {
		                if (sr.isSuccess()) {
		                    // Operation was successful, so get the ID of the record that was processed
		                    idList.add(sr.getId());
		                    System.debug('Successfully move dummy campaign member.');
		                    
		                }
		                else {
		                    // Operation failed, so get all errors               
		                    for(Database.Error err : sr.getErrors()) {
		                        System.debug(logginglevel.ERROR, 'There is error move dummy campaign member. Error Message is: ' + err.getMessage());
		                    }

		                }
		            } 
				}

				if(idList.size() > 0){
					system.debug('Success record : ' + idList);
					List<Id> contactidList = new List<Id>();
					List<Id> leadidList = new List<Id>();

					List<Campaignmember> succeesList = [Select leadid,contactid from Campaignmember where Id = :idList];
					if(succeesList.size() > 0){
						for(Campaignmember c : succeesList){
							if(c.leadid != null){
								leadidList.add(c.leadid);
							}else if(c.contactid != null){
								contactidList.add(c.contactid);
							}else{
								system.debug('Campaign member not found.');
							}

						}

					}

					delCMList = [ select Id ,Name from Campaignmember 
									where (leadid in:leadidList or contactid in:contactidList) 
									AND Campaign.Recordtypeid =: recordTypeId ];
					system.debug('CAR Trigger : Delete Campaign member dummy - '+delCMList);

					try{
						delete delCMList;
					}catch(Exception e){
		             	system.debug(e);
		            }
				}

			}catch(Exception e){
             	system.debug(e);
            }

			
		}



	}



}