public with sharing class RTL_CampaignCallListExtension {

    public String CampaignID;
    public String CampaignMemberID {get;set;}
    public CampaignMember CampMemObj {get;set;}
    public CampaignMember camMemReassign {get;set;}
    public transient Map<String,String> campaignMap {get;set;}
    

    public List<SelectOption> userOwnerSelectionList {get{
        if(userOwnerSelectionList == null){
            userOwnerSelectionList = new List<SelectOption>();
            userOwnerSelectionList.add(new SelectOption('',Label.RTL_Campaign_CallList_Filter_All));
            userOwnerSelectionList.add(new SelectOption('My List',Label.RTL_Campaign_CallList_MyList_Option)); 
            userOwnerSelectionList.add(new SelectOption('My team List',Label.RTL_Campaign_CallList_MyTeamList_Option));          
        }
        return userOwnerSelectionList;
    }
    set;}
    public List<SelectOption> offerResultSelectList {get{
        if(offerResultSelectList == null){
            offerResultSelectList = new List<SelectOption>();

            offerResultSelectList.add(new SelectOption('',Label.RTL_Campaign_CallList_Filter_All ));
            //offerResultSelectList.add(new SelectOption('Pending', Label.RTL_Campaign_Member_Filter_Offer_Pending ));
            //offerResultSelectList.add(new SelectOption('Interested',Label.RTL_Campaign_Member_Filter_Offer_Interest ));
            //offerResultSelectList.add(new SelectOption('Not Interested',Label.RTL_Campaign_Member_Filter_Offer_NotInterest ));
            offerResultSelectList.add(new SelectOption(Label.RTL_Campaign_Member_Filter_Offer_Pending, Label.RTL_Campaign_Member_Filter_Offer_Pending ));
            offerResultSelectList.add(new SelectOption(Label.RTL_Campaign_Member_Filter_Offer_Interest ,Label.RTL_Campaign_Member_Filter_Offer_Interest ));
            offerResultSelectList.add(new SelectOption(Label.RTL_Campaign_Member_Filter_Offer_NotInterest ,Label.RTL_Campaign_Member_Filter_Offer_NotInterest ));
        }
        return offerResultSelectList;
        }set;}

    private Set<ID> retailRecordTypeIDset {
        get{
            if( retailRecordTypeIDset == null )
            {
                retailRecordTypeIDset = new Set<ID>();
                 for(Recordtype perRecordType :  [SELECT ID from Recordtype 
                            WHERE sObjectType ='Campaign'
                            AND (NOT DeveloperName LIKE '%Master%')
                            AND (NOT DeveloperName LIKE '%Dummy%')
                            AND (NOT DeveloperName LIKE '%Commercial%')]){
        
                                retailRecordTypeIDset.add(perRecordType.id); 
                            }
            }

            return retailRecordTypeIDset;
        }
        set;

    }

    public List<SelectOption> CampaignSelectOptionList {get{
            if(CampaignSelectOptionList == null){

                CampaignSelectOptionList = new List<SelectOption>();
                CampaignSelectOptionList.add(new SelectOption('',Label.RTL_Campaign_CallList_Filter_All));
                for(Campaign cp : [SELECT ID,RecordTypeId,Status,isActive,Name 
                                    FROM Campaign 
                                WHERE RecordTypeId IN:retailRecordTypeIDset
                                AND Status != 'Expired'
                                AND isActive=true
                                AND RTL_Call_Start_Date__c <= TODAY AND RTL_Call_End_Date__c >= TODAY
                                ORDER BY RTL_Priority__c ASC ,NAME ASC]){

                    CampaignSelectOptionList.add(new SelectOption(cp.id,cp.Name));
                    
                }
                

            }
            return CampaignSelectOptionList;
        }set;}

    public List<SelectOption> contactStatusSelectionList {get{
        if(contactStatusSelectionList == null){
            contactStatusSelectionList = new List<SelectOption>();
            contactStatusSelectionList.add(new SelectOption('',Label.RTL_Campaign_CallList_Filter_All));

            Schema.DescribeFieldResult fieldResult = CampaignMember.RTL_Contact_Status__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

            Set<String> availableOptions = new Set<String>{'New', 'Contact', 'Follow-up', 'Re-attempt'};

            for( Schema.PicklistEntry f : ple)
            {
                if( availableOptions.contains( f.getValue() ) )
                {
                    contactStatusSelectionList.add(new SelectOption(f.getValue(), f.getLabel() ));
                }
            }  

            
        }
        return contactStatusSelectionList;
    }
    set;}

    public List<SelectOption> invitationStatusSelectionList {get{
        if(invitationStatusSelectionList == null){
            invitationStatusSelectionList = new List<SelectOption>();
            invitationStatusSelectionList.add(new SelectOption('',Label.RTL_Campaign_CallList_Filter_All));

            Schema.DescribeFieldResult fieldResult = CampaignMember.RTL_Invitation_Status__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

            //Set<String> availableOptions = new Set<String>{'New', 'Contact', 'Follow-up', 'Re-attempt'};

            for( Schema.PicklistEntry f : ple)
            {
                //if( availableOptions.contains( f.getValue() ) )
                //{
                    invitationStatusSelectionList.add(new SelectOption(f.getValue(), f.getLabel() ));
                //}
            }  

            
        }
        return invitationStatusSelectionList;
    }
    set;}

    //public String selectedOwner {get;set;}
    public boolean isFromCampaign {get;set;}
    public boolean isFromHomeScreen {get;set;}
    public boolean isSearchScreen {get;set;}
    public boolean isRelocateScreen {get;set;}
    public boolean isToggleSelectAll {get;set;}
    public Integer OffsetSize {get;set;}
    private Integer QueryLimit = 400;
    public Integer CountTotalRecords {get;set;}
    public Integer MaxNumoberofRecords {get;set;}
    public Integer StartNumberofRecords {get;set;}
    //public Datetime callEndDate {get;set;}

    // Not implement search cache
    //private RTL_Campaign_CallList_Search_Cache__c searchCache{
    //    get{
    //        if( searchCache ==null )
    //        {
    //            String userId = Userinfo.getUserId();
    //            try {
    //                searchCache = [SELECT id,RTL_User__c,RTL_Call_End_Date__c,
    //                    RTL_Campaign__c,RTL_Contact_Status__c,RTL_Invitation_Status__c,
    //                    RTL_Offer_Result__c,RTL_Owner__c
    //                    FROM RTL_Campaign_CallList_Search_Cache__c 
    //                    WHERE RTL_User__c = :userId
    //                ];
    //            } catch(Exception e) {
    //                searchCache = new RTL_Campaign_CallList_Search_Cache__c();
    //                searchCache.RTL_User__c = userId;
    //            }             
    //        }
    //        return searchCache;   
    //    }
    //    set;
    //}

    private string isMyCallList;

    private static final Integer NORECORDPERPAGE = 5;

    public class CampaignMemberWrapper{
        public Integer RowNumber {get;set;}
        public CampaignMember campaignmemberobj {get;set;}
        public boolean isSelected {get;set;}
        public String Title {get;set;}
        public String CompanyName {get;set;}
        public String FirstName {get;set;}
        public String LastName {get;set;}
        public String Email {get;set;}
        public String Type {get;set;}
        public String CampaignName {get;set;}
        // For display in table for debug
        public Date CallEndDate{get;set;}
        public String ContactStatus {get;set;}
        public String offerResult {get;set;}
        public String invitationStatus {get;set;}
    }


    public  List<CampaignMember> CampaignMemberList {get{
        if(CampaignMemberList ==null){
            CampaignMemberList = new List<CampaignMember>();
        }
        return CampaignMemberList;
        } set;}


    public List<CampaignMemberWrapper> campaignwrapperList {get{
        if(campaignwrapperList ==null){
            campaignwrapperList = new List<CampaignMemberWrapper>();   
        }
        return campaignwrapperList;                                          

    }set;}

    public User currentUser{
        get{
            if( currentUser == null )
            {
                currentUser = [SELECT ID,UserRole.Name,
                        RTL_Channel__c,Zone_Code__c, RTL_Branch_Code__c 
                FROM USER WHERE ID =:Userinfo.getUserId() ];
            }
            return currentUser;
        }
        set;
    }
            
    private String userType {
        get{
            if(userType == null)
            {
                String roleName = currentUser.UserRole.Name;
                
                if( roleName != null )
                {
                    if( roleName.startsWith('BR-Sales-Zone') )
                    {
                        userType = 'Agent'; 
                    }
                    else if( roleName.startsWith('BR-MGR-Zone') )
                    {
                        userType = 'BrancManager';
                    }
                    else if( roleName.startsWith('WM-TL') )
                    {
                        userType = 'WM-TL';
                    }
                    else if( roleName.startsWith('WM-RM') )
                    {
                        userType = 'WM-RM';
                    }
                    else 
                    {
                        userType = 'other';                        
                    }

                }
                else 
                {
                    userType = 'other';   
                }



            }
            return userType;
        }
        set;
    }

    private String userBranchId {
        get{
            if( userBranchId == null )
            {
                //User currentuser = [SELECT ID,UserRole.Name,
                //        RTL_Channel__c,Zone_Code__c, RTL_Branch_Code__c 
                //        FROM USER WHERE ID =:Userinfo.getUserId() ];

                if(currentuser.RTL_Branch_Code__c !=null){
                    List<Branch_and_Zone__c> BranchZonelist =  [SELECT ID,Branch_Code__c  from Branch_and_zone__c
                                                  WHERE Branch_Code__C =: currentuser.RTL_Branch_Code__c LIMIT 1];
                    if(BranchZonelist.size()>0){
                        Branch_and_Zone__c baz = BranchZonelist.get(0);
                        userBranchId = baz.id;
                    }
                }
            }
            return userBranchId;
        }
        set;
    }

    public String searchcamMemCampaignID{get;set;}
    public String searchcamMemContactStatus{get;set;}
    public Date searchcamMemEndDate{get;set;}
    public String searchcamMemOfferResult{get;set;}
    public String searchcamMemInvitationStatus{get;set;}
    public String searchselectedOwner{get;set;}

    private String firstSorting;
    private String lastSorting;

    //public string debug {get;set;}

    public RTL_CampaignCallListExtension(ApexPages.StandardController stdController) {


        CampaignID = Apexpages.CurrentPage().getParameters().get('id');
        isMyCallList = Apexpages.CurrentPage().getParameters().get('mylist');

        CampMemObj = new CampaignMember();
        CampMemObj.Campaign = new Campaign();
        CampMemObj.Campaign.RTL_Call_End_Date__c = null;

        camMemReassign = new CampaignMember();
        isFromCampaign =false;
        isFromHomeScreen = false;                             
        isSearchScreen =true;
        isRelocateScreen =false;
        isToggleSelectAll = false;
        OffsetSize = 0;

        searchcamMemOfferResult = Label.RTL_Campaign_Member_Filter_Offer_Pending;

        // When navigate to callist directly , call search cache
        if( CampaignID == null && isMyCallList == null )
        {
            CampMemObj.RTL_Contact_Status__c = '';
            searchcamMemContactStatus='';
           
        }
        else if( isMyCallList == '1' )
        {
            //selectedOwner = 'My List';
            //searchselectedOwner = selectedOwner;
            searchselectedOwner = 'My List';
            CampMemObj.RTL_Contact_Status__c = '';
        }
        else if(CampaignID !=null && CampaignID !='')
        {
            isFromCampaign = true;
            CampMemObj.CampaignID = CampaignID;
            searchcamMemCampaignID = CampaignID;
            CampMemObj.RTL_Contact_Status__c = '';
             
        }

        queryPaginateCampaignMemberWrapperList(QueryLimit,'init');
        
    }
    
    public List<CampaignMemberWrapper>  queryPaginateCampaignMemberWrapperList(Integer NoOflimit,String type)
    {

        campaignwrapperList = new List<CampaignMemberWrapper>();

        List<String> filterStringList = new List<String>();

        MaxNumoberofRecords = 0;

        String CountQuery ='SELECT Count() from CampaignMember ';

        String QueryString = 'SELECT ID,CampaignID,Campaign.Name,LeadID,ContactID,Name,RTL_Contact_Status__c,RTL_Channel__c, ' +
                                                    'RTL_Campaign_Priority__c,RTL_Campaign_Channel_formula__c, '+
                                                    'RTL_Contact_Staff_Name__c,RTL_Invitation_Status__c, ' +
                                                    'RTL_Assigned_Zone_Rpt__c,RTL_Assigned_Agent__r.FirstName,RTL_Assigned_Agent__r.LastName, '+
                                                    'RTL_Contact_Staff_Name__r.FirstName,RTL_Contact_Staff_Name__r.LastName, '+
                                                    'LastModifiedDate,Lead.Status,RTL_Assigned_Agent__c,RTL_Assigned_Branch__c,RTL_Campaign_EndDate__c, '+
                                                    'Status,Lead_Status__c,Lead.Title,Lead.FirstName,Lead.LastName, '+
                                                    'Contact.FirstName,Contact.Lastname,RTL_Offer_Result__c, '+
                                                    'Lead.Name,Contact.Name,Contact.Title,Lead.Company, '+
                                                    'contact.Account.Name,lead.email,contact.email, '+
                                                    'RTL_Segmentation__c,RTL_Last_Contact_Status__c,Campaign.RTL_Call_End_Date__c, '+
                                                    'RTL_Campaign_Member_Sorting_Order__c,RTL_Company_Account__c, '+
                                                    'RTL_Segment__c,RTL_Promotion__c,RTL_Product_Feature__c,customer__c,Customer__r.Name '+
                                                    'from CampaignMember ';

        String suffixQueryString = '';
        if( type == 'next' )
        {
            suffixQueryString = ' ORDER BY RTL_Campaign_Member_Sorting_Order__c ASC NULLS LAST ';
            filterStringList.add(' RTL_Campaign_Member_Sorting_Order__c > :lastSorting ');
            
        }
        else if ( type == 'previous' )
        {
            suffixQueryString = ' ORDER BY RTL_Campaign_Member_Sorting_Order__c DESC NULLS LAST ';
            filterStringList.add(' RTL_Campaign_Member_Sorting_Order__c < :firstSorting ');
        }
        else  if ( type == 'init' )
        {
            suffixQueryString = ' ORDER BY RTL_Campaign_Member_Sorting_Order__c ASC NULLS LAST ';
        }

        String limitsQueryString = ' LIMIT :NoOflimit';

        if(searchcamMemCampaignID !=null&& searchcamMemCampaignID !=''){
            filterStringList.add(' CampaignID = :searchcamMemCampaignID ');
        }

        if(searchcamMemContactStatus !=null && searchcamMemContactStatus !=''){
            filterStringList.add(' RTL_Contact_Status__c = :searchcamMemContactStatus ');
        }

        if( searchcamMemEndDate != null )
        {
            //*** need to check from campaign instead
            filterStringList.add(' Campaign.RTL_Call_End_Date__c < :searchcamMemEndDate ');
        }

        if(searchcamMemOfferResult !=null && searchcamMemOfferResult !=''){
            filterStringList.add(' RTL_Offer_Result__c = :searchcamMemOfferResult ');
        }

        if(searchcamMemInvitationStatus !=null && searchcamMemInvitationStatus !=''){
            filterStringList.add(' RTL_Invitation_Status__c = :searchcamMemInvitationStatus ');
        }

        //============= Defualt Filter base on User Type =============

        filterStringList.add(' Campaign.RecordTypeId IN :retailRecordTypeIDset ');
        filterStringList.add(' Campaign.Status  != \'Expired\' ');
        filterStringList.add(' Campaign.isActive = true ');
        filterStringList.add(' Campaign.RTL_Call_Start_Date__c <= TODAY AND Campaign.RTL_Call_End_Date__c >= TODAY ');

        // Normal User see only Campaing Member assinged to their branch
        if(  userType == 'Agent' || userType == 'BrancManager')
        {
            filterStringList.add(' ( RTL_Assigned_Branch__c = :userBranchId OR RTL_Assigned_Agent__c = :userId OR RTL_Contact_Staff_Name__c = :userId ) ');
        }
        // Wealth User see only Campaing Member assinged to their zone
        else if (  userType == 'WM-TL' || userType == 'WM-RM' )
        {
            String zoneCode = currentUser.Zone_Code__c;
            filterStringList.add(' ( RTL_Assigned_Zone_Rpt__c = :zoneCode OR RTL_Assigned_Agent__c = :userId OR RTL_Contact_Staff_Name__c = :userId ) ');
            // Wealth team now allowed to see campaign member event it assigned to branch
            //filterStringList.add(' RTL_Assigned_Branch__c = null ');
        }


        String userId = currentUser.id;
        if( searchselectedOwner == 'My List' )
        {
            filterStringList.add('( RTL_Contact_Staff_Name__c = :userId OR RTL_Assigned_Agent__c = :userId )');
        }
        else if (searchselectedOwner == 'My team List')
        {
            if( userType == 'BrancManager' || userType == 'WM-TL'  )
            {
                // No filter , see all Campaign Member from thier zone 
                if( userType == 'BrancManager'  )
                {
                    filterStringList.add(' RTL_Assigned_Branch__c = :userBranchId ');
                }
                else if( userType == 'WM-TL'  )
                {
                    filterStringList.add(' RTL_Assigned_Zone_Rpt__c = :zoneCode ');
                }
            }
            else if( userType == 'Agent' || userType == 'WM-RM'  )
            {
                // Assigned Branch/Zone Must be same as User's 
                if( userType == 'Agent'  )
                {
                    filterStringList.add(' RTL_Assigned_Branch__c = :userBranchId ');
                }
                else if( userType == 'WM-RM'  )
                {
                    filterStringList.add(' RTL_Assigned_Zone_Rpt__c = :zoneCode ');
                }

                filterStringList.add(' RTL_Assigned_Agent__c = null ');
                filterStringList.add(' RTL_Contact_Staff_Name__c = null ');
            }

        }   
        // no select owner , See All - with atleast branch/contact/assigned assing to current user
        else 
        {
            if( userType == 'BrancManager' || userType == 'WM-TL'  )
            {
                // No filter , see all Campaign Member from thier zone 
            }
            else if( userType == 'Agent' || userType == 'WM-RM'  )
            {
                if( userType == 'Agent'  )
                {
                    filterStringList.add(' ( RTL_Contact_Staff_Name__c = :userId OR RTL_Assigned_Agent__c = :userId OR ( RTL_Assigned_Branch__c = :userBranchId AND RTL_Assigned_Agent__c = null AND RTL_Contact_Staff_Name__c = null ) ) ');
                }
                else if( userType == 'WM-RM'  )
                {
                    filterStringList.add(' ( RTL_Contact_Staff_Name__c = :userId OR RTL_Assigned_Agent__c = :userId OR ( RTL_Assigned_Zone_Rpt__c = :zoneCode AND RTL_Assigned_Agent__c = null AND RTL_Contact_Staff_Name__c = null ) ) ');
                }
                //filterStringList.add(' ( RTL_Assigned_Agent__c = null OR ( RTL_Assigned_Agent__c != null  AND ( RTL_Contact_Staff_Name__c = :userId OR RTL_Assigned_Agent__c = :userId ) ) ) ');
                //filterStringList.add(' ( RTL_Assigned_Agent__c = null OR RTL_Contact_Staff_Name__c = :userId OR RTL_Assigned_Agent__c = :userId ) ');
            }
        }
      

        
       // if(searchcamMemAssignedBranch!=null && searchcamMemAssignedBranch !=''){
       //     filterStringList.add(' RTL_Assigned_Branch__c = :searchcamMemAssignedBranch ');
       // }
       // if(searchcamMemberType !=null && searchcamMemberType !=''){
       //         if(selectedtype=='Lead'){
       //             filterStringList.add(' LeadID !=null ');
       //         }else if(selectedtype=='Contact'){
       //             filterStringList.add(' ContactID !=null ');
       //         }
       // }
       // if(searchcamMemAssignedAgent !=null && searchcamMemAssignedAgent !=''){
       //     filterStringList.add(' RTL_Assigned_Agent__c = :searchcamMemAssignedAgent ');
       // }


        if(filterStringList.size()>0){
            QueryString +=' WHERE ';
            CountQuery +=' WHERE ';
            integer index = 1;
            for(String filterstr : filterStringList){
                if(index !=1){
                    QueryString += ' AND ';
                    CountQuery += ' AND ';
                }
                QueryString += filterstr;
                CountQuery += filterstr;
                index++;
            }
        }

        if( type == 'init' )
        {
            countTotalRecords = Database.countQuery(CountQuery);
        }

        QueryString += suffixQueryString + limitsQueryString;

        //CampaignMemberList = (List<CampaignMember>)Database.query(QueryString);
        List<CampaignMember> CampaignMemberTmpList = (List<CampaignMember>)Database.query(QueryString);
        // Resort result order then process
        CampaignMemberList = new List<CampaignMember>();
        if( type == 'next' || type == 'init' )
        {
            CampaignMemberList = CampaignMemberTmpList;
            
        }
        else if ( type == 'previous' )
        {
            for(Integer i = CampaignMemberTmpList.size() - 1; i >= 0; i--){
                CampaignMemberList.add(CampaignMemberTmpList[i]);
            }
        }
        
        campaignMap = new Map<String,String>();
        Set<ID> campaignIDSet = new Set<ID>(); 

        for(CampaignMember cm : CampaignMemberList){
            CampaignMemberWrapper cmw = new CampaignMemberWrapper();
                cmw.campaignmemberobj = cm;
                cmw.isSelected =false;
                cmw.RowNumber = OffsetSize+MaxNumoberofRecords+1;
               
                if(cm.ContactID !=null){
                    cmw.Type = 'Contact';
                    cmw.Title = cm.Contact.Title;
                    cmw.FirstName = cm.Contact.FirstName;
                    cmw.LastName = cm.Contact.LastName;
                    cmw.CompanyName = cm.contact.Account.Name;
                    cmw.Email = cm.contact.email;
                }else if(cm.LeadID !=null){
                    cmw.Type = 'Lead';
                    cmw.Title = cm.Lead.Title;
                    cmw.FirstName = cm.Lead.FirstName;
                    cmw.LastName = cm.Lead.LastName;
                    cmw.CompanyName = cm.lead.Company;
                    cmw.Email = cm.Lead.email;
                }
                // For debug
                cmw.CallEndDate = cm.Campaign.RTL_Call_End_Date__c;
                cmw.ContactStatus = cm.RTL_Contact_Status__c;
                cmw.offerResult = cm.RTL_Offer_Result__c;
                cmw.invitationStatus = cm.RTL_Invitation_Status__c;

                campaignIDSet.add(cm.CampaignId);
                campaignwrapperList.add(cmw);
                MaxNumoberofRecords++;
        }

        for(Campaign camp : [SELECT ID,Name from Campaign WHERE ID IN:campaignIDSet]){
            campaignMap.put(camp.id,camp.name);
        }

        for(CampaignMemberWrapper cpmwp : campaignwrapperList){
                cpmwp.CampaignName = campaignMap.get(cpmwp.campaignmemberobj.CampaignID);
        }

        MaxNumoberofRecords +=OffsetSize;

        if( MaxNumoberofRecords == 0 )
        {
            StartNumberofRecords = 0;
        }
        else 
        {
            StartNumberofRecords = OffsetSize+1;
        }

        if( CampaignMemberList.size() > 0 )
        {
            firstSorting =  CampaignMemberList.get(0).RTL_Campaign_Member_Sorting_Order__c;
            lastSorting = CampaignMemberList.get( CampaignMemberList.size() - 1 ).RTL_Campaign_Member_Sorting_Order__c;

        }

        return campaignwrapperList;
    }

  


    public void searchCampaignMembers(){


        searchcamMemCampaignID = CampMemObj.CampaignID==null?'':CampMemObj.CampaignID;
        //searchselectedOwner = selectedOwner;
        searchcamMemContactStatus = CampMemObj.RTL_Contact_Status__c==null?'':CampMemObj.RTL_Contact_Status__c;
        //searchcamMemEndDate = CampMemObj.Campaign.RTL_Call_End_Date__c;
        // RTL_Offer_Result__c not writeable , so read data from searchcamMemOfferResult directly in page
        //searchcamMemOfferResult = CampMemObj.RTL_Offer_Result__c==null?'':CampMemObj.RTL_Offer_Result__c;
        searchcamMemInvitationStatus = CampMemObj.RTL_Invitation_Status__c==null?'':CampMemObj.RTL_Invitation_Status__c;

        
        //======== Not implement search catch ===========
        //searchCache.RTL_Call_End_Date__c = CampMemObj.RTL_Call_End_Date__c;
        //searchCache.RTL_Campaign__c =  CampMemObj.CampaignID;
        //searchCache.RTL_Contact_Status__c = CampMemObj.RTL_Contact_Status__c;
        //searchCache.RTL_Invitation_Status__c = CampMemObj.RTL_Invitation_Status__c;
        //searchCache.RTL_Offer_Result__c = searchcamMemOfferResult;
        //searchCache.RTL_Owner__c = selectedOwner;

        //upsert searchCache;


        OffsetSize = 0;
        queryPaginateCampaignMemberWrapperList(QueryLimit,'init');

        

    }

    

    
    public void backbutton(){
            isSearchScreen =true;
            isRelocateScreen =false;
            //queryCampaignMemberWrapperList(QueryLimit, OffsetSize);
            queryPaginateCampaignMemberWrapperList(QueryLimit,'init');
    }
        
    public Pagereference cancel(){
        PageReference detailpage;
        detailpage = new ApexPages.StandardController(CampMemObj).view();
        detailpage.setRedirect(true);          
        return detailpage; 
    }



     public PageReference Next() {
        OffsetSize += QueryLimit;
        //queryCampaignMemberWrapperList(QueryLimit, OffsetSize);
        queryPaginateCampaignMemberWrapperList(QueryLimit,'next');
        return null;
    }
    public PageReference Previous() {
        OffsetSize -= QueryLimit;
        //queryCampaignMemberWrapperList(QueryLimit, OffsetSize);
        queryPaginateCampaignMemberWrapperList(QueryLimit,'previous');
        return null;
    }
  
    public Boolean getDisablePrevious(){
        if(OffsetSize>0){
            return false;
        }
        else return true;
    }
    public Boolean getDisableNext() {
        if (OffsetSize + QueryLimit < countTotalRecords){

            return false;
        }
        else return true;
    }



   
}