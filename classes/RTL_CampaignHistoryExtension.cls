public with sharing class RTL_CampaignHistoryExtension {

	public class MassCampaignData{
		public Campaign camObj  {get;set;}
		public CampaignMember camMemObj  {get;set;}

		public CampaignMember getCam()
	    {
	        return new CampaignMember(CampaignId = this.camObj.id);
	    }
	}

	public Account accountObj{get;set;}
	private Id accountId;

	private List<String> exclusiveCampaignRecordTypeDevName {
		get{
			if( exclusiveCampaignRecordTypeDevName == null )
			{
				exclusiveCampaignRecordTypeDevName = new List<String>();
				
				exclusiveCampaignRecordTypeDevName.add('Exclusive_Campaign_Active');
				exclusiveCampaignRecordTypeDevName.add('Exclusive_Campaign_Expired');
				
			}
			return exclusiveCampaignRecordTypeDevName;
		}
		set;
	}

	private List<String> localExCampaignRecordTypeDevName {
		get{
			if( localExCampaignRecordTypeDevName == null )
			{
				localExCampaignRecordTypeDevName = new List<String>();
				
				localExCampaignRecordTypeDevName.add('Local_Exclusive_Campaign_Active');
				localExCampaignRecordTypeDevName.add('Local_Exclusive_Campaign_Expired');
				
			}
			return localExCampaignRecordTypeDevName;
		}
		set;
	}

	private List<String> massCampaignRecordTypeDevName {
		get{
			if( massCampaignRecordTypeDevName == null )
			{
				massCampaignRecordTypeDevName = new List<String>();
				massCampaignRecordTypeDevName.add('Mass_Campaign_Active');
			}
			return massCampaignRecordTypeDevName;
		}
		set;
	}

	public String campaignLiteURL {
		get{
			if( campaignLiteURL == null )
			{
				RTL_Campaign_History_Setting__mdt chMeta = [select id,CampaignLiteURL__c
                   from RTL_Campaign_History_Setting__mdt 
                   where DeveloperName = 'Campaign_Display_Setting'];

                campaignLiteURL = chMeta.CampaignLiteURL__c;
			}
			return campaignLiteURL;
		}
		set;
	}

	private Integer customerRequestCampaignItemPerPage {
		get{
			if( customerRequestCampaignItemPerPage == null )
			{
				RTL_Campaign_History_Setting__mdt chMeta = [select id,CustomerRequestDisplay__c
                   from RTL_Campaign_History_Setting__mdt 
                   where DeveloperName = 'Campaign_Display_Setting'];

                customerRequestCampaignItemPerPage = integer.valueOf(chMeta.CustomerRequestDisplay__c);
			}
			return customerRequestCampaignItemPerPage;
		}
		set;
	}
	public List<CampaignMember> customerRequestCampaign {get;set;}
	public Integer customerRequestCampaignPage {get;set;}
	public Long customerRequestCampaignTotalPage {get;set;}
	public Boolean disablePreviousCustomerRequeste {get;set;}
	public Boolean disableNextCustomerRequest {get;set;}
	public List<Integer> customerRequestPageList {get;set;}


	private Integer exclusiveCampaignItemPerPage {
		get{
			if( exclusiveCampaignItemPerPage == null )
			{
				RTL_Campaign_History_Setting__mdt chMeta = [select id,ExclusiveDisplay__c
                   from RTL_Campaign_History_Setting__mdt 
                   where DeveloperName = 'Campaign_Display_Setting'];

                exclusiveCampaignItemPerPage = integer.valueOf(chMeta.ExclusiveDisplay__c);
			}
			return exclusiveCampaignItemPerPage;
		}
		set;
	}
	public List<CampaignMember> exclusiveCampaign {get;set;}
	public Integer exclusiveCampaignPage {get;set;}
	public Long exclusiveExCampaignTotalPage {get;set;}
	public Boolean disablePreviousExclusive {get;set;}
	public Boolean disableNextExclusive {get;set;}
	public List<Integer> exclusivePageList {get;set;}

	private Integer localExCampaignItemPerPage {
		get{
			if( localExCampaignItemPerPage == null )
			{
				RTL_Campaign_History_Setting__mdt chMeta = [select id,LocalExclusiveDisplay__c
                   from RTL_Campaign_History_Setting__mdt 
                   where DeveloperName = 'Campaign_Display_Setting'];

                localExCampaignItemPerPage = integer.valueOf(chMeta.LocalExclusiveDisplay__c);
			}
			return localExCampaignItemPerPage;
		}
		set;
	}
	public List<CampaignMember> localExCampaign {get;set;}
	public Integer localExCampaignPage {get;set;}
	public Long localExCampaignTotalPage {get;set;}
	public Boolean disablePreviousLocalEx {get;set;}
	public Boolean disableNextLocalEx {get;set;}
	public List<Integer> localExPageList {get;set;}


	public String campaignPeriodMethod {get;set;}
	private Date campaignFromDate;
	private Date campaignToDate;

	private Integer massCampaignItemPerPage {
		get{
			if( massCampaignItemPerPage == null )
			{
				RTL_Campaign_History_Setting__mdt chMeta = [select id,MassDisplay__c
                   from RTL_Campaign_History_Setting__mdt 
                   where DeveloperName = 'Campaign_Display_Setting'];

                massCampaignItemPerPage = integer.valueOf(chMeta.MassDisplay__c);
			}
			return massCampaignItemPerPage;
		}
		set;
	}

	//public List<CampaignMember> massCampaign {get;set;}
	public List<MassCampaignData> massCampaign {get;set;}
	public Integer massCampaignPage {get;set;}
	public Long massCampaignTotalPage {get;set;}
	public Boolean disablePreviousMass {get;set;}
	public Boolean disableNextMass {get;set;}
	public List<Integer> massPageList {get;set;}

	public Boolean isRenderCampaign  {
		get{
			if( isRenderCampaign == null )
			{
				isRenderCampaign = false;
			}
			return isRenderCampaign;
		}
		set;
	}
	public Boolean isRenderSMS {
		get{
			if( isRenderSMS == null )
			{
				isRenderSMS = false;
			}
			return isRenderSMS;
		}
		set;
	}
	public Boolean isRenderFulfillment {
		get{
			if( isRenderFulfillment == null )
			{
				isRenderFulfillment = false;
			}
			return isRenderFulfillment;
		}
		set;
	}
	public RTL_CampaignHistoryExtension(ApexPages.StandardController stdController) {
		accountId = stdController.getRecord().id;
		try {
			accountObj = [Select ID,Name FROM Account WHERE id =: accountId ];
		} catch(Exception e) {
			System.debug(e.getMessage());
		}

		isRenderCampaign = true;
		campaignPeriodMethod = 'current';

		exclusiveCampaignPage = 1; 
		localExCampaignPage = 1;
		massCampaignPage = 1;
		customerRequestCampaignPage = 1;

		setCampaignPeriod();
		
	}

	public void setCampaignPeriod()
	{	
		if( campaignPeriodMethod == 'past' )
		{
			campaignFromDate = date.today().addMonths(-24);
			campaignToDate = date.today().addMonths(-6);
		}
		else 
		{
			campaignFromDate = date.today().addMonths(-6);
			campaignToDate = date.today();
		}

		queryExclusive(exclusiveCampaignPage);
		queryLocalEx(localExCampaignPage);
		queryMass(massCampaignPage);
		queryCustomerRequest(customerRequestCampaignPage);
	}

	private void queryCustomerRequest(Integer pageNo)
	{
	
		String countQuery ='SELECT Count() from CampaignMember '+
		'WHERE Campaign.RecordType.DeveloperName = :exclusiveCampaignRecordTypeDevName '+
		'AND Customer__c = :accountId '+
		'AND ( Campaign.status != \'On Hold\' OR RTL_Contact_Staff_Name__c != NULL ) ' +
		'AND Campaign.StartDate >= :campaignFromDate '+
		'AND Campaign.StartDate <= :campaignToDate ' +
		'AND Campaign.RTL_Campaign_Objective_child__c = \'Drop-Lead\' '
		;

		Integer countTotalRecords = Database.countQuery(countQuery);

		if( countTotalRecords == 0 || accountId == null )
		{
			disablePreviousCustomerRequeste = true;
			disableNextCustomerRequest = true;
			customerRequestCampaign = null;

			customerRequestCampaignPage = 1;
			customerRequestCampaignTotalPage = 1;
			customerRequestPageList = new List<Integer>();

		}
		else 
		{
			Integer rowLimit = customerRequestCampaignItemPerPage;
			Integer rowOffset = (pageNo-1)*rowLimit;

			customerRequestCampaignTotalPage = ((Decimal.valueOf(countTotalRecords) )/Decimal.valueOf(rowLimit)).round(System.RoundingMode.CEILING);
			
			customerRequestCampaign = [SELECT ID, Campaign.RTL_Campaign_Code_9_digits__c , Campaign.RTL_Campaign_Code_10_digits__c,
							Campaign.Name,Campaign.StartDate,Campaign.EndDate,Campaign.RecordType.DeveloperName,
							Campaign.status,Campaign.RTL_Campaign_Channel__c,RTL_Offer_Result__c,Campaign.RTL_Campaign_Channel_formula__c
							FROM CampaignMember
							WHERE Campaign.RecordType.DeveloperName in :exclusiveCampaignRecordTypeDevName
							AND Customer__c = :accountId
							AND ( Campaign.status != 'On Hold' OR RTL_Contact_Staff_Name__c != NULL )
							AND Campaign.StartDate >= :campaignFromDate
							AND Campaign.StartDate <= :campaignToDate
							AND Campaign.RTL_Campaign_Objective_child__c = 'Drop-Lead'
							ORDER BY CreatedDate DESC
							LIMIT :rowLimit OFFSET :rowOffset
							];

			disablePreviousCustomerRequeste = false;
			disableNextCustomerRequest = false;

			if( pageNo == 1)
			{
				disablePreviousCustomerRequeste = true;

			}

			if( pageNo*rowLimit >= countTotalRecords )
			{
				disableNextCustomerRequest = true;
			}


			customerRequestPageList = new List<Integer>();
			Integer count = 1;
			customerRequestPageList.add(count);
			while ( count*rowLimit < countTotalRecords ) {
				count++;
			    customerRequestPageList.add(count);
			}
		}
	}

	private void queryExclusive(Integer pageNo)
	{
		String countQuery ='SELECT Count() from CampaignMember '+
		'WHERE Campaign.RecordType.DeveloperName = :exclusiveCampaignRecordTypeDevName '+
		'AND Customer__c = :accountId '+
		'AND ( Campaign.status != \'On Hold\' OR RTL_Contact_Staff_Name__c != NULL ) ' +
		'AND Campaign.StartDate >= :campaignFromDate '+
		'AND Campaign.StartDate <= :campaignToDate ' +
		'AND Campaign.RTL_Campaign_Objective_child__c != \'Drop-Lead\' '
		;

		Integer countTotalRecords = Database.countQuery(countQuery);

		if( countTotalRecords == 0 || accountId == null )
		{
			disablePreviousExclusive = true;
			disableNextExclusive = true;
			exclusiveCampaign = null;

			exclusiveCampaignPage = 1;
			exclusiveExCampaignTotalPage = 1;
			exclusivePageList = new List<Integer>();

		}
		else 
		{
			Integer rowLimit = exclusiveCampaignItemPerPage;
			Integer rowOffset = (pageNo-1)*rowLimit;

			exclusiveExCampaignTotalPage = ((Decimal.valueOf(countTotalRecords) )/Decimal.valueOf(rowLimit)).round(System.RoundingMode.CEILING);
			
			exclusiveCampaign = [SELECT ID, Campaign.RTL_Campaign_Code_9_digits__c , Campaign.RTL_Campaign_Code_10_digits__c,
							Campaign.Name,Campaign.StartDate,Campaign.EndDate,Campaign.RecordType.DeveloperName,
							Campaign.status,Campaign.RTL_Campaign_Channel__c,RTL_Offer_Result__c,Campaign.RTL_Campaign_Channel_formula__c
							FROM CampaignMember
							WHERE Campaign.RecordType.DeveloperName in :exclusiveCampaignRecordTypeDevName
							AND Customer__c = :accountId
							AND ( Campaign.status != 'On Hold' OR RTL_Contact_Staff_Name__c != NULL )
							AND Campaign.StartDate >= :campaignFromDate
							AND Campaign.StartDate <= :campaignToDate
							AND Campaign.RTL_Campaign_Objective_child__c != 'Drop-Lead'
							ORDER BY CreatedDate DESC
							LIMIT :rowLimit OFFSET :rowOffset
							];

			disablePreviousExclusive = false;
			disableNextExclusive = false;

			if( pageNo == 1)
			{
				disablePreviousExclusive = true;

			}

			if( pageNo*rowLimit >= countTotalRecords )
			{
				disableNextExclusive = true;
			}


			exclusivePageList = new List<Integer>();
			Integer count = 1;
			exclusivePageList.add(count);
			while ( count*rowLimit < countTotalRecords ) {
				count++;
			    exclusivePageList.add(count);
			}
		}

	}

	private void queryLocalEx(Integer pageNo)
	{
		String countQuery ='SELECT Count() from CampaignMember '+
		'WHERE Campaign.RecordType.DeveloperName = :localExCampaignRecordTypeDevName '+
		'AND Customer__c = :accountId '+
		'AND ( Campaign.status != \'On Hold\' OR RTL_Contact_Staff_Name__c != NULL ) ' +
		'AND Campaign.StartDate >= :campaignFromDate '+
		'AND Campaign.EndDate <= :campaignToDate'
		;

		Integer countTotalRecords = Database.countQuery(countQuery);

		if( countTotalRecords == 0 || accountId == null )
		{
			disablePreviousLocalEx = true;
			disableNextLocalEx = true;
			localExCampaign = null;

			localExCampaignPage = 1;
			localExCampaignTotalPage = 1;
			localExPageList = new List<Integer>();

		}
		else 
		{
			Integer rowLimit = localExCampaignItemPerPage;
			Integer rowOffset = (pageNo-1)*rowLimit;

			localExCampaignTotalPage = ((Decimal.valueOf(countTotalRecords) )/Decimal.valueOf(rowLimit)).round(System.RoundingMode.CEILING);
			
			localExCampaign = [SELECT ID, Campaign.RTL_Campaign_Code_9_digits__c , Campaign.RTL_Campaign_Code_10_digits__c,
							Campaign.Name,Campaign.StartDate,Campaign.EndDate,Campaign.RecordType.DeveloperName,
							Campaign.status,Campaign.RTL_Campaign_Channel__c,RTL_Offer_Result__c,Campaign.RTL_Campaign_Channel_formula__c
							FROM CampaignMember
							WHERE Campaign.RecordType.DeveloperName in :localExCampaignRecordTypeDevName
							AND Customer__c = :accountId
							AND ( Campaign.status != 'On Hold' OR RTL_Contact_Staff_Name__c != NULL )
							AND Campaign.StartDate >= :campaignFromDate
							AND Campaign.StartDate <= :campaignToDate
							ORDER BY CreatedDate DESC
							LIMIT :rowLimit OFFSET :rowOffset
							];
		

			disablePreviousLocalEx = false;
			disableNextLocalEx = false;

			if( pageNo == 1)
			{
				disablePreviousLocalEx = true;

			}

			if( pageNo*rowLimit >= countTotalRecords )
			{
				disableNextLocalEx = true;
			}


			localExPageList = new List<Integer>();
			Integer count = 1;
			localExPageList.add(count);
			while ( count*rowLimit < countTotalRecords ) {
				count++;
			    localExPageList.add(count);
			}
		}

	}


	private void queryMass(Integer pageNo)
	{

		massCampaign = new List<massCampaignData>();

		String countQuery ='SELECT Count() FROM Campaign '+
							'WHERE RecordType.DeveloperName in :massCampaignRecordTypeDevName '+
							'AND status != \'On Hold\' '+
							'AND IsActive = true ' +
							'AND StartDate >= :campaignFromDate '+
							'AND StartDate <= :campaignToDate'
							;

		Integer countTotalRecords = Database.countQuery(countQuery);

		if( countTotalRecords == 0 || accountId == null )
		{
			disablePreviousMass = true;
			disableNextMass = true;
			massCampaign = null;

			massCampaignPage = 1;
			massCampaignTotalPage = 1;
			massPageList = new List<Integer>();

		}
		else 
		{
			Integer rowLimit = massCampaignItemPerPage;
			Integer rowOffset = (pageNo-1)*rowLimit;

			massCampaignTotalPage = ((Decimal.valueOf(countTotalRecords) )/Decimal.valueOf(rowLimit)).round(System.RoundingMode.CEILING);
			

			Map<ID,Campaign> massCamList =  new Map<ID, Campaign>([SELECT ID,RTL_Campaign_Code_10_digits__c,
						Name,StartDate,EndDate,status,RTL_Campaign_Channel__c,RTL_Campaign_Channel_formula__c
						FROM Campaign
						WHERE RecordType.DeveloperName in :massCampaignRecordTypeDevName
						AND status != 'On Hold'
						AND IsActive = true
						AND StartDate >= :campaignFromDate
						AND StartDate <= :campaignToDate
						LIMIT :rowLimit OFFSET :rowOffset
						]);

			

			List<CampaignMember> cmList = [ SELECT ID,Name,RTL_Offer_Result__c,CampaignId
					FROM CampaignMember 
					WHERE CampaignId in :massCamList.keyset() 
					AND Customer__c = :accountId
					];


			MAP<ID,CampaignMember> campaignWithMember = new MAP<ID,CampaignMember>();
			for( CampaignMember cm : cmList )
			{
				if( !campaignWithMember.containsKey(cm.CampaignId) )
				{
					campaignWithMember.put(cm.CampaignId,cm);
				}
			}

			for ( ID idKey : massCamList.keyset() ) 
			{
				Campaign c = massCamList.get(idKey);

				MassCampaignData mcd = new MassCampaignData();
				mcd.camObj = c;
				mcd.camMemObj = campaignWithMember.get(idKey);

				massCampaign.add(mcd);
			}

			disablePreviousMass = false;
			disableNextMass = false;

			if( pageNo == 1)
			{
				disablePreviousMass = true;

			}

			if( pageNo*rowLimit >= countTotalRecords )
			{
				disableNextMass = true;
			}


			massPageList = new List<Integer>();
			Integer count = 1;
			massPageList.add(count);
			while ( count*rowLimit < countTotalRecords ) {
				count++;
			    massPageList.add(count);
			}
		}

	}

	public PageReference previousCustomerRequest()
	{
		customerRequestCampaignPage--;
		queryCustomerRequest(customerRequestCampaignPage);
		return null;
	}

	public PageReference nextCustomerRequest()
	{
		customerRequestCampaignPage++;
		queryCustomerRequest(customerRequestCampaignPage);
		return null;
	}

	public PageReference navigateCustomerRequest()
	{
		queryCustomerRequest(customerRequestCampaignPage);
		return null;
	}

	public PageReference previousExclusive()
	{
		exclusiveCampaignPage--;
		queryExclusive(exclusiveCampaignPage);
		return null;
	}

	public PageReference nextExclusive()
	{
		exclusiveCampaignPage++;
		queryExclusive(exclusiveCampaignPage);
		return null;
	}

	public PageReference navigateExclusive()
	{
		queryExclusive(exclusiveCampaignPage);
		return null;
	}

	public PageReference previousLocalEx()
	{
		localExCampaignPage--;
		queryLocalEx(localExCampaignPage);
		return null;
	}

	public PageReference nextLocalEx()
	{
		localExCampaignPage++;
		queryLocalEx(localExCampaignPage);
		return null;
	}

	public PageReference navigateLocalEx()
	{
		queryLocalEx(localExCampaignPage);
		return null;
	}

	public PageReference previousMass()
	{
		massCampaignPage--;
		queryMass(massCampaignPage );
		return null;
	}

	public PageReference nextMass()
	{
		massCampaignPage++;
		queryMass(massCampaignPage );
		return null;
	}

	public PageReference navigateMass()
	{
		queryMass(massCampaignPage );
		return null;
	}

	public void displayData()
	{
		String tabName = Apexpages.currentPage().getParameters().get('tabName');



		if( tabName == 'campaign' )
		{
			system.debug(tabName);
			if( isRenderCampaign == false )
			{
				// Reder data for SMS
			}
			isRenderCampaign = true;
		}
		else if ( tabName == 'sms' )
		{
			system.debug(tabName);
			if( isRenderSMS == false )
			{
				// Reder data for SMS
			}
			isRenderSMS = true;
		}
		else if( tabName == 'fulfillment' )
		{
			system.debug(tabName);
			if( isRenderFulfillment == false )
			{
				// Reder data for fulfillment
			}
			isRenderFulfillment = true;
		}

	}

}