@isTest
private class RTL_CampaignHistoryExtensionTest {

	/*
	Pending for test class , need to understand and go through the Campaign create process
	*/	
	@testSetup static void methodName() {

		TestUtils.createAppConfig();

		List<Account> accs = TestUtils.createAccounts(1,'Master Test','Individual',true);

		RecordType rc = [SELECT id,developerName FROM recordType WHERE developerName = 'Mass_Campaign_Active' ];

		List<Campaign> masterCamListLatest = TestUtils.createCampaign(1,'MB',false);
		masterCamListLatest[0].Name = 'Master Cam';
		masterCamListLatest[0].RTL_Campaign_Code_9_digits__c  = 'MS0000000';
		masterCamListLatest[0].StartDate  = Date.Today();
		masterCamListLatest[0].EndDate  = Date.Today().addDays(7);
		insert masterCamListLatest;

		List<Campaign> camListLatest = TestUtils.createCampaign(3,'MB',false);

		final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';

		Integer i = 0;
		for(Campaign c : camListLatest )
		{
			//camList.Customer__c = accs[0].id;	
			c.StartDate  = Date.Today();
			c.EndDate  = Date.Today().addDays(7);
			c.RecordTypeId = rc.id;
			//c.RTL_Campaign_Code_10_digits__c = 'MS0000000' + chars.substring(i, i+1);
			c.ParentId = masterCamListLatest[0].id;
			i++;
		}

		insert camListLatest;

		List<CampaignMember> camMemList = new List<CampaignMember>();

		for (Campaign c : camListLatest)
		{

			CampaignMember cm = new CampaignMember();
			cm.CampaignId = c.id;
			cm.Customer__c = accs[0].id;
			//cm.Name = 'Test Test';
			//cm.RTL_Offer_Result__c = ''; 
			camMemList.add(cm);
		}
		
		insert camMemList;


	}

	@isTest static void test_method_one() {

		Account acc = [SELECT ID FROM Account LIMIT 1];

		ApexPages.StandardController sc = new ApexPages.StandardController(acc);
		RTL_CampaignHistoryExtension che = new RTL_CampaignHistoryExtension(sc);
                
        PageReference pageRef = Page.RTL_CampaignHistory;
        pageRef.getParameters().put('id', String.valueOf(acc.Id));
        Test.setCurrentPage(pageRef);

        Test.startTest();

        che.navigateLocalEx();
        che.nextLocalEx();
        che.previousLocalEx();

        che.navigateMass();
        che.nextMass();
        che.previousMass();

        che.campaignPeriodMethod  = 'past';
        che.setCampaignPeriod();

        Test.stopTest();
		



		

	}
	

	
}