global class RTL_CampaignMemberAssignEmailErrorLog implements Schedulable {
	global void execute(SchedulableContext sc) {
		String query = 'SELECT Id, Name FROM CampaignMember where RTL_Assigned_Channel__c = \'TMB Admin\'';
        RTL_BatchCMAssignEmailErrorLog logPurge = new RTL_BatchCMAssignEmailErrorLog(query,'TMB Admin');
        Id BatchProcessId = Database.ExecuteBatch(logPurge);
	}
}