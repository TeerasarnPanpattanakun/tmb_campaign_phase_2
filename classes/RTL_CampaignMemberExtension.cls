public without sharing class RTL_CampaignMemberExtension {

    public CampaignMember currentObject {get;set;}
    public Lead objLead {get;set;}
    public String contactName {get;set;}
    private String LABEL_INTEREST = 'Interested';
    private Map<String,RTL_NBO_Product_Record_Type_Mapping__mdt> mapConfRecordType = new Map<String,RTL_NBO_Product_Record_Type_Mapping__mdt>();
    private Map<String,RecordType> mapRecordType = new Map<String,RecordType>();
    private User usInfo = [ select Id,RTL_Channel__c,Region__c,Zone__c,RTL_Branch__c 
                            from User 
                            where Id = :UserInfo.getUserId() limit 1 ];
    public List<CampaignProductInterestedWrapper> createOppList {
        get
        {
            if(createOppList == null){
                createOppList = new List<CampaignProductInterestedWrapper>();
            }
            return createOppList;
            
        }set;
    }
    public List<Campaign> activeCampaignList {
        get
        {
            if(activeCampaignList == null){
                activeCampaignList = new List<Campaign>();
            }
            return activeCampaignList;
            
        }set;
    }
    List<Opportunity> oppList = new List<Opportunity>();    
    ApexPages.standardController m_sc = null;

    public List<FulfillmentWrapper> fulfillmentList {
        get
        {
            if(fulfillmentList == null){
                fulfillmentList = new List<FulfillmentWrapper>();
            }
            return fulfillmentList;
            
        }set;
    }
    public static User currentUser{
    get{
        if(currentUser ==null){
            currentUser = [SELECT Id,Name,Employee_ID__c ,Segment__c ,Reporting_Segment__c ,
                                    Region__c ,Region_Code__c ,Zone__c ,Zone_Code__c ,RTL_Branch_Code__c ,
                                    RTL_Branch__c ,RTL_Channel__c,UserRole.Name,Profile.Name
                            FROM User
                           WHERE Id =: System.UserInfo.getUserId()];
        }
        return currentUser;
        
    }set;}

    private Boolean nextStepCreateOpp = false;
    //private Boolean nextStepViewOpp = false;
    public boolean isSaveSucess{get;set;}
    public boolean isRetailUser{get;set;}
    public boolean isCampaignActive{get;set;}
    private String saveErrorMessage = '';

    public RTL_CampaignMemberExtension(ApexPages.StandardController stdController) {
        m_sc = stdController;
        objLead = new Lead();
        currentObject = (CampaignMember)stdController.getRecord();
        currentObject = getCampaignMember(currentObject.Id).get(0);
        //currentobject.RTL_Contact_Status__c = null;
        isRetailUser = false;
        isCampaignActive = false;

        if( currentObject.LeadId != null ){
            objLead = getLead(currentObject.LeadId);
        }    
        for(RTL_NBO_Product_Record_Type_Mapping__mdt each : getConfRecordType() ){
            mapConfRecordType.put(each.RTL_Product_Group__c,each);
        }
        for(RecordType each : getRecordTypeOpportunity() ){
            mapRecordType.put(each.DeveloperName,each);
        }

        system.debug('Contact - '+currentObject.RTL_TMB_Cust_ID__c+' : '+currentObject.Campaign.RTL_Campaign_Code_10_digits__c);

        if(currentObject.RTL_TMB_Cust_ID__c != null && currentObject.Campaign.RTL_Campaign_Code_10_digits__c != null){
            fulfillmentList = getFulfillmentList(currentObject.RTL_TMB_Cust_ID__c ,currentObject.Campaign.RTL_Campaign_Code_10_digits__c);
        }

        if(currentUser.Profile.Name.containsIgnoreCase('Retail') || currentUser.Profile.Name.containsIgnoreCase('System')){
            isRetailUser = true;
        }

        if(currentobject.Campaign != null){
            activeCampaignList = getActiveCampaign(currentobject.campaignid);
            system.debug('CPM : campaign '+activeCampaignList);
            if(activeCampaignList.size() > 0){
                isCampaignActive = true;
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,System.Label.RTL_CampaignMemberEdit_ERR003));    
            }
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,System.Label.RTL_CampaignMemberEdit_ERR003)); 
        }

    }

    private void setLeadOwner()
    {

        if( currentObject.LeadId != null && !objLead.IsConverted )
        {
            

            //Check if Owner is not User
            List<User> userOwner = [SELECT id FROM User WHERE id=:objLead.OwnerId];

            if( userOwner.size() == 0 )
            {
                //Set Lead Owner to  Current User
                objLead.OwnerId = UserInfo.getUserId();
            }
        }
    }

    //public PageReference apexSave(){
    public Object apexSave(){

        saveErrorMessage = '';

        // Set Lead Ownder
        setLeadOwner();

        Savepoint sp = Database.setSavepoint();         
        currentobject.RTL_Contact_Staff_Name__c = usInfo.Id;
        currentobject.RTL_Contact_Region_Group_Head__c = usInfo.Region__c;
        currentobject.RTL_Contact_Zone_Group__c = usInfo.Zone__c;
        currentobject.RTL_Contact_Branch_Team_Name_Code__c = usInfo.RTL_Branch__c;


        nextStepCreateOpp = false;
        for( Integer i = 1 ; i <= 5 ; i++ ){
            // Check if offer result is interested and not create opportunity yet
            if( currentObject.get('RTL_OfferResult_Product_'+i+'__c') == LABEL_INTEREST 
                &&  currentObject.get('RTL_RelatedOpportunity_'+i+'__c') == null ){
                nextStepCreateOpp = true;
                break;
            }
        }
        if( nextStepCreateOpp ){
            //return nextPageCreateOpportunity();
            isSaveSucess = false;
            return null;
        }


        try{
            updateCampaignMember();
            
            if( currentObject.LeadId != null && !objLead.IsConverted ){

                //Check offer result status 
                //Boolean allNotInterested = true;
                //Boolean hasOfferProduct = false;
                //for (Integer count = 1; count <= 5; count++) {
                //    String offerResult = String.valueOf(currentObject.get('RTL_OfferResult_Product_'+count+'__c' ));
                //    String productGroup = String.valueOf(currentObject.get('RTL_Product_Group_'+count+'__c' ));

                //    //system.debug(Logginglevel.ERROR,offerResult);
                //    //system.debug(Logginglevel.ERROR,productGroup);

                //    if( productGroup == '' )
                //    {
                //        // No offer product - skip
                //    }
                //    else if ( productGroup != '' && offerResult == 'Not Interested' )
                //    {
                //        hasOfferProduct = true;
                //    }
                //    else if ( productGroup != '' && offerResult != 'Not Interested' )
                //    {
                //        hasOfferProduct = true;
                //        allNotInterested = false;
                //        break;
                //    }

                //}
                
                //if( hasOfferProduct == true &&  allNotInterested == true )
                //{
                //    objLead.Status = 'Unqualified';
                //}
                //else if( currentobject.RTL_Contact_Status__c == 'Contact' ){
                //    objLead.Status = 'Contacted';
                //}

                if( currentobject.RTL_Contact_Status__c == 'Contact' ){
                    objLead.Status = 'Contacted';
                }

                update objLead;
            }

            isSaveSucess = true;
            system.debug('isSaveSucess'+isSaveSucess);
            return null;
            
            
        }catch( DMLEXception e ){
            Database.rollback(sp);
            //System.debug(Logginglevel.ERROR,e.getMessage());
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, e.getMessage() +' ['+e.getLineNumber()+']' ));
            
            Integer numErrors = e.getNumDml();

            for(Integer i=0;i<numErrors;i++) {
                saveErrorMessage += e.getDmlMessage(i); 
            }

            system.debug('CPM : Update - '+saveErrorMessage+e.getMessage() +' ['+e.getLineNumber()+']'+e.getStackTraceString());
            isSaveSucess = false;
            return null;
            
        }       

        //PageReference nextPage =  new PageReference('/'+currentObject.Id+'?isdtp=vw'); 
        //nextPage.setRedirect(true);    
        //return nextPage;
        //return null;
    }

    // PageReference method to perform redirection base on save logic
    public PageReference redirectAfterSave(){
        system.debug('isSaveSucess'+isSaveSucess);

        if( isSaveSucess )
        {
            /*ageReference nextPage =  new PageReference('/'+currentObject.Id+'?isdtp=vw'); 
            nextPage.setRedirect(true);    
            return nextPage;*/
            currentObject = getCampaignMember(currentObject.Id).get(0);  
            return Page.RTL_ViewCampaignMember;
        }
        else 
        {
            if( nextStepCreateOpp )
            {
                nextStepCreateOpp = false;
                return nextPageCreateOpportunity();
            }

            if( saveErrorMessage != '' )
            {
                system.debug('CRM : saveErrorMessage - '+ saveErrorMessage);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, System.Label.RTL_CampaignMemberEdit_ERR002 +saveErrorMessage));            
            }

            return null;    
        }

        

    }

    public PageReference apexCreateOpportunity(){
        Savepoint sp = Database.setSavepoint();
        oppList = new List<Opportunity>();  
        Id accId = currentObject.Contact.AccountId;
        Id conId;
        try{          
            updateCampaignMember();  
        }catch( Exception e ){
            Database.rollback(sp);
            System.debug('CRM : '+e.getMessage());
             ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, System.Label.RTL_CampaignMemberEdit_ERR001));            
            return null;
        }

        try{
            System.debug('update lead : '+objLead);    

            if( objLead.Id != null && !objLead.IsConverted ){

                System.debug('convert step');
                Boolean chkDup = false;
                // FOR SKIP CHECK UP CONVERT LEAD
                objLead.RTL_Allow_Convert_Lead__c = true;
                objLead.Status = 'Qualified';

                if( !validateLead(objLead,objLead) ){
                    Database.rollback(sp);
                    return null;
                }

                system.debug('objLead'+objLead);

                update objLead;

                Map<String,Account> accountSet = new Map<String,Account>();
                for(Account acct : [Select Id,ID_Type_PE__c, ID_Number_PE__c, RTL_NID__c from Account where RTL_NID__c = :getNID(objLead.RTL_Citizen_Id__c)]){
                    accountSet.put(acct.ID_Type_PE__c+acct.ID_Number_PE__c,acct);    
                }

                if( accountSet.containsKey(objLead.RTL_ID_Type__c + objLead.RTL_Citizen_Id__c) ){
                    System.debug('dup account');
                    chkDup = true;
                    accId = accountSet.get(objLead.RTL_ID_Type__c + objLead.RTL_Citizen_Id__c).Id;

                    List<Contact> ExistingConList = [SELECT Id,FirstName,LastName,AccountID
                                                FROM Contact WHERE AccountID =: accId
                                                ];
                    for(contact existingCon : ExistingConList){
                        conId = existingCon.Id;                            
                    }
                    System.debug('accId : '+accId);
                    System.debug('contId : '+conId);

                }

                                       

                LeadStatus convertStatus = [SELECT Id, MasterLabel 
                FROM LeadStatus WHERE IsConverted=true and Masterlabel = 'Closed Converted' LIMIT 1];

                Database.LeadConvert lc = new database.LeadConvert();
                lc.setDoNotCreateOpportunity(true);                
                lc.setLeadId(objLead.id);
                if( chkDup ){ 
                    lc.setAccountId(accId);
                    lc.setContactId(conId);
                }
                
                lc.setConvertedStatus(convertStatus.MasterLabel);
                Database.LeadConvertResult lcr;
                lcr = Database.convertLead(lc);
                accId = lcr.getAccountId();
            }                    
        }catch( Exception e ){
            Database.rollback(sp);
            System.debug('CRM : '+e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, System.Label.RTL_CampaignMemberEdit_ERR001 ));
            return null;
        }

        // create opp line

        if( createOppList.size() > 0 ){            
            for( CampaignProductInterestedWrapper each : createOppList ){
                Opportunity newOpp = new Opportunity();
                newOpp = each.objOpp;
                newOpp.RTL_Product_Group__c = String.valueOf(currentObject.get(each.apiProductGroup));
                newOpp.RTL_Prod_SubGrp__c = String.valueOf(currentObject.get(each.apiProductSubGroup));
                newOpp.RTL_Product_Name__c = String.valueOf(currentObject.get(each.apiProductId));                
                newOpp.AccountId = accId;    
                newOpp.CampaignId = currentObject.CampaignId;
                newOpp.RTL_Campaign_Code__c = currentObject.Campaign.RTL_Campaign_Code_10_digits__c;
                newOpp.RTL_Campaign_Name__c = currentObject.Campaign.Name;
                newOpp.RTL_Campaign_Start_Date__c = currentObject.Campaign.RTL_Campaign_Start_Date__c;              
                System.debug('create opp : '+newOpp);            
                oppList.add(newOpp);
            }
        }

        try{
            if( oppList.size() > 0 ){
                insert oppList;
            }
        }catch( DMLEXception e){
            Database.rollback(sp);
            System.debug('CRM : '+e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, System.Label.RTL_CampaignMemberEdit_ERR001));
            return null;
        }


        //stamp opp to related opp in campaign member
        if( oppList.size() > 0 ){
            for(Opportunity objOpp:oppList){
                
                system.debug('CPM : insert opp : '+objOpp);//list opp
                system.debug('CPM : campaign member update opp : '+currentObject);//campaign member
                for(integer i = 1; i <= 5; i++){
                    Id campaignProduct = (id) currentObject.get('RTL_Campaign_Product_'+i+'__c');
                    system.debug(campaignProduct);
                    if(campaignProduct!=null && campaignProduct.equals(objOpp.RTL_Product_Name__c)){
                        currentObject.put('RTL_RelatedOpportunity_'+i+'__c',objOpp.id); 
                        if(currentObject.get('RTL_Reason_'+i+'__c') != null){
                            currentObject.put('RTL_Reason_'+i+'__c',null); 
                        }
                        break;
                    }
                }
            }
       

            try{
                 update currentObject;
            }catch( Exception e ){
                Database.rollback(sp);
                System.debug(e.getMessage());
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, System.Label.RTL_CampaignMemberEdit_ERR001 ));
                return null;
            }

         }


        //nextStepViewOpp = true;
        //return null;
        return nextPageViewOpportunity();


        //PageReference nextPage =  new PageReference('/'+currentObject.Id+'?isdtp=vw'); 
        //nextPage.setRedirect(true);    
        //return nextPage;
    }

    public void updateCampaignMember(){
        currentobject.No_of_Attempts__c = (currentobject.No_of_Attempts__c==null) ? 0 : currentobject.No_of_Attempts__c;
        currentobject.No_of_Attempts__c = ( currentobject.RTL_Contact_Status__c == 'Re-attempt' ) ? currentobject.No_of_Attempts__c+1 : currentobject.No_of_Attempts__c;
        String contactStatus = (currentobject.RTL_Contact_Status__c==null) ? '' : currentobject.RTL_Contact_Status__c;
        String contactStatusReason = (currentobject.RTL_Reason__c==null) ? '' : currentobject.RTL_Reason__c;        
        currentobject.RTL_Last_Contact_Status__c = RTL_Utility.getLabelPicklist(contactStatus,'RTL_Contact_Status__c','CampaignMember')+' '
        +RTL_Utility.getLabelPicklist(contactStatusReason,'RTL_Reason__c','CampaignMember')+' '+System.now().format();
        update currentobject;
    }

    public String getNID (String idNumber) {
        String nid;
        if (idNumber != null && idNumber.length() > 0) {
            nid = (idNumber.length() <= 5) ?  idNumber: idNumber.substring(0,1)+idNumber.substring(idNumber.length()-4); 
        }
        return nid;
    }

    public PageReference backPageEditCampaignMember(){
        // reset value
        for( CampaignProductInterestedWrapper each : createOppList ){
            if( !each.viewSubGroup ){
                currentobject.put(each.apiProductSubGroup,null);
            }
            if( !each.viewProductName ){
                currentobject.put(each.apiProductId,null);
            }
        }
        return Page.RTL_CampaignMemberEdit;
    }

    public PageReference nextPageCreateOpportunity(){
        // get new value
        createOppList = getCampaignProductInterested();
        return Page.RTL_CampaignMemberCreateOpportunity;
    }

    public PageReference nextPageViewOpportunity(){
        return Page.RTL_CampaignMemberViewOpportunity;
    }


    public PageReference editCampaignMemberButton(){
        if(isCampaignActive){
            currentobject.RTL_Contact_Status__c = null;
            return Page.RTL_CampaignMemberEdit;
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,System.Label.RTL_CampaignMemberEdit_ERR003)); 
            return Page.RTL_ViewCampaignMember;
        }
    }

    public PageReference cancel(){
        PageReference returnPage;
        try{
            returnPage = new PageReference('/'+currentObject.Id);
        }catch(Exception e){
            //returnPage = new PageReference('/'+currentObject.Id);  
        }
        
        return returnPage;
    }

    public PageReference canceltoview(){

       return Page.RTL_ViewCampaignMember;
    }
    
    public List<CampaignProductWrapper> getCampaignProduct(){
        List<CampaignProductWrapper> returnList = new List<CampaignProductWrapper>();
        Integer count = 1;
        for( Integer i = 1 ; i <= 5 ; i++ ){
            Boolean readOnly = (currentObject.get('RTL_RelatedOpportunity_'+i+'__c') == null) ? false : true;
            if( currentObject.get('RTL_Product_Group_'+i+'__c') != null
            || currentObject.get('RTL_Sub_Group_'+i+'__c') != null
            || currentObject.get('RTL_Campaign_Product_'+i+'__c') != null ){
                returnList.add( new CampaignProductWrapper(count++,
                'RTL_Product_Group_'+i+'__c',
                'RTL_Sub_Group_'+i+'__c',
                'RTL_Campaign_Product_'+i+'__c',
                'RTL_Campaign_Product_'+i+'__r.Name',
                'RTL_OfferResult_Product_'+i+'__c',
                'RTL_Reason_'+i+'__c',
                'RTL_RelatedOpportunity_'+i+'__c',
                readOnly
                ) );
            }
        }
        
        return returnList;
    }

    public List<CampaignProductInterestedWrapper> getCampaignProductInterested(){
        List<CampaignProductInterestedWrapper> returnList = new List<CampaignProductInterestedWrapper>();
        Integer count = 1;
        for( Integer i = 1 ; i <= 5 ; i++ ){
            Boolean viewSubGroup = (currentObject.get('RTL_Sub_Group_'+i+'__c') == null) ? false : true;
            Boolean viewProductName = (currentObject.get('RTL_Campaign_Product_'+i+'__c') == null) ? false : true;

            // Now get product to create opportunities only not converted product
            if( currentObject.get('RTL_OfferResult_Product_'+i+'__c') == LABEL_INTEREST
                &&  currentObject.get('RTL_RelatedOpportunity_'+i+'__c') == null ){
                Opportunity objOpp = new Opportunity();
                String productGroup = String.valueOf(currentObject.get('RTL_Product_Group_'+i+'__c'));
                if( mapConfRecordType.containsKey( productGroup ) ){
                    String recordTypeByProductGroup = mapConfRecordType.get( productGroup ).RTL_Record_Type_DevName__c;
                    objOpp.StageName = mapConfRecordType.get( productGroup ).Default_Stage__c;
                    if( mapRecordType.containsKey( recordTypeByProductGroup ) ){
                        objOpp.RecordTypeId = mapRecordType.get( recordTypeByProductGroup ).Id;                                                     
                    }                    
                }
                
                returnList.add( new CampaignProductInterestedWrapper(count++,
                'RTL_Product_Group_'+i+'__c',
                'RTL_Sub_Group_'+i+'__c',
                'RTL_Campaign_Product_'+i+'__c',
                'RTL_Campaign_Product_'+i+'__r.Name',
                objOpp,
                viewSubGroup,
                viewProductName             
                ) );                
            }
        }
        
        return returnList;
    }

    private Boolean validateLead(Lead leadObj, Lead oldLeadObj){ 
        Boolean returnChk = true;
        if (oldLeadObj.Status != 'Qualified'){//check if the lead status was Qualified
            returnChk = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, System.Label.LeadError_QualifiedBeforeConversion ));            
        }else {
            Boolean chkPrimary = false;
            List<RTL_Interested_products_c__c> interestProduct = [SELECT Id,Product_Name__c,Is_Primary__c FROM RTL_Interested_products_c__c 
                          WHERE Lead__c = :leadObj.Id order by Is_Primary__c desc];    
            if( interestProduct.size() > 0 ){                  
                /*for( RTL_Interested_products_c__c each : interestProduct ){
                    if( each.Is_Primary__c ){
                        chkPrimary = each.Is_Primary__c;
                    }
                }
                if( !chkPrimary ){
                    interestProduct.get(0).Is_Primary__c = true;
                    update interestProduct.get(0);
                }*/

                //Get product id for new opp
                Id productId = (Id) currentObject.get(createOppList.get(0).apiProductId);
                system.debug(interestProduct);

                //Check matching product on interested product lead
                for( RTL_Interested_products_c__c each : interestProduct ){

                    if( each.Is_Primary__c ){
                        chkPrimary = each.Is_Primary__c;
                    }
                    
                    if( !chkPrimary ){
                        if( productId!=null && each.Product_Name__c == productId){
                            each.Is_Primary__c = true;
                            chkPrimary = true;
                            system.debug(each);
                            break;
                        }
                    }
                }

                update interestProduct;

                //Not Matching in interested product lead
                if(!chkPrimary){
                    createInterestedProduct(leadObj);
                    system.debug('CPM : Create interested product when product not match.');
                }
                
            }else{
                createInterestedProduct(leadObj);
                system.debug('CPM : Create interested product when product not found.');
            }
        }
        return returnChk;
    }

    public void createInterestedProduct(Lead leadObj){
        system.debug('Campaign Member before convert : '+currentObject);
        system.debug('createOppList'+createOppList);

        if(createOppList.size() > 0){
            Id productId = (Id) currentObject.get(createOppList.get(0).apiProductId);
            system.debug('Product for new opp id : '+productid);
            if(productId != null){
                RTL_Interested_products_c__c interestedProduct = new RTL_Interested_products_c__c();
                interestedProduct.Is_Primary__c = true;
                interestedProduct.Lead__c = currentObject.LeadId;
                interestedProduct.Product_Name__c = productId;
                insert interestedProduct;
            }

        }
    }

    public List<Opportunity> getOpportunityList(){
        List<Opportunity> returnList = [select Id,Name,AccountId,Account.Name,RTL_Product_Name__r.Name,RTL_Product_Name__c,StageName,RTL_Status__c,Amount,CloseDate,Owner.Name
        from Opportunity where Id IN :oppList];
        if( returnList.size() > 0 ){
            contactName = returnList.get(0).Account.Name;
        }
        return returnList;
    }    

    public List<RTL_NBO_Product_Record_Type_Mapping__mdt> getConfRecordType(){
        return [select Id,DeveloperName,Default_Stage__c,RTL_Product_Group__c,RTL_Record_Type_DevName__c from RTL_NBO_Product_Record_Type_Mapping__mdt];
    }
    public List<RecordType> getRecordTypeOpportunity(){
        return [select Id,DeveloperName,Name from RecordType where SObjectType = 'Opportunity'];
    }
    public Lead getLead(Id inputId){
        return [select Id,Name,FirstName,LastName,RTL_Mobile_Number__c,RTL_ID_Type__c,RTL_Citizen_Id__c,Status,
        IsConverted,ConvertedAccountId,OwnerId
         from Lead where Id = :inputId];
    }

    public List<Campaign> getActiveCampaign(Id inputId){
        return [SELECT Id, Name, RecordTypeId, Status,Approvedflag__c 
                FROM Campaign 
                where status not in ('Expired','On Hold','Cancelled') 
                and Approvedflag__c = true 
                and Id = :inputId];
    }

    public List<CampaignMember> getCampaignMember(Id inputId){
        return (List<CampaignMember>)queryCampaignMember(inputId);
    }
    
    public List<SObject> queryCampaignMember(Id inputId){
        try{            
            String objectName = 'CampaignMember';
            // select other field
            String strQuery = 'select RTL_Contact_Staff_Name__r.Name,Contact.Name,Campaign.Name,Lead.Name,Contact.AccountId,Campaign.RTL_Campaign_Code_10_digits__c, ';
            strQuery += 'RTL_Campaign_Product_1__r.Name,RTL_Campaign_Product_2__r.Name,RTL_Campaign_Product_3__r.Name,RTL_Campaign_Product_4__r.Name,RTL_Campaign_Product_5__r.Name,Campaign.RTL_Campaign_Start_Date__c,Lead.IsConverted,Lead.ConvertedAccountId, Contact.account.name,';
            strQuery += queryAllField(objectName);
            strQuery = strQuery.substring(0,strQuery.length()-1);
            String strWhere = ' where id = :inputId ';
            strQuery += ' from '+objectName;
            strQuery += strWhere;
            System.debug('strQuery : '+strQuery);
            return Database.query(strQuery);
            
        }catch(QueryException e){
            return new List<SObject>();  
        }
    }

    public List<Fulfillment__c> getFulfillment(String cusid,String campaigncode){
        return (List<Fulfillment__c>)queryFulfillment(cusid,campaigncode);
    }

    public List<SObject> queryFulfillment(String cusid,String campaigncode){
        try{            
            String objectName = 'Fulfillment__c';
            // select other field
            String strQuery = 'select RTL_Campaign__r.RTL_Campaign_Code_10_digits__c,RTL_Campaign__r.Name, ';
            strQuery += queryAllField(objectName);
            strQuery = strQuery.substring(0,strQuery.length()-1);
            String strWhere = ' where ';
            // check TMB Customer ID and Campaign Code
            strWhere += 'RTL_Customer__r.TMB_Customer_ID_PE__c =: cusid and RTL_Campaign__r.RTL_Campaign_Code_10_digits__c =: campaigncode';
            strQuery += ' from '+objectName;
            strQuery += strWhere;
            System.debug('strQuery : '+strQuery);
            return Database.query(strQuery);
            
        }catch(QueryException e){
            return new List<SObject>();  
        }
    }

    public List<FulfillmentWrapper> getFulfillmentList(String cusid,String campaigncode){
        List<FulfillmentWrapper> returnList = new List<FulfillmentWrapper>();
        Integer count = 1;
        List<Fulfillment__c> fulfillmentlist = getFulfillment(cusid,campaigncode);
        system.debug('CPM : fulfillmentlist - '+fulfillmentlist);

        if(fulfillmentlist.size() > 0){
            for(Fulfillment__c f : fulfillmentlist){
                returnList.add(new FulfillmentWrapper(count,
                    f.RTL_Delivery_Date__c,
                    f.RTL_Campaign__r.RTL_Campaign_Code_10_digits__c,
                    f.RTL_Campaign__r.Name,
                    f.RTL_Fulfillment_Type__c,
                    f.RTL_Description__c,
                    f.RTL_Account_ID__c,
                    f.RTL_PCI_Card_NO__c,
                    f.RTL_Amount__c,
                    f.RTL_Fulfillment_Status__c));

                count++;
            }

        }

        return returnList;
    }

    public Static String queryAllField(String objName) {
        String strQuery = '';
        List<String> q = new List<String>();
        Map<String, Schema.SObjectType> m  = Schema.getGlobalDescribe() ;
        Schema.DescribeSObjectResult r =  m.get(objName).getDescribe();
        List<String>apiNames =  new list<String>();
        for(string apiName : r.fields.getMap().keySet()){
            q.add(apiName);
        }
        for( String s : q ){
            strQuery += s+',';
        }
        return strQuery;
    }

    public class CampaignProductWrapper{
        public Integer rowNum {get;set;}
        public String apiProductGroup {get;set;}
        public String apiProductSubGroup {get;set;}
        public String apiProductId {get;set;}
        public String apiProductName {get;set;}
        public String apiOfferResult {get;set;}
        public String apiReason {get;set;}
        public String apiOpportunity {get;set;}
        public Boolean readOnly {get;set;}
        public CampaignProductWrapper(Integer rowNum,String apiProductGroup,String apiProductSubGroup,String apiProductId,String apiProductName,
            String apiOfferResult,String apiReason,String apiOpportunity,Boolean readOnly){
            this.rowNum = rowNum;
            this.apiProductGroup = apiProductGroup;
            this.apiProductSubGroup = apiProductSubGroup;
            this.apiProductId = apiProductId;
            this.apiProductName = apiProductName;
            this.apiOfferResult = apiOfferResult;
            this.apiReason = apiReason;
            this.apiOpportunity = apiOpportunity;
            this.apiReason = apiReason;
            this.readOnly = readOnly;
        }
    }

    public class CampaignProductInterestedWrapper{
        public Integer rowNum {get;set;}
        public String apiProductGroup {get;set;}
        public String apiProductSubGroup {get;set;}
        public String apiProductId {get;set;}
        public String apiProductName {get;set;}
        public Opportunity objOpp {get;set;}
        public Boolean viewSubGroup {get;set;}
        public Boolean viewProductName {get;set;}
        
        public CampaignProductInterestedWrapper(Integer rowNum,String apiProductGroup,String apiProductSubGroup,
            String apiProductId,String apiProductName,Opportunity objOpp,Boolean viewSubGroup,Boolean viewProductName){
            this.rowNum = rowNum;
            this.apiProductGroup = apiProductGroup;
            this.apiProductSubGroup = apiProductSubGroup;
            this.apiProductId = apiProductId;
            this.apiProductName = apiProductName;
            this.objOpp = objOpp;
            this.viewSubGroup = viewSubGroup;
            this.viewProductName = viewProductName;
        }
    }

    public class FulfillmentWrapper{
        public Integer rowNum {get;set;}
        public Date apiProcessDate {get;set;}
        public String apiCampaignCode {get;set;}
        public String apiCampaignName {get;set;}
        public String apiFulfillmentType {get;set;}
        public String apiDescription {get;set;}
        public String apiAccountId {get;set;}
        public String apiCardNo {get;set;}
        public Decimal apiAmount {get;set;}
        public String apiStatus {get;set;}
        public FulfillmentWrapper(Integer rowNum,Date apiProcessDate,String apiCampaignCode,String apiCampaignName,String apiFulfillmentType,
            String apiDescription,String apiAccountId,String apiCardNo,Decimal apiAmount,String apiStatus){
            this.rowNum = rowNum;
            this.apiProcessDate = apiProcessDate;
            this.apiCampaignCode = apiCampaignCode;
            this.apiCampaignName = apiCampaignName;
            this.apiFulfillmentType = apiFulfillmentType;
            this.apiDescription = apiDescription;
            this.apiAccountId = apiAccountId;
            this.apiCardNo = apiCardNo;
            this.apiAmount = apiAmount;
            this.apiStatus = apiStatus;
        }
    }

}