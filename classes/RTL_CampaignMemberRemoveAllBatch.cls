global class RTL_CampaignMemberRemoveAllBatch implements Database.Batchable<sObject> ,Database.Stateful{
    
    // instance member to retain state across transactions
    global TimeZone tz = UserInfo.getTimeZone();
    global Integer recordsProcessed = 0;
    global Integer recordsSuccessful = 0;
    global Integer recordsFailed = 0;
    global Datetime batchDate = datetime.now();
    global Datetime batchStartTime = datetime.now();
    global Datetime batchEndTime = null;
    
    global final String query;
    public String CampaignID {get; set;}
    public Set<String> listIDforDel;
    public string campaignName {get;set;}
    public Integer amountDelCampaignMember {get;set;}
    
    global RTL_CampaignMemberRemoveAllBatch(Id cid) {//string q,
        CampaignID = cid;
        //query = q;
        amountDelCampaignMember = 0;
    }
    
    // Start Method
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //return Database.getQueryLocator(query); 
        return Database.getQueryLocator([Select Campaign_ID__c,
                                         Campaign_Member_ID__c ,
                                         Deleted__c,
                                         Deleted_Time__c,
                                         Marketing_Code__c
                                         from RTL_Deleted_Campaign_Member__c
                                         where Campaign_ID__c = :CampaignID
                                         order by Campaign_ID__c]);
    }
    
    // Execute Logic
    global void execute(Database.BatchableContext BC, List<RTL_Deleted_Campaign_Member__c> scope) {
    //global void execute(Database.BatchableContext BC, List<Campaignmember> scope) {
        System.Debug('TMB: -> RTL_CampaignMemberRemoveAllBatch start of execute');
        system.debug('scope :' + scope.size());
        Savepoint sp = Database.setSavepoint();
        listIDforDel = new Set<String>();
        boolean isDeleted = false;
        if (scope.size() > 0) {
            for(RTL_Deleted_Campaign_Member__c dc : scope){
                listIDforDel.add(dc.Campaign_Member_ID__c);
            }
            system.debug('listIDforDel size : '+listIDforDel.size());
            
            Map<id,Campaignmember> mapCampaignmember = new Map<ID,Campaignmember>([SELECT id,Campaignid,Campaign.name
                                                                                   FROM Campaignmember 
                                                                                   WHERE Campaignid = :CampaignID 
                                                                                   and id in:listIDforDel]);                        
            system.debug('Del size : '+mapCampaignmember.size());

            //Delete campaign member from temp object
            try {
                campaignName = mapCampaignmember.values().get(0).Campaign.name;//get campaign name
                delete mapCampaignmember.values();
                amountDelCampaignMember += mapCampaignmember.size();
                isDeleted = true;
            } catch (Exception e) {
                system.debug(e);
                isDeleted = false;
                Database.rollback(sp);
            }


            /*try {
                delete scope;
                isDeleted = true;
            } catch (Exception e) {
                system.debug(e);
                isDeleted = false;
                Database.rollback(sp);
            }

            if(isDeleted){
                for(Campaignmember c : scope){
                    campaignName = c.Campaign.name;
                }

                amountDelCampaignMember += scope.size();
            }*/
            
            //Update del time and flag.
            if (isDeleted) {
                for (RTL_Deleted_Campaign_Member__c dl : scope) {
                    if(!dl.Deleted__c){
                        Datetime current = DateTime.now();
                        dl.Deleted__c = true;
                        dl.Deleted_Time__c = current;
                    }
                }
                
                //Update Campaign Member History
                try {
                    update scope;
                    
                } catch (Exception e) {
                    system.debug(e);
                    Database.rollback(sp);
                }
            }else{
                try {
                    system.debug('scope del : '+scope.size());
                    delete scope;
                } catch (Exception e) {
                    system.debug(e);
                    Database.rollback(sp);
                }
            }
        }
        
        
        
        
        System.Debug('TMB: -> RTL_CampaignMemberRemoveAllBatch end of execute');
    }
    
    // Finish Job
    global void finish(Database.BatchableContext BC) {
        system.debug('Final Campaign Name : '+campaignName);
        system.debug('Final Campaign Member del : '+amountDelCampaignMember);

        batchEndTime = datetime.now();
        recordsProcessed = recordsSuccessful + recordsFailed;
        
        AsyncApexJob job = [SELECT Id, Status FROM AsyncApexJob WHERE Id = :bc.getJobId()];
        
        String userName = UserInfo.getUserName();
        User activeUser = [Select Email From User where Username = : userName limit 1];
        String userEmail = activeUser.Email;
        
        String subject = 'Campaing Member Delete Batch Success.';
        string mainUrl =  'https://' +  System.URL.getSalesforceBaseUrl().getHost() +'/ui/support/servicedesk/ServiceDeskPage#/'+ CampaignID;
        string htmlMsg =  'Campaign : '+campaignName
            + '<br/>Campaign Member delete : '+amountDelCampaignMember
            + '<br/>Campaing member delete batch processing is completed'
            + '<br/>Please click below url to view '
            + '<br/>View <a href="' + mainUrl + '"> click here</a>';

        if(amountDelCampaignMember > 0){
            
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] { userEmail };
                message.optOutPolicy = 'FILTER';
            message.subject = subject;
            message.setHtmlBody(htmlMsg);
            Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        }
        
        
        
        
    }
    
}