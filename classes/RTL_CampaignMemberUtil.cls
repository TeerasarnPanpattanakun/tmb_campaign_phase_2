public with sharing class RTL_CampaignMemberUtil {

	/**
	* Get Campaign Member Default Record Type
	**/
	private static String campaignMemberDefaultRecordTypeId {
        get{
                if(campaignMemberDefaultRecordTypeId==null){

                    try {
                        RTL_Campaign_Member_Default_Record_Type__mdt cmdrtId = [select id,
                        	RTL_Default_Record_Type_Name__c
                        	from RTL_Campaign_Member_Default_Record_Type__mdt 
                        	where MasterLabel = 'Default'];

                        String recordTypeName = cmdrtId.RTL_Default_Record_Type_Name__c;

                        RecordType rc = [SELECT Id
                        FROM RecordType
                        WHERE sObjectType = 'CampaignMember'
                        AND name = :recordTypeName
                        ];

                        campaignMemberDefaultRecordTypeId = rc.id;
                        
                    } catch(Exception e) {
                        System.debug(e.getMessage());
                        campaignMemberDefaultRecordTypeId = null;
                    }
            }
            return CampaignMemberDefaultRecordTypeId;
        }
        set;
    }

    /**
	* Set Campaign CampaignMemberRecordTypeId to default value
	* This method called on Campaign Trigger (Before Update , Before Insert)
	**/
    public static void setCampaignMemberRecordTypeId(List<Campaign> triggerNewCampaignList)
    {
    	
    	for(Campaign cam : triggerNewCampaignList)
    	{
    		cam.CampaignMemberRecordTypeId = campaignMemberDefaultRecordTypeId;
    	}

    }

    /**
	* Set Campaign Member's Offer product from MDM data (Long single String format)
	* This method called on Campaign Member Trigger (Before Update , Before Insert)
	**/
	public static void RTL_SaveProductsOffer(List<CampaignMember> triggernewCampaignMemberList){

        Set<String> retailProductsNameSet = new Set<String> ();
        Set<String> retailProductsSubGroupSet = new Set<String> ();

        Map<String,RTL_product_master__c> RetailProductMap = new Map<String,RTL_product_master__c >();
        Map<String,String> RetailProductSubGroupMap = new  Map<String,String>();

        for(CampaignMember camMem : triggernewCampaignMemberList){

            List<List<String>> productsOffer = RTL_UploadCampaignMemberCreateLead.processProductOfferString(camMem.RTL_Products_Offer_String__c);

            for( List<String> productItem : productsOffer  )
            {
                if( productItem.get(0) == '3' )
                {
                    retailProductsNameSet.add(productItem.get(1));
                }
                else if ( productItem.get(0) == '2' )
                {
                    retailProductsSubGroupSet.add(productItem.get(1));
                }
                
            }
        }

        // =================== OFFER PRODUCT==================================
        if(retailProductsNameSet.size()>0){
            for(RTL_product_master__c pm : [select Id,RTL_Product2_Name__c ,
                Product_Sub_group__c,Product_Group__c,Active__c,Name
                FROM RTL_product_master__c 
                WHERE Active__c = true 
                AND Name IN: retailProductsNameSet ]){
                RetailProductMap.put(pm.Name,pm);
            }
        }

        if(retailProductsSubGroupSet.size()>0){

            for(AggregateResult agResult : [select Product_Group__c,Product_Sub_group__c
                            from RTL_product_master__c 
                            where Active__c = true AND Product_Sub_group__c IN: retailProductsSubGroupSet 
                            GROUP BY Product_Group__c,Product_Sub_group__c 
                        ]){
                String subGroupName =  (String)agResult.get('Product_Sub_group__c');

                RetailProductSubGroupMap.put( subGroupName.tolowercase() ,(String)agResult.get('Product_Group__c'));
            }
        }

        for(CampaignMember camMem : triggernewCampaignMemberList){

            List<List<String>> productOfferResultList = RTL_UploadCampaignMemberCreateLead.generateProductOfferList(camMem.RTL_Products_Offer_String__c,RetailProductMap,RetailProductSubGroupMap);

            Integer count = 0;

            for( List<String> productOfferItem : productOfferResultList  )
            {
                count++;

                String groupName = productOfferItem.get(0);
                String subGroupName = productOfferItem.get(1);
                String retailProductId = productOfferItem.get(2);

                camMem.put('RTL_Product_Group_'+count+'__c', groupName );
                camMem.put('RTL_Sub_Group_'+count+'__c', subGroupName );
                camMem.put('RTL_Campaign_Product_'+count+'__c', retailProductId );   

            }
        }
    }

    public static void RTL_UpdateCampaignMemberAssigned(List<CampaignMember> triggernewCampaignMemberList,Map<ID,CampaignMember> triggerOldCampaignMemberList){
        Map<Id, CampaignMember> branchLists = new Map<Id, CampaignMember>();
        Map<Id, CampaignMember> agentLists = new Map<Id, CampaignMember>();
        List<String> branchItem = new List<String>();
        List<String> agentItem = new List<String>();
        for(CampaignMember camMem : triggernewCampaignMemberList){
            String newAssignedAgent = camMem.RTL_Assigned_Agent__c;
            String newAssignedBranch = camMem.RTL_Assigned_Branch__c;
            String oldAssignedAgent = '';
            String oldAssignedBranch = '';

            if(triggerOldCampaignMemberList.containsKey(camMem.Id)){
                CampaignMember camMemOld = triggerOldCampaignMemberList.get(camMem.Id);
                oldAssignedAgent = camMemOld.RTL_Assigned_Agent__c;
                oldAssignedBranch = camMemOld.RTL_Assigned_Branch__c;
            }

            if( newAssignedAgent != oldAssignedAgent || newAssignedBranch != oldAssignedBranch){
                if(newAssignedAgent != oldAssignedAgent){
                    if(newAssignedAgent != null){
                        agentItem.add(newAssignedAgent); 
                        agentLists.put(camMem.Id,camMem);
                    }
                }else{
                    if(newAssignedBranch != oldAssignedBranch){
                        if(newAssignedBranch != null){
                            branchItem.add(newAssignedBranch);
                            branchLists.put(camMem.Id,camMem);
                        }
                    }
                }
            }
        }
        
        if(branchItem.size() > 0){
            Map<Id, Branch_and_Zone__c> branchAndZoneLists = new Map<Id, Branch_and_Zone__c>([SELECT Id, Branch_Code__c, RTL_Zone_Code__c, RTL_Region_Code__c FROM Branch_and_Zone__c where Id in:branchItem]);
        
            for(CampaignMember newCamMemBranch:  branchLists.values()){
                Branch_and_Zone__c branchAndZoneById = branchAndZoneLists.get(newCamMemBranch.RTL_Assigned_Branch__c);
                newCamMemBranch.RTL_Branch_Team_Name_Code_Rpt__c = branchAndZoneById.Branch_Code__c;
                newCamMemBranch.RTL_Assigned_Region_Code_Rpt__c = branchAndZoneById.RTL_Region_Code__c;

                if( branchAndZoneById.RTL_Zone_Code__c != null &&  branchAndZoneById.RTL_Zone_Code__c.length() > 4 )
                {
                    newCamMemBranch.RTL_Assigned_Zone_Rpt__c = branchAndZoneById.RTL_Zone_Code__c.substring(0,4);
                }
                else 
                {
                    newCamMemBranch.RTL_Assigned_Zone_Rpt__c = branchAndZoneById.RTL_Zone_Code__c;    
                }
                
            }
        }

        if(agentItem.size() > 0){
            Map<Id, User> userLists = new Map<Id, User>([SELECT Id, RTL_Branch_Code__c ,Zone_Code__c, Region_Code__c FROM User WHERE Id in:agentItem]);

            for(CampaignMember newCamMemAgent:  agentLists.values()){
                User userById = userLists.get(newCamMemAgent.RTL_Assigned_Agent__c);
                newCamMemAgent.RTL_Branch_Team_Name_Code_Rpt__c = userById.RTL_Branch_Code__c;
                newCamMemAgent.RTL_Assigned_Region_Code_Rpt__c = userById.Region_Code__c;

                if( userById.Zone_Code__c != null && userById.Zone_Code__c.length() > 4 )
                {
                    newCamMemAgent.RTL_Assigned_Zone_Rpt__c = userById.Zone_Code__c.substring(0,4);
                }
                else 
                {
                    newCamMemAgent.RTL_Assigned_Zone_Rpt__c = userById.Zone_Code__c;
                }
                
            }
        }
        //System.debug(agentItem);
        //System.debug(agentLists);

    }

    public static void RTL_UpdateContactStatus(List<Campaign> triggerNewCampaignList, Map<Id,Campaign> oldCampaignMap)
    {
        List<Id> camMemId = new List<Id>();
        for(Campaign camList : triggerNewCampaignList){
             String camOldId = '';

            if(oldCampaignMap.containsKey(camList.Id)){
                Campaign camOld = oldCampaignMap.get(camList.Id);
                camOldId = camOld.Status;
            }

            if(camList.Status == 'Expired' && camOldId != 'Expired'){
                camMemId.add(camList.Id);
            }
        }

        if(camMemId.size() > 0){
            List<CampaignMember> camMemLists = [SELECT Id,RTL_Contact_Status__c FROM campaignmember WHERE RTL_Contact_Status__c='New' and campaign.id in:camMemId];
            if(camMemLists.size() > 0){
                for(CampaignMember camMem : camMemLists){
                    camMem.RTL_Contact_Status__c = 'Unused';
                }
                update camMemLists;
            }
        }
    }

 public static void RTL_UpdateMarkettingCode(List<CampaignMember> triggerNewCampaignMemberList)
    {
        RTL_CampaignMember_Running_No__c runningNo = [SELECT Name,Running_No__c 
            FROM RTL_CampaignMember_Running_No__c 
            WHERE Name = 'Campaign Member Running No' LIMIT 1];

        //MAP<ID,String> campaignCodeMap = new MAP<ID,String>();
        List<ID> camIdList = new List<ID>();
        for( CampaignMember cm : triggerNewCampaignMemberList )
        {
            camIdList.add(cm.CampaignId);
            //campaignCodeMap.put( cm.CampaignId , cm.RTL_Campaign_Code_10_digits__c );
        }

        Map<ID,Campaign> campaignMap = new Map<ID,Campaign>( [SELECT ID,RTL_Campaign_Code_10_digits__c,
            Parent.RTL_Campaign_Type__c
            FROM Campaign WHERE Id in :camIdList ] );


        Integer runningNoInt = Integer.ValueOf(runningNo.Running_No__c);
        String running =  String.ValueOf(runningNoInt).leftPad(10 ,'0'); 

        for( CampaignMember cm : triggerNewCampaignMemberList )
        {
            String campaignType = campaignMap.get( cm.CampaignId ).Parent.RTL_Campaign_Type__c;

            //if( campaignType == 'Mass' || campaignType == 'Local Exclusive' )
            //{
                if( ( cm.RTL_Marketing_Code__c == '' || cm.RTL_Marketing_Code__c == null ) && ( cm.RTL_Web_Unique_ID__c == '' || cm.RTL_Web_Unique_ID__c == null ) )
                {
                    runningNoInt++;
                    running = String.ValueOf(runningNoInt).leftPad(10 ,'0');
                    String campaignCode = campaignMap.get( cm.CampaignId ).RTL_Campaign_Code_10_digits__c;
                    cm.RTL_Marketing_Code__c = campaignCode+running;
                }    

            //}
        }

        runningNo.Running_No__c = running;
        update runningNo;
    }

    public static void RTL_UpdateCustomer(List<CampaignMember> triggerNewCampaignMemberList)
    {

        List<ID> contactIdList = new List<ID>();
        for( CampaignMember cm : triggerNewCampaignMemberList )
        {
            if( cm.ContactId != null )
            {
                contactIdList.add(cm.ContactId);
            }
        }
        Map<ID,Contact> contactMap = new Map<ID,Contact>([ SELECT id,Account.id From Contact 
            WHERE id in :contactIdList ]);

        for( CampaignMember cm : triggerNewCampaignMemberList )
        {
            if( cm.ContactId != null )
            {
                Contact c = contactMap.get( cm.ContactId );
                cm.Customer__c = c.Account.Id;
            }
        }
    }

    /*
    Trigger to update Lead Owner Id base on CampaignMember assigned Agent/Branch
    */
    public static void updateLeadOwnerIdFromCampaign(List<CampaignMember> triggerNewCampaignMemberList)
    {
        system.debug('gade: ');
        system.debug(triggerNewCampaignMemberList);

        List<CampaignMember> leadTypeCampaignMemberList = new List<CampaignMember>();

        for( CampaignMember cmOrg : triggerNewCampaignMemberList )
        {
            if( cmOrg.LeadId != null )
            {
                if( cmOrg.RTL_Assigned_Branch__c != null || cmOrg.RTL_Assigned_Agent__c != null )
                {
                    leadTypeCampaignMemberList.add(cmOrg);
                }
            }
        }

        if( leadTypeCampaignMemberList.size() > 0 )
        {
            MAP<ID,ID> LeadQueueORUserMap = new MAP<ID,ID>();

            SET<ID> branchZoneIdList = new SET<ID>();
            for(CampaignMember cm : leadTypeCampaignMemberList )
            {
                if( cm.RTL_Assigned_Branch__c != null )
                {
                    branchZoneIdList.add(cm.RTL_Assigned_Branch__c);
                } 
            }

            MAP<ID,String> branchQueueDevNameMap = new MAP<ID,String>();
            MAP<String,ID> queueDevNameBranchMap = new MAP<String,ID>();
            List<Branch_and_Zone__c> bzList = [ SELECT id,Branch_Code__c
                    from Branch_and_Zone__c 
                    WHERE id IN :branchZoneIdList ];

            for( Branch_and_Zone__c bz : bzList )
            {
                String devName = 'RTL_' + bz.Branch_Code__c;
                String devName2 = 'RTLQ_' + bz.Branch_Code__c;
                //branchQueueDevNameMap.put( bz.id,devName )
                queueDevNameBranchMap.put( devName,bz.id );
                queueDevNameBranchMap.put( devName2,bz.id );
            }

            MAP<ID,ID> bracnhGroupMap = new MAP<ID,ID>();
            List<Group> groupList = [ Select Id, DeveloperName from Group where Type = 'Queue' AND DeveloperName in :queueDevNameBranchMap.keySet() ];
            for(Group gp : groupList)
            {
                ID branchZoneId = queueDevNameBranchMap.get(gp.DeveloperName);
                bracnhGroupMap.put( branchZoneId , gp.id );
            }

            for(CampaignMember cm : leadTypeCampaignMemberList )
            {
                if( cm.RTL_Assigned_Branch__c != null )
                {
                    ID queueId = bracnhGroupMap.get(cm.RTL_Assigned_Branch__c);
                    LeadQueueORUserMap.put(cm.LeadID , queueId );
                }
                else if( cm.RTL_Assigned_Agent__c != null )
                {
                    ID agentId = cm.RTL_Assigned_Agent__c;
                    LeadQueueORUserMap.put(cm.LeadID , agentId);
                }
            }

            // Update lead to Queue/Agent on Owner
            if( LeadQueueORUserMap.size() > 0 )
            {
                //LeadQueueORUserMap

                List<Lead> leadList = [SELECT id,OwnerId FROM Lead WHERE id in :LeadQueueORUserMap.keySet() ];

                for( Lead ld : leadList )
                {
                    ld.OwnerId = LeadQueueORUserMap.get(ld.id);
                }
                update leadList;
            }

            
        }

    }
    


    //Class VerifyResult
    //{
    //    public String error;
    //    public Boolean isVerify;
    //}

    //public VerifyResult verifyProductFeature(String productFeatureString)
    //{
    //    VerifyResult result = new VerifyResult();

    //    //List<List<String>> productsOffer = RTL_UploadCampaignMemberCreateLead.processProductOfferString(productFeatureString);

    //    return result;
    //}

}