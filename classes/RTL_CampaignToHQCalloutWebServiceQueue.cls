public class RTL_CampaignToHQCalloutWebServiceQueue 
//implements Queueable, Database.AllowsCallouts 
{
/*
	private String userSessionId;

	private List<String> emailsNotification{
        get{
            if(emailsNotification==null){
            	emailsNotification = new List<String>();
            	List<RTL_Campaign_Submit_Error_Email_To__mdt> listData = [select id,RTL_Email_to__c,RTL_Status__c
                   from RTL_Campaign_Submit_Error_Email_To__mdt
                   where RTL_Status__c = true];

                   for(RTL_Campaign_Submit_Error_Email_To__mdt item : listData )
                   {
                		emailsNotification.add( item.RTL_Email_to__c );
                   }	
            }
            return emailsNotification;
        }
        set;
    }

	public static String MasterCampaignRecordTypeId {
    	get{
	    	if(MasterCampaignRecordTypeId==null){
	    		Recordtype rt = [ SELECT ID,name from Recordtype WHERE sObjectType ='Campaign' AND name = 'Master Campaign' ];
				MasterCampaignRecordTypeId = rt.id;
	    	}
    		return MasterCampaignRecordTypeId;
    	}
    	set;
    }


	private String campaignId;
	private RTL_Campaign_To_HQ_Callout_Log__c camLog;
	private RTL_Campaign_To_HQ_Callout_Transaction__c camTran;


	public RTL_CampaignToHQCalloutWebServiceQueue(RTL_Campaign_To_HQ_Callout_Log__c cLog , RTL_Campaign_To_HQ_Callout_Transaction__c cTran, String sessionId)
	{
		userSessionId = sessionId;
		campaignId = cLog.RTL_Campaign_ID__c;
		camLog = cLog;
		camTran = cTran;
	}

	public class MasterChildCampaing
	{
		public Campaign master;
		public List<Campaign> childrenList;
	}

	public static RTL_CampaignToHQ_WSDL_SAMPLE.Campaign generateCampaingRequest(String camId, String recordTypeId)
	{
		MasterChildCampaing mmco = getMasterChildCampaingObject(camId,recordTypeId);
		RTL_CampaignToHQ_WSDL_SAMPLE.Campaign camObj = genCampaingObject(mmco.master,mmco.childrenList);

		return camObj;
	}

	public static MasterChildCampaing getMasterChildCampaingObject(String camId, String recordTypeId)
	{
		MasterChildCampaing mcco = new MasterChildCampaing();
		Campaign masterCam = new Campaign();
		List<Campaign> childrenCamList = new List<Campaign>();

		if( recordTypeId == MasterCampaignRecordTypeId )
		{
			masterCam = [ SELECT id,name,parentId,
				RTL_Campaign_Code_9_digits__c,RTL_Campaign_Code_10_digits__c,RTL_Campaign_Objective__c,
				RTL_Campaign_Sub_Type__c,RTL_Priority__c,StartDate,EndDate,RTL_Campaign_Stage__c,
				Status,RTL_Campaign_Channel__c,RTL_Call_Start_Date__c,RTL_Call_End_Date__c,
				RTL_Tracking_Type__c
				FROM Campaign WHERE id = :camId ];

			childrenCamList =  [ SELECT id,name,parentId,
				RTL_Campaign_Code_9_digits__c,RTL_Campaign_Code_10_digits__c,RTL_Campaign_Objective__c,
				RTL_Campaign_Sub_Type__c,RTL_Priority__c,StartDate,EndDate,RTL_Campaign_Stage__c,
				Status,RTL_Campaign_Channel__c,RTL_Call_Start_Date__c,RTL_Call_End_Date__c,
				RTL_Tracking_Type__c
				FROM Campaign WHERE parentId = :camId ];

		}
		// Child
		else 
		{
			Campaign childCam = [ SELECT id,name,parentId,
				RTL_Campaign_Code_9_digits__c,RTL_Campaign_Code_10_digits__c,RTL_Campaign_Objective__c,
				RTL_Campaign_Sub_Type__c,RTL_Priority__c,StartDate,EndDate,RTL_Campaign_Stage__c,
				Status,RTL_Campaign_Channel__c,RTL_Call_Start_Date__c,RTL_Call_End_Date__c,
				RTL_Tracking_Type__c
				FROM Campaign WHERE id = :camId ];

			childrenCamList.add(childCam);

			masterCam = [ SELECT id,name,parentId,
				RTL_Campaign_Code_9_digits__c,RTL_Campaign_Code_10_digits__c,RTL_Campaign_Objective__c,
				RTL_Campaign_Sub_Type__c,RTL_Priority__c,StartDate,EndDate,RTL_Campaign_Stage__c,
				Status,RTL_Campaign_Channel__c,RTL_Call_Start_Date__c,RTL_Call_End_Date__c,
				RTL_Tracking_Type__c
				FROM Campaign WHERE id = :childCam.parentId ];
		}

		mcco.master = masterCam;
		mcco.childrenList = childrenCamList;

		return mcco;
	}

	public void execute(QueueableContext context) {
        	
        	RTL_CampaignToHQ_WSDL_SAMPLE.result resultObj = new RTL_CampaignToHQ_WSDL_SAMPLE.result();
        	// Master
			RTL_CampaignToHQ_WSDL_SAMPLE.Campaign camObj = generateCampaingRequest(campaignId,camLog.RTL_Campaign__r.RecordTypeId);
        	
			resultObj = callSOAP(camObj);

			if( resultObj != null )
			{

				if( resultObj.Status == 'Success' )
				{
					//update transaction
					camTran.RTL_Status__c = 'Success';

					//upldate log
					camLog.RTL_Status__c = 'Success';
					
				}
				else //if ( resultObj.Status == 'Failed' )
				{
					Decimal attempNo =  camLog.RTL_No_of_Retry__c;
					// Try to retry 2 more times
					if( camLog.RTL_No_of_Retry__c < 3 )
					{

						// reset create date for next retry call
						camLog.RTL_Status__c = 'Retry';
						camLog.RTL_No_of_Retry__c = camLog.RTL_No_of_Retry__c+1;
						camTran.RTL_Remark__c = 'Failed on Attemp #' + attempNo;
					}
					else 
					{
						// Mask this transaction Permanant failed
						camLog.RTL_Status__c = 'Fail';	
						camTran.RTL_Remark__c = 'Failed after Attemp #' + camLog.RTL_No_of_Retry__c;	

						//**** Process Email ************
						sendNotificationEmail(campaignId);
					}
					camTran.RTL_Status__c = 'Fail';


				}

				camLog.RTL_Status_text__c = resultObj.Description;
				camLog.RTL_Last_Call_Date__c = Datetime.now();
				camLog.RTL_Status_Code__c = resultObj.ErrorCode;

				camTran.RTL_Status_Text__c = resultObj.Description;
				camTran.RTL_Call_Date__c = Datetime.now();
				camTran.RTL_Status_Code__c = resultObj.ErrorCode;



				update camLog;
				update camTran;
			}

			//system.debug(resultObj.Status);

			// Chain to self class to process onother in queue callout
			RTL_CampaignToHQCalloutQueue calloutProcess = new RTL_CampaignToHQCalloutQueue();
			calloutProcess.execute();
			//System.enqueueJob(calloutProcess);

	}

	private void sendNotificationEmail(String cId)
    {
    	// Ignore sending email if no email data in custom meta data
    	if( emailsNotification.size() == 0 )
    	{
    		return;
    	}	

    	Campaign camObject = [SELECT ID,Name FROM Campaign WHERE ID =:cId ];
        
        //String userName = UserInfo.getUserName();
        //User activeUser = [Select Email From User where Username = : userName limit 1];
        //String userEmail = activeUser.Email;
        
        String tagerUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/RTL_CampaignToHQResubmit?campaign_id=' + cId;
        
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        //message.toAddresses = new String[] { userEmail };
        message.toAddresses = emailsNotification;

        //message.optOutPolicy = 'FILTER';
        message.subject = 'Campaing '+ camObject.Name +' Submit To HQ Failed';
        message.setHtmlBody('Campaign ' + camObject.Name + ' Submit To HQ Failed, Please click below to view result<br />' +
                                '<b>View:<b> <a href="'+ tagerUrl +'">click here</a><br />');
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        
    }

	private RTL_CampaignToHQ_WSDL_SAMPLE.result callSOAP(RTL_CampaignToHQ_WSDL_SAMPLE.Campaign campaignObject)
	{
		try{
			//String sessionId = UserInfo.getSessionId();
			//system.debug(sessionId);
			RTL_CampaignToHQ_WSDL_SAMPLE.RTL_CampaignToHQ_SOAP service = new RTL_CampaignToHQ_WSDL_SAMPLE.RTL_CampaignToHQ_SOAP();

			service.SessionHeader = new RTL_CampaignToHQ_WSDL_SAMPLE.SessionHeader_element();
			service.SessionHeader.sessionId = userSessionId;

			RTL_CampaignToHQ_WSDL_SAMPLE.result callResult = service.manageCampaign(campaignObject);

			return callResult;
		} catch(exception e) {
			RTL_CampaignToHQ_WSDL_SAMPLE.result result = new RTL_CampaignToHQ_WSDL_SAMPLE.result();

			result.Status = 'Fail';
			result.Description =  e.getMessage();
			return result;
		}
	}

	public static RTL_CampaignToHQ_WSDL_SAMPLE.Campaign genCampaingObject(Campaign masterCam,List<Campaign> childCams)
	{
		RTL_CampaignToHQ_WSDL_SAMPLE.Campaign campaignObject = new RTL_CampaignToHQ_WSDL_SAMPLE.Campaign();
		campaignObject.CampaignID_SF = masterCam.RTL_Campaign_Code_9_digits__c;
		campaignObject.CampaignStatus_SF = masterCam.status;


		RTL_CampaignToHQ_WSDL_Sample.ArrayOfCampignActivity atv = new RTL_CampaignToHQ_WSDL_Sample.ArrayOfCampignActivity();
		atv.CampignActivity = new List<RTL_CampaignToHQ_WSDL_Sample.CampignActivity>();

		// Child Campaign
		for(Campaign childCam : childCams )
		{
			RTL_CampaignToHQ_WSDL_SAMPLE.CampignActivity camAct = new RTL_CampaignToHQ_WSDL_SAMPLE.CampignActivity();
			camAct.CampaignActivityID_SF  = childCam.RTL_Campaign_Code_10_digits__c;
			camAct.CampaignActivityStatusSF = childCam.status;

			atv.CampignActivity.add(camAct);
		}

		campaignObject.Activity = atv;

		return campaignObject;

	}
*/
}