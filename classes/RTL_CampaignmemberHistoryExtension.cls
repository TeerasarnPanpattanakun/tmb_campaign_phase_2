public with sharing class RTL_CampaignmemberHistoryExtension {

    private final CampaignMember campaignMemberObj;
    public List <RTL_Campaign_Member_History__c> cmhList {get;set;}
    private List <RTL_Campaign_Member_History_detail__c> cmhdList;
    public Map<Id,List<RTL_Campaign_Member_History_detail__c>> cmhdMap {get;set;}

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public RTL_CampaignmemberHistoryExtension(ApexPages.StandardController stdController) {
        campaignMemberObj = (CampaignMember)stdController.getRecord();

        cmhList = [SELECT ID,RTL_Campaign_Member_ID__c,RTL_Campaign__c,
                            RTL_Contact__c,RTL_Date__c,RTL_Lead__c,RTL_Modified_By__c
                                                                     FROM RTL_Campaign_Member_History__c 
                                                                     WHERE RTL_Campaign_Member_ID__c =:campaignMemberObj.id
                                                                     ORDER BY RTL_Date__c DESC ];

        cmhdList = [SELECT ID,RTL_Field_Name__c,RTL_Field_New_Value__c,RTL_Field_Old_Value__c,RTL_Campaign_Member_History__c
                                                                    FROM  RTL_Campaign_Member_History_detail__c 
                                                                    WHERE RTL_Campaign_Member_History__c IN:cmhList
                                                                    ORDER BY CreatedDate DESC ];    
        cmhdMap = new Map<Id,List<RTL_Campaign_Member_History_detail__c>> ();
        

        for(RTL_Campaign_Member_History_detail__c cmhd : cmhdList){
            if(cmhdMap.containsKey(cmhd.RTL_Campaign_Member_History__c)){
                cmhdMap.get(cmhd.RTL_Campaign_Member_History__c).add(cmhd);
            }else{
                List<RTL_Campaign_Member_History_detail__c> cmhdChild = new List<RTL_Campaign_Member_History_detail__c>();
                cmhdChild.add(cmhd);
                cmhdMap.put(cmhd.RTL_Campaign_Member_History__c,cmhdChild);
            }
        }                                                                                                            

    }    
    public PageReference cancel(){
        PageReference returnPage;
        try{
            returnPage = new PageReference('/'+campaignMemberObj.id);
        }catch(Exception e){
            //returnPage = new PageReference('/'+currentObject.Id);  
        }
        
        return returnPage;
    }                                                          
  
}