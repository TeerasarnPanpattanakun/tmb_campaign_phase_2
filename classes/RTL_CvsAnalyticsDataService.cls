//Generated by wsdl2apex

public class RTL_CvsAnalyticsDataService {
    public class getCVSAnalyticsData_element {
        public String RMID;
        private String[] RMID_type_info = new String[]{'RMID','http://www.tmbbank.com/CVSAnalyticsData/',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.tmbbank.com/CVSAnalyticsData/','false','false'};
        private String[] field_order_type_info = new String[]{'RMID'};
    }
    public class CVSAnalyticsData {
        public String Status;
        public String Message;
        public String MIBStatus;
        public String UsagePercentage;
        public String suitability;
        public String privilege;
        public String privilege2Url;
        public String currentPrivilege2Desc;
        public String entitledPrivilege2Desc;
        private String[] Status_type_info = new String[]{'Status','http://www.tmbbank.com/CVSAnalyticsData/',null,'1','1','false'};
        private String[] Message_type_info = new String[]{'Message','http://www.tmbbank.com/CVSAnalyticsData/',null,'1','1','false'};
        private String[] MIBStatus_type_info = new String[]{'MIBStatus','http://www.tmbbank.com/CVSAnalyticsData/',null,'1','1','false'};
        private String[] UsagePercentage_type_info = new String[]{'UsagePercentage','http://www.tmbbank.com/CVSAnalyticsData/',null,'1','1','false'};
        private String[] suitability_type_info = new String[]{'suitability','http://www.tmbbank.com/CVSAnalyticsData/',null,'1','1','false'};
        private String[] privilege_type_info = new String[]{'privilege','http://www.tmbbank.com/CVSAnalyticsData/',null,'1','1','false'};
        private String[] privilege2Url_type_info = new String[]{'privilege2Url','http://www.tmbbank.com/CVSAnalyticsData/',null,'1','1','false'};
        private String[] currentPrivilege2Desc_type_info = new String[]{'currentPrivilege2Desc','http://www.tmbbank.com/CVSAnalyticsData/',null,'1','1','false'};
        private String[] entitledPrivilege2Desc_type_info = new String[]{'entitledPrivilege2Desc','http://www.tmbbank.com/CVSAnalyticsData/',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.tmbbank.com/CVSAnalyticsData/','false','false'};
        private String[] field_order_type_info = new String[]{'Status','Message','MIBStatus','UsagePercentage','suitability','privilege','privilege2Url','currentPrivilege2Desc','entitledPrivilege2Desc'};
    }
    public class getCVSAnalyticsDataResponse_element {
        public RTL_CvsAnalyticsDataService.CVSAnalyticsData Result;
        private String[] Result_type_info = new String[]{'Result','http://www.tmbbank.com/CVSAnalyticsData/',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.tmbbank.com/CVSAnalyticsData/','false','false'};
        private String[] field_order_type_info = new String[]{'Result'};
    }
    public class CVSAnalyticsDataPort {
        AppConfig__c mc = AppConfig__c.getValues('WsOSC07');
        string tmpEndpoing = mc == null ? 'https://tmbcrmservices.tmbbank.com/uatservices/AccountPlantProxy.asmx' : mc.Value__c;
        public String endpoint_x = tmpEndpoing;
        public Map<String, String> inputHttpHeaders_x;
        public Map<String, String> outputHttpHeaders_x;
        AppConfig__c cer = AppConfig__c.getValues('MulesoftCert');
        string certName = cer == null ? 'retailtest_mulesoft' : cer.Value__c;
        public String clientCertName_x = certName; // wil change to
        public String clientCert_x;
        public String clientCertPasswd_x = 'test';
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://www.tmbbank.com/CVSAnalyticsData/', 'RTL_CvsAnalyticsDataService'};
        public RTL_CvsAnalyticsDataService.CVSAnalyticsData getCVSAnalyticsData(String RMID) {
            RTL_CvsAnalyticsDataService.getCVSAnalyticsData_element request_x = new RTL_CvsAnalyticsDataService.getCVSAnalyticsData_element();
            request_x.RMID = RMID;
            RTL_CvsAnalyticsDataService.getCVSAnalyticsDataResponse_element response_x;
            Map<String, RTL_CvsAnalyticsDataService.getCVSAnalyticsDataResponse_element> response_map_x = new Map<String, RTL_CvsAnalyticsDataService.getCVSAnalyticsDataResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://www.tmbbank.com/CVSAnalyticsData/getCVSAnalyticsData',
              'http://www.tmbbank.com/CVSAnalyticsData/',
              'getCVSAnalyticsData',
              'http://www.tmbbank.com/CVSAnalyticsData/',
              'getCVSAnalyticsDataResponse',
              'RTL_CvsAnalyticsDataService.getCVSAnalyticsDataResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.Result;
        }
    }
}