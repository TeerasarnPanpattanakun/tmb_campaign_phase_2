global class RTL_DummyCampaignMemberEmailErrorLog implements Schedulable {
		global void execute(SchedulableContext sc) {
		String query = 'SELECT Id, Name FROM CampaignMember where Campaign.RecordType.name = \'Dummy Campaign\'';
        RTL_BatchCMAssignEmailErrorLog logPurge = new RTL_BatchCMAssignEmailErrorLog(query,'Dummy Campaign');
        Id BatchProcessId = Database.ExecuteBatch(logPurge);
	}
}