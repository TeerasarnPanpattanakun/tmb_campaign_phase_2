public class RTL_HomeScreenCallListComponentCtrl {
    public transient Map<Id,campaignHomeScreenWrapper> mapCampaign {get;set;}
    public List<ProcessInstanceWorkItem> itemToApprove {get;set;}
    public transient List<CampaignMember> listPendingCall {get;set;}
    public transient Map<Id,CampaignMember> mapCampaignMember {get;set;}
    public transient List<aggregateresult> allCampaignHasMemberList {get;set;}
    public transient List<aggregateresult> allCampaignMemberList {get;set;}
    public transient List<aggregateresult> pendingCampaignMemberList {get;set;}
    public Set<Id> setCampaignId {get;set;}
    public Map<Id, Recordtype> campaignRecordType {get; set;}
    public Boolean showTable {get;set;}
    public Boolean showPendingCall {get;set;}
    public Boolean showItemToApprove {get;set;}
    public Boolean showReassignButton {get;set;}
    public String callListNavTabLabel {get;set;}

    public integer sizeItemCallList {get{
        if (sizeItemCallList == null){
            sizeItemCallList = 0;
        }
        return sizeItemCallList;
        
    }set;}

    public integer sizeItemToApprove {get{
        if (sizeItemToApprove == null){
            sizeItemToApprove = 0;
        }
        return sizeItemToApprove;
        
    }set;}

    //Code Referral
    public integer countNewReferral {get{
        if (countNewReferral == null){
            countNewReferral = 0;
        }
        return countNewReferral;
        
    }set;}

    public string reportPageReferral {get{
        if (reportPageReferral == null){
            reportPageReferral = '';
            
            profile pro = [select id,name from profile where id =: userinfo.getprofileID()];
            list<ReportProfileMapping__c> rpm = new list<ReportProfileMapping__c>();
                rpm = [select ReferralReportName__c from ReportProfileMapping__c 
                                           where isActive__c = true and ProfileName__c =: pro.name];
            if (rpm.size() == 0)
                rpm = [select ReferralReportName__c from ReportProfileMapping__c 
                                           where isActive__c = true and ProfileName__c = 'Default'];
                                           
            if (rpm.size() > 0 && rpm.get(0).ReferralReportName__c != null)
            {
                list<report> res = [select id from Report where DeveloperName =: rpm.get(0).ReferralReportName__c]; 
                if (res.size() > 0)
                    reportPageReferral = res.get(0).id;
            }

        }
        return reportPageReferral;
    }set;}    

    public RTL_HomeScreenCallListComponentCtrl() {


        //for (DescribeTabSetResult tsr : Schema.describeTabs()) {
        //    // Each tab of the app
        //    for (DescribeTabResult tr : tsr.getTabs()  )
        //    {
        //        if( tr.getName() == 'Call_List' )
        //        {
        //            callListNavTabLabel = tr.getLabel();
        //        }
        //    }            
        //}

        mapCampaign = new Map<Id,campaignHomeScreenWrapper>();
        itemToApprove = new List<ProcessInstanceWorkItem>();
        listPendingCall = new List<CampaignMember>();
        allCampaignMemberList = new List<aggregateresult>();
        pendingCampaignMemberList = new List<aggregateresult>();
        setCampaignId = new Set<Id>();

        showPendingCall = false;
        showItemToApprove = false;
        showReassignButton = false;
        showTable = false;

        Set<String> accessPendingCall = new Set<String>();
        Set<String> accessItemToApprovewithoutTable = new Set<String>();
        Set<String> accessItemToApprovewithTable = new Set<String>();
        Set<String> accessReassignButton = new Set<String>();
        Set<String> accessCampaignTable = new Set<String>();
        Set<String> accessBranchCampaignMember = new Set<String>();
        Set<String> accessZoneCampaignMember = new Set<String>();


        User currentUser = [SELECT Id,Username
                                    ,UserRole.name
                                    ,Profile.name
                                    ,RTL_Branch_Code__c
                                    ,Zone_Code__c 
                            FROM User 
                            where id =: UserInfo.getUserId() limit 1];

        System.debug('Current User: ' + UserInfo.getUserName());
        System.debug('Current User Id: ' + UserInfo.getUserId());
        System.debug('Current User Role'+currentUser.UserRole.name);
        System.debug('Current User Profile'+currentUser.Profile.name);

        //Code P'Fah RTL_Referral__c Object
        countNewReferral = [SELECT count() FROM RTL_Referral__c WHERE 
                            (RTL_Stage__c = 'New' OR RTL_Stage__c = 'In progress_contacted' ) AND 
                            (RTL_IsUserInQueue__c = true OR RTL_Is_Owner__c = true)];  

        //Get manage home button meta
        Map<String,RTL_Home_Campaign_Manage_Button__mdt> homeManageCampaign = new Map<String,RTL_Home_Campaign_Manage_Button__mdt>();
        for (RTL_Home_Campaign_Manage_Button__mdt h : [SELECT Id,Label, CampaignTable__c, 
            ItemToApprove__c, PendingCall__c, Profile__c, Reassign__c, Role__c ,BranchOnly__c,ZoneOnly__c
            FROM RTL_Home_Campaign_Manage_Button__mdt ]) {
            homeManageCampaign.put(h.Label,h);
        }

        system.debug('Home Manage Campaign : '+homeManageCampaign);

        for(RTL_Home_Campaign_Manage_Button__mdt h : homeManageCampaign.values()){
            if(h.PendingCall__c){
                accessPendingCall.add(h.Label);
            }

            if(h.ItemToApprove__c && !h.CampaignTable__c){
                accessItemToApprovewithoutTable.add(h.Label);
            }

            if(h.Reassign__c){
                accessReassignButton.add(h.Label);
            }

            if(h.CampaignTable__c){
                accessCampaignTable.add(h.Label);
            }
            if(h.BranchOnly__c){
                accessBranchCampaignMember.add(h.Label);
            }
            if(h.ZoneOnly__c){
                accessZoneCampaignMember.add(h.Label);
            }
        }


        if(currentUser.UserRole.name != null){
           if(startsWithAny(currentUser.UserRole.name,homeManageCampaign.keySet())||
                startsWithAny(currentUser.Profile.name,homeManageCampaign.keySet())){
            /*if(currentUser.UserRole.name.startsWith('BR-MGR-Zone') || 
                currentUser.UserRole.name.startsWith('BR-Sales-Zone') ||
                currentUser.UserRole.name.startsWith('WM-TL') ||
                currentUser.UserRole.name.startsWith('WM-RM') ||
                currentUser.UserRole.name.startsWith('BBG') ||
                currentUser.Profile.name.startsWith('TMB Retail Marketing Team') ||
                currentUser.Profile.name.startsWith('System Administrator')){*/


                //Get RecordType Child Campaign
                campaignRecordType = new Map<Id, RecordType>([SELECT ID, Name, DeveloperName
                                                            FROM Recordtype
                                                            WHERE sObjectType = 'Campaign'
                                                            AND (NOT DeveloperName LIKE '%Commercial%')
                                                            AND (NOT DeveloperName LIKE '%Master%')
                                                            AND (NOT DeveloperName LIKE '%Dummy%')]);

                //Get item to approve 
                getItemToApprove();
                //Get current user pending call list
                getCampaignMemberPendingCall();

                //Get Campaign has member more than 1
                //getCampaignHasMember();


                //Check Condition branch and zone
                if(startsWithAny(currentUser.Profile.name,accessBranchCampaignMember) || startsWithAny(currentUser.UserRole.name,accessBranchCampaignMember)){
                    if(startsWithAny(currentUser.Profile.name,accessReassignButton) || startsWithAny(currentUser.UserRole.name,accessReassignButton)){
                        showReassignButton = true;
                    }

                    showPendingCall = true;
                    showTable = true;


                    allCampaignMemberList = new List<aggregateresult>([ select campaignId,count(Id) countcm 
                                                                        from campaignmember 
                                                                        where RTL_Branch_Team_Name_Code_Rpt__c =: currentUser.RTL_Branch_Code__c
                                                                        group by campaignId limit 10000]);
                    

                    pendingCampaignMemberList = new List<aggregateresult>([ select campaignId,count(Id) countcm
                                                                            from campaignmember 
                                                                            Where RTL_Branch_Team_Name_Code_Rpt__c =: currentUser.RTL_Branch_Code__c
                                                                            And RTL_Contact_Status__c = 'New'
                                                                            group by campaignId limit 10000]);





                }else if(startsWithAny(currentUser.Profile.name,accessZoneCampaignMember) || startsWithAny(currentUser.UserRole.name,accessZoneCampaignMember)){
                    if(startsWithAny(currentUser.Profile.name,accessReassignButton) || startsWithAny(currentUser.UserRole.name,accessReassignButton)){
                        showReassignButton = true;
                    }

                    showPendingCall = true;
                    showTable = true;

                    allCampaignMemberList = new List<aggregateresult>([ select campaignId,count(Id) countcm 
                                                                        from campaignmember 
                                                                        where RTL_Assigned_Zone_Rpt__c =: currentUser.Zone_Code__c
                                                                        group by campaignId limit 10000]);
                    

                    pendingCampaignMemberList = new List<aggregateresult>([ select campaignId,count(Id) countcm
                                                                            from campaignmember 
                                                                            Where RTL_Assigned_Zone_Rpt__c =: currentUser.Zone_Code__c
                                                                            And RTL_Contact_Status__c = 'New'
                                                                            group by campaignId limit 10000]);
                }else if(startsWithAny(currentUser.Profile.name,accessItemToApprovewithoutTable) || startsWithAny(currentUser.UserRole.name,accessItemToApprovewithoutTable)){

                    showItemToApprove = true;

                }else{

                    showPendingCall = true;
                    showItemToApprove = true;
                    showReassignButton = true;
                    showTable = true;


                    allCampaignMemberList = new List<aggregateresult>([ select campaignId,count(Id) countcm 
                                                                        from campaignmember 
                                                                        group by campaignId limit 10000]);

                    pendingCampaignMemberList = new List<aggregateresult>([ select campaignId,count(Id) countcm
                                                                            from campaignmember 
                                                                            Where RTL_Contact_Status__c = 'New'
                                                                            group by campaignId limit 10000]);
                }

                Set<Id> tempCampaignId = new Set<Id>();
                String campaignId;

                if(allCampaignMemberList.size() > 0){
                    for(aggregateresult a : allCampaignMemberList){
                        campaignId = String.valueOf(a.get('campaignId'));
                        tempCampaignId.add(campaignId);
                    }
                }

                 //Get Campaign
                List<Campaign> allCampaign = [SELECT Id,name
                                                FROM Campaign 
                                                where IsActive = true 
                                                and RTL_Call_Start_Date__c <= TODAY AND RTL_Call_End_Date__c >= TODAY
                                                and id in: tempCampaignId
                                                and RecordTypeid in:campaignRecordType.values()
                                                order by RTL_Campaign_Order__c limit 100];

                system.debug('Campaign : '+allCampaign.size());

                //Get All Campaign to Wrapper Class
                for(Campaign c : allCampaign){
                    campaignHomeScreenWrapper cam = new campaignHomeScreenWrapper(c.Id,c.Name,0,0);
                    mapCampaign.put(c.id,cam);
                }

                if(startsWithAny(currentUser.Profile.name,accessCampaignTable) || startsWithAny(currentUser.UserRole.name,accessCampaignTable)){
                     //Assign Count of Campaign member all
                    if(allCampaignMemberList.size() > 0){
                        for(aggregateresult a : allCampaignMemberList){  
                            String camId =  String.valueOf(a.get('campaignId'));
                            Integer count = Integer.valueOf(a.get('countcm'));

                            if(mapCampaign.get(camId)!=null){
                                if(mapCampaign.get(camId).campaignId == camId){
                                    mapCampaign.get(camId).allCampaignNumber = count;
                                }
                            }

                        }

                    }
 
                    //Assign Count of Campaign member => contact status = New
                    if(pendingCampaignMemberList.size() > 0){
                        for(aggregateresult a : pendingCampaignMemberList){  
                            String camId =  String.valueOf(a.get('campaignId'));
                            Integer count = Integer.valueOf(a.get('countcm'));

                            if(mapCampaign.get(camId)!=null){
                                if(mapCampaign.get(camId).campaignId == camId){
                                    mapCampaign.get(camId).pendingCampaignNumber = count;
                                }
                            }

                        }
                    }

                }

            }else{
                showPendingCall = false;
                showItemToApprove = false;
                showReassignButton = false;
                showTable = false;
            }



        }

        
    }

   /* public Set<Id> getCampaignHasMember(){
        allCampaignHasMemberList = new List<aggregateresult>([ select campaignId,count(Id) countcm ,
                                                                campaign.name
                                                                from campaignmember 
                                                                group by campaignId,campaign.name 
                                                                having count(Id) >0
                                                                limit 10000]);
        
        system.debug('allCampaignHasMemberList : '+allCampaignHasMemberList);
        String campaignId;
        for(aggregateresult a : allCampaignHasMemberList){
            campaignId = String.valueOf(a.get('campaignId'));
            setCampaignId.add(campaignId);

        }

        return setCampaignId;

    }*/

    public void getItemToApprove(){
        itemToApprove = [SELECT ProcessInstance.TargetObjectId 
                FROM ProcessInstanceWorkItem 
                WHERE ProcessInstance.Status = 'Pending' 
                AND ActorId =:UserInfo.getUserId() limit 1000 ];

        if(itemToApprove.size() > 0){
            sizeItemToApprove = itemToApprove.size();
        }else{
            sizeItemToApprove = 0;
        }

    }
    public void getCampaignMemberPendingCall(){
        listPendingCall = [SELECT id 
                    FROM CampaignMember
                    WHERE (RTL_Assigned_Agent__c =: UserInfo.getUserId() 
                    or RTL_Contact_Staff_Name__c =: UserInfo.getUserId()) 
                    and RTL_Offer_Result__c = 'Pending' 
                    and Campaign.RecordTypeid in:campaignRecordType.values()
                    and Campaign.IsActive = true 
                    and Campaign.RTL_Call_Start_Date__c <= TODAY AND Campaign.RTL_Call_End_Date__c >= TODAY
                    limit 2000];

        if(listPendingCall.size() > 0){
            sizeItemCallList = listPendingCall.size();
        }else{
            sizeItemCallList = 0;
        }
    }


    public class campaignHomeScreenWrapper{
        public Id campaignId {get;set;}
        public String campaignName {get;set;}
        public Integer allCampaignNumber {get;set;}
        public Integer pendingCampaignNumber {get;set;}

        campaignHomeScreenWrapper(Id campaignId,String campaignName,Integer allCampaignNumber,Integer pendingCampaignNumber){
            this.campaignId = campaignId;
            this.campaignName = campaignName;
            this.allCampaignNumber = allCampaignNumber;
            this.pendingCampaignNumber = pendingCampaignNumber;

        }
    }

    public static Boolean startsWithAny(String input, Set<String> substrings)
    {
        if (input == null) return false;
        String expression = '^(' + String.join(new List<String>(substrings), '|') + ')';
        return Pattern.compile(expression).matcher(input).find();
    }

}