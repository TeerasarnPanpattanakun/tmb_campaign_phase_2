public with sharing class RTL_InterestedProductTriggerHandler extends TriggerHandler {
    public RTL_InterestedProductTriggerHandler(){
        if(Test.isRunningTest()){
            this.setMaxLoopCount(10);
        }
        else{
            this.setMaxLoopCount(5000);
        }
        System.Debug( 'TMB: -> RTL_InterestedProductTriggerHandler Invoked' );
    }

    protected override void afterInsert(map<id,SObject> newMap) {       
        System.Debug('TMB: -> RTL_InterestedProductTriggerHandler start of afterInsert');
        updateLeadPrimaryCount(null, newMap);
    }   
    
    protected override void afterUpdate(map<id,sObject> oldMap, map<id,sObject> newMap) {        
        System.Debug('TMB: -> RTL_InterestedProductTriggerHandler start of afterUpdate');  
        updateLeadPrimaryCount(oldMap,newMap);
    }
    
    protected override void afterDelete(map<id,sObject> oldMap) {       
        System.Debug('TMB: -> RTL_InterestedProductTriggerHandler start of afterDelete');
        updateLeadPrimaryCount(oldMap, null);
    }  
    
    /**
     * This is the method to update the primary product from lead counter after insert/update/delete
     * insert: null, newMap
     * update: oldMap, newMap
     * delete: oldMap, null
     **/
    private static void updateLeadPrimaryCount(map<id,SObject> oldMap, map<id,SObject> newMap) {
        List<Lead> leadsToUpdate = new List<Lead>();

        map<id,SObject> tmpMap = null;       
        if (oldMap != null) tmpMap = oldMap;
        if (newMap != null) tmpMap = newMap;

        Map<Id, Lead> leadMap = new Map<Id, Lead>();
        Set<Id> leadIds = new Set<Id>();
        //get Leads map from list of interested products
        for (Id productId:tmpMap.keySet())
            leadIds.add(((RTL_Interested_products_c__c)tmpMap.get(productId)).Lead__c);
        for(Lead lead : [Select Id, RecordTypeId, FirstName, LastName, RTL_Count_InterestedProducts_Primary__c from Lead where Id in :leadIds])
            leadMap.put(lead.Id, lead);
        
        RTL_Interested_products_c__c tmpProductObj = null;
        RTL_Interested_products_c__c oldProductObj = null;
        RTL_Interested_products_c__c newProductObj = null;
        Lead leadObj = null;
        for (Id productId:tmpMap.keySet()){
            tmpProductObj = (RTL_Interested_products_c__c)tmpMap.get(productId);
            //only update lead object when lead is not converted into customer
            if (tmpProductObj.Customer__c == null) {
                //only update lead object when a primary product has primary type changed
                leadObj = leadMap.get(tmpProductObj.Lead__c);
                //if (RTL_Utility.getObjectRecordTypeIdsByDevNamePrefix(Lead.SObjectType, 'Retail').contains(leadObj.RecordTypeId)) {//only continue if it's retail record type
                    if (oldMap != null && newMap != null) {//update product
                        oldProductObj = (RTL_Interested_products_c__c)oldMap.get(productId);
                        newProductObj = (RTL_Interested_products_c__c)newMap.get(productId);
                        if (newProductObj.Is_Primary__c && !oldProductObj.Is_Primary__c) { 
                            leadObj.RTL_Count_InterestedProducts_Primary__c = leadObj.RTL_Count_InterestedProducts_Primary__c+1;
                            leadsToUpdate.add(leadObj);
                        } else if (oldProductObj.Is_Primary__c && !newProductObj.Is_Primary__c) {
                            leadObj.RTL_Count_InterestedProducts_Primary__c = leadObj.RTL_Count_InterestedProducts_Primary__c-1;
                            leadsToUpdate.add(leadObj);                    
                        }
                    } else if (oldMap != null) {//delete product
                        //only update lead object when a primary product is deleted
                        if (tmpProductObj.Is_Primary__c) {
                            leadObj.RTL_Count_InterestedProducts_Primary__c = leadObj.RTL_Count_InterestedProducts_Primary__c-1;
                            leadsToUpdate.add(leadObj);
                        }                    
                    } else if (newMap != null) {//insert product
                        //only update lead object when a primary product is added
                        if (tmpProductObj.Is_Primary__c) {
                            if (leadObj.RTL_Count_InterestedProducts_Primary__c != null) {
                                leadObj.RTL_Count_InterestedProducts_Primary__c = leadObj.RTL_Count_InterestedProducts_Primary__c+1;
                            } else {//when creating interested product from web-to-lead
                                leadObj.RTL_Count_InterestedProducts_Primary__c = 1;
                            }
                            leadsToUpdate.add(leadObj);
                        }                    
                    }
                //}
            }       
        }
        
        if (leadsToUpdate.size() > 0) {
            Database.SaveResult[] lsr = Database.update(leadsToUpdate, false);

            // Iterate through each returned result
            for (Database.SaveResult sr : lsr) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully updated RTL_Count_InterestedProducts_Primary__c of lead.');
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug(logginglevel.ERROR, 'There is error updating RTL_Count_InterestedProducts_Primary__c of lead. Error Message is: ' + err.getMessage());
                    }
                }
            }
        }   
    }     
}