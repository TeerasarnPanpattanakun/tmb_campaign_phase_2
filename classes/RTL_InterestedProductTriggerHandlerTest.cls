@IsTest

public with sharing class RTL_InterestedProductTriggerHandlerTest {
    static {
        TestUtils.createAppConfig();
    }   
    
    public static testmethod void checkPrimaryProductCountLead(){
        System.debug(':::: checkPrimaryProductCountLead Start ::::');  
    
        TEST.startTest();
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        //create test lead queue
        RTL_TestUtility.createLeadQueues(true); 
        System.runAs(retailUser) {
            //create branch and zone
            List<Branch_and_Zone__c> branchList = RTL_TestUtility.createBranchZone(2, true);            
            //create retail master product
            RTL_TestUtility.createRetailMasterProducts(true);
            //create new test lead     
            List<Lead> leads = RTL_TestUtility.createLeads(1, true);
            //create 2 new interested products for lead with 1 primary and 1 non-primary
            RTL_TestUtility.createInterestedProducts(leads, true);
            //check result: the count of primary interested product should be 1
            Lead lead = [select RTL_Count_InterestedProducts_Primary__c from Lead where Id=:leads[0].Id];
            System.assertEquals(1, lead.RTL_Count_InterestedProducts_Primary__c);
            
            //update the non-primary intested product into primary for the same lead
            RTL_Interested_products_c__c nonPrimaryProduct = [select Is_Primary__c from RTL_Interested_products_c__c where Lead__c = :lead.Id and Is_Primary__c = false];
            nonPrimaryProduct.Is_Primary__c = true;
            //check result: the update should be failed because no more than one primary interested product is allowed
            boolean errMsgMatch = false;
            try {
              update nonPrimaryProduct;
            } catch (Exception ex) {
              System.debug('no more than one primary interested product test case err msg = ' + ex.getMessage());
                errMsgMatch = ex.getMessage().contains(System.Label.LeadError_NoMoreThanOneInterestedProduct);
                System.assert(errMsgMatch);              
            }     
            
            //update one of the primary intested product into non-primary for the same lead
            RTL_Interested_products_c__c primaryProduct = [select Is_Primary__c from RTL_Interested_products_c__c where Lead__c = :lead.Id and Is_Primary__c = true limit 1];
            primaryProduct.Is_Primary__c = false;
            update primaryProduct;
            //check result: the count of primary interested product should be 1-1=0
            lead = [select RTL_Count_InterestedProducts_Primary__c from Lead where Id=:leads[0].Id];
            System.assertEquals(0, lead.RTL_Count_InterestedProducts_Primary__c);                   
            
            //delete the non-primary interested product from the same lead
            nonprimaryProduct = [select Id, Is_Primary__c from RTL_Interested_products_c__c where Lead__c = :lead.Id and Is_Primary__c = false limit 1];
            delete nonprimaryProduct;
            //check result: the count of primary interested product has no change, should be 0
            lead = [select RTL_Count_InterestedProducts_Primary__c from Lead where Id=:leads[0].Id];
            System.assertEquals(0, lead.RTL_Count_InterestedProducts_Primary__c);                                     
        }   

        TEST.stopTest();
        System.debug(':::: checkPrimaryProductCountLead End ::::');
    }    
    
    public static testmethod void checkPrimaryProductCountWebtoLead(){
        System.debug(':::: checkPrimaryProductCountWebtoLead Start ::::'); 

        TEST.startTest();
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        TestInit.createUser(false);
        User adminUser = TestInit.us;        
        System.runAs(adminUser) {
            //create test lead queue
            Map<String, Group> leadQueues = RTL_TestUtility.createLeadQueues(true);

            //create test branch
            RTL_TestUtility.createBranchZone(1, true);
            //create test product and web-to-lead assignment criterias
            RTL_TestUtility.createLeadAssignCriterias(true);
        }
        System.runAs(retailUser) {
            //create positive test data
            RTL_TestUtility.createPositiveWebToLead();
            Lead webtolead = [select RTL_Count_InterestedProducts_Primary__c from Lead where LastName='WebToLead' and FirstName='Test1'];
            //System.assertEquals(1, webtolead.RTL_Count_InterestedProducts_Primary__c);          
        }
        
        TEST.stopTest();
        System.debug(':::: checkPrimaryProductCountWebtoLead End ::::');    
    }    
}