@isTest
public class RTL_LeadTriggerHandlerTest {
    static List<Account> acctList;
    static List<Lead> leadList;
    static Map<String, Group> queueMap;
	static {
        TestUtils.createAppConfig();
        
    }
    
    static testmethod void testLeadConversion(){
        Test.startTest();
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        System.runAs(retailUser) {
            RTL_TestUtility.createRetailMasterProducts(true);
        	leadList = RTL_TestUtility.createLeads(2,true);
        	RTL_TestUtility.createInterestedProducts(leadList, true);
            List<Branch_and_Zone__c> branchList = RTL_TestUtility.createBranchZone(2, true);
        	
            leadList[0].Status = 'Qualified';
        	update leadList;

        	Database.LeadConvert lc = new database.LeadConvert();
			lc.setLeadId(leadList[0].id);
			lc.setDoNotCreateOpportunity(false);
			lc.setConvertedStatus('Closed Converted');

			Database.LeadConvertResult lcr = Database.convertLead(lc);
			System.assert(lcr.isSuccess());
        }
        Test.stopTest();
    }
    
    static testmethod void testLeadConversionFail(){
        Test.startTest();
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        System.runAs(retailUser) {
            RTL_TestUtility.createRetailMasterProducts(true);
        	leadList = RTL_TestUtility.createLeads(2,true);
            List<Branch_and_Zone__c> branchList = RTL_TestUtility.createBranchZone(2, true);
        	
            

        	Database.LeadConvert lc = new database.LeadConvert();
			lc.setLeadId(leadList[0].id);
			lc.setDoNotCreateOpportunity(false);
			lc.setConvertedStatus('Closed Converted');
            
            try{
                Database.LeadConvertResult lcr = Database.convertLead(lc);
            }catch(Exception e){
                
            }
            RTL_TestUtility.createInterestedProducts(leadList, true);
            leadList[0].Status = 'Qualified';
        	update leadList;

        }
        Test.stopTest();
    }
    
    static testmethod void testChangeLeadOwner(){
        Test.startTest();
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        System.runAs(retailUser) {
            RTL_TestUtility.createRetailMasterProducts(true);
        	leadList = RTL_TestUtility.createLeads(2,true);
        	RTL_TestUtility.createInterestedProducts(leadList, true);
            List<Branch_and_Zone__c> branchList = RTL_TestUtility.createBranchZone(2, true);
	
        }
        leadList[0].OwnerId = UserInfo.getUserId();
        update leadList;
        Test.stopTest();
    }
    
    static testmethod void testChangeLeadOwnerQueue(){
        Test.startTest();
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        System.runAs(retailUser) {
            queueMap = RTL_TestUtility.createLeadQueues(true);
            RTL_TestUtility.createRetailMasterProducts(true);
        	leadList = RTL_TestUtility.createLeads(2,true);
        	RTL_TestUtility.createInterestedProducts(leadList, true);
            List<Branch_and_Zone__c> branchList = RTL_TestUtility.createBranchZone(2, true);
            leadList[0].OwnerId = queueMap.get('RTLQ_001').id;
        	update leadList;
        }        
        Test.stopTest();
    }
    
    static testmethod void testChangeLeadOwnerOutboundQueue(){
        Test.startTest();
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        System.runAs(retailUser) {
            queueMap = RTL_TestUtility.createLeadQueues(true);
            RTL_TestUtility.createRetailMasterProducts(true);
        	leadList = RTL_TestUtility.createLeads(2,true);
        	RTL_TestUtility.createInterestedProducts(leadList, true);
            List<Branch_and_Zone__c> branchList = RTL_TestUtility.createBranchZone(2, true);
            leadList[0].OwnerId = queueMap.get('RTLQ_Outbound').id;
        	update leadList;
	
        }
  
        Test.stopTest();
    }
}