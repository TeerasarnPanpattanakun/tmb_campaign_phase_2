public class RTL_ReferralTriggerHandler {
    public static Boolean isBeforeInsert = false;
    
    private static RTL_Branch_Manager_Title__c branchTitle;
    private static Map<String,User> branchCodeUserMap = new Map<String,User>();
    private static Map<String,User> empCodeUserMap = new Map<String,User>();
    public static User currentUser{
    get{
        if(currentUser ==null){
            currentUser = [SELECT Id,Name,Employee_ID__c ,Segment__c ,Reporting_Segment__c ,
                            		Region__c ,Region_Code__c ,Zone__c ,Zone_Code__c ,RTL_Branch_Code__c ,
                            		RTL_Branch__c ,RTL_Channel__c,UserRole.Name,Profile.Name
                            FROM User
                           WHERE Id =: System.UserInfo.getUserId()];
        }
        return currentUser;
        
    }set;}
    
    public static Map<String, Branch_and_Zone__c> branchCodeMap{
    get{
        if(branchCodeMap ==null){
        	branchCodeMap = new Map<String, Branch_and_Zone__c>();
        	for(Branch_and_Zone__c branchzone : [Select Name, Branch_Code__c, RTL_Region_Code__c, RTL_Zone_Code__c,RTL_Zone__c, RTL_Region_Retail__c  
                                                 from Branch_and_Zone__c]) {
            	branchCodeMap.put(branchzone.Branch_Code__c, branchzone);
        	}
        }
        return branchCodeMap;
        
    }set;}
    
        
    public static void handleBeforeInsert(List<RTL_Referral__c> newList){
        if(isBeforeInsert == false){
            isBeforeInsert = true;
        }
        //filter only referral that attached to existing customer
		List<RTL_Referral__c> referralWithCust = new List<RTL_Referral__c>();
        List<RTL_Referral__c> referralToUpdate = new List<RTL_Referral__c>();
        List<RTL_Referral__c> referralWithRetailProduct = new List<RTL_Referral__c>();
        
        Set<Id> retailProductIdSet = new Set<Id>();
        Set<Id> accountIdSet = new Set<Id>();
        List<String> multiInterestedProduct = new List<String>();
        //loop through new referral list
        for(RTL_Referral__c referral : newList){
            //if attach to account keep account id into set
            if(referral.RTL_Account_Name__c != null){
                accountIdSet.add(referral.RTL_Account_Name__c);
                referralWithCust.add(referral);
            }
            
            //If created with Closed-xxx stage or Invalid (Duplicate Opportunity), stamp First Contacted
            if(referral.RTL_Stage__c.containsIgnoreCase('Closed') || referral.RTL_Stage__c == 'Invalid (Duplicate Opportunity)'){
				referralToUpdate.add(referral);
            }
            
            if(referral.RTL_Product_Name__c != null){
                retailProductIdSet.add(referral.RTL_Product_Name__c);
                referralWithRetailProduct.add(referral);
            }
            
            //Populate interested product on reporting product field
            if(referral.RTL_Interested_Product__c != null){   
                referral.RTL_Reporting_Product__c = sortMultiInterestedProduct(referral.RTL_Interested_Product__c);
            }
            
            if(referral.RTL_Stage__c == 'Closed (Interested)'){
                referral.addError(System.Label.RTL_Referral_ERR006);
            }
            
            
            
            if(referral.RTL_Preferred_Branch__c == null && branchCodeMap.containsKey(currentUser.RTL_Branch_Code__c) ){
                referral.RTL_Preferred_Branch__c = branchCodeMap.get(currentUser.RTL_Branch_Code__c).id;
               
            }
            if(referral.RTL_Preferred_Branch__c == null && referral.RTL_RecordType_Name__c != 'Refer Within Commercial'){
            	referral.RTL_Preferred_Branch__c.addError(System.Label.RTL_Required_Field);
            }

            //Stamp Created Date as Referred Date
            referral.RTL_Referred_Date_time__c = System.now();
            referral.RTL_Referred_Date__c = System.today();
        }
        
        
        
        if(referralWithCust.size() > 0){
        	//query account information to stamp on referral record
        	Map<Id,Account> accountMap = queryAccount(accountIdSet);
            setAccountInfo(referralWithCust,accountMap);
        }
        
        if(referralToUpdate.size() > 0){
        	updateFirstContactedInfo(referralToUpdate);
        	updateClosedInfo(referralToUpdate);
        }
         
        if(retailProductIdSet.size() > 0){
            Map<Id,RTL_Product_Master__c> productMap = queryRetailProduct(retailProductIdSet);
            for(RTL_Referral__c referral : referralWithRetailProduct){
           		referral.RTL_Reporting_Product__c = productMap.get(referral.RTL_Product_Name__c).Product_Group__c;
            }
        }
        
        
        
        
        new RTL_ReferralAssignment(newList);
        Set<Id> ownerIdSet = new Set<Id>();
        for(RTL_Referral__c referral : newList){
            ownerIdSet.add(referral.OwnerId);
        }
        Map<Id, User> userMap = queryUser(ownerIdSet);
        Map<Id, Group> queueMap = queryQueue(ownerIdSet);
        updateLastAssignedInfo(newList);
        updateOwnerInfo(newList,userMap,queueMap);
        

    }
	
    public static void handleAfterInsert(List<RTL_Referral__c> newList){
    	manualShareCreatorRecord(newList);
    }
    
    public static void handleBeforeUpdate(Map<Id,RTL_Referral__c> oldMap,Map<Id,RTL_Referral__c> newMap){
        List<RTL_Referral__c> referralWithCust = new List<RTL_Referral__c>();
        List<RTL_Referral__c> referralToClosed = new List<RTL_Referral__c>();
        List<RTL_Referral__c> referralToFirstContacted = new List<RTL_Referral__c>();
        List<RTL_Referral__c> referralOwnerChanged = new List<RTL_Referral__c>();
        List<RTL_Referral__c> referralAssign  = new List<RTL_Referral__c>();
        List<RTL_Referral__c> referralWithRetailProduct = new List<RTL_Referral__c>();
        
        
        Set<Id> retailProductIdSet = new Set<Id>();
        Set<Id> accountIdSet = new Set<Id>();
        Set<Id> ownerIdSet = new Set<Id>();
        
        Map<Id,Opportunity> opportunityMap = queryOpportunity(newMap.keySet());
        Map<Id,UserRecordAccess> recordAccessMap = queryRecordaccess(newMap.keySet());
        
        System.debug('Referral Before Update');
        for(Id referralId : newMap.keySet()){
            RTL_Referral__c newReferral = newMap.get(referralId);
            RTL_Referral__c oldReferral = oldMap.get(referralId);
            System.debug(newReferral);
            System.debug(oldReferral);
            UserRecordAccess recordAccess = recordAccessMap.containsKey(referralId)?recordAccessMap.get(referralId):null; 
                
            newReferral.RTL_Reporting_Product__c = String.ValueOf(newReferral.RTL_Interested_Product__c);

            
            //If Stage Changed
            if(newReferral.RTL_Stage__c != oldReferral.RTL_Stage__c){
                //if New to Closed ,Invalid (Duplicate Opportunity) , In progress
                if((oldReferral.RTL_Stage__c == 'New' && 
                    (newReferral.RTL_Stage__c.containsIgnoreCase('Closed') || newReferral.RTL_Stage__c == 'Invalid (Duplicate Opportunity)'))||
                    newReferral.RTL_Stage__c == 'In progress_Contacted'){
                    referralToFirstContacted.add(newReferral);
                }
				
                //prevent users from Closed (Interested) Referral without opportunity
                if(newReferral.RTL_Stage__c == 'Closed (Interested)' && 
                   (opportunityMap.containsKey(newReferral.id) == false && 
                    RTL_ReferralOpportunityService.isOpportunityTrigger == false)){
                	newReferral.addError(System.Label.RTL_Referral_ERR006);
            	}
                //If any stage to closed
                if(newReferral.RTL_Stage__c.containsIgnoreCase('Closed') || newReferral.RTL_Stage__c == 'Invalid (Duplicate Opportunity)'){
                	referralToClosed.add(newReferral);
                }
            }
            
            if(newReferral.RTL_Account_Name__c != null && newReferral.RTL_Account_Name__c != oldReferral.RTL_Account_Name__c){
                referralWithCust.add(newReferral);
                accountIdSet.add(newReferral.RTL_Account_Name__c);
            }
                        
            //if owner changed
            if(newReferral.OwnerId != oldReferral.OwnerId){
                //Accept Referral from Accept Button
                if(newReferral.OwnerId == System.UserInfo.getUserId() && newReferral.RTL_Is_Accept_Button__c == true){
                    newReferral.RTL_Is_Accept_Button__c = false;
                    if(currentUser.Profile.Name.containsIgnoreCase('Retail') == false && recordAccess != null && recordAccess.hasTransferAccess == false){
                        newReferral.addError(System.Label.RTL_Referral_ERR003);
                    }else if(currentUser.Profile.Name.containsIgnoreCase('Retail') == true && oldReferral.RTL_isUserinQueue__c == false ){
                        newReferral.addError(System.Label.RTL_Referral_ERR003);
                    }
                }
                //Assign Referral
                else{
                    referralAssign.add(newReferral);
                }
                ownerIdSet.add(newReferral.OwnerId);
                referralOwnerChanged.add(newReferral);
            }else{
                if((currentUser.Profile.Name.containsIgnoreCase('Retail') == true || recordAccess != null && recordAccess.HasTransferAccess == false)&& newReferral.RTL_Is_Owner__c == false && isBeforeInsert == false){
                    newReferral.addError(System.Label.RTL_Referral_ERR004);
                }
            }
            
            if(newReferral.RTL_Product_Name__c != null){
                retailProductIdSet.add(newReferral.RTL_Product_Name__c);
                referralWithRetailProduct.add(newReferral);
            }else{
                newReferral.RTL_Reporting_Product__c = null;
            }
            
            if(newReferral.RTL_Interested_Product__c != null){
                newReferral.RTL_Reporting_Product__c = sortMultiInterestedProduct(newReferral.RTL_Interested_Product__c);
            }else{
                newReferral.RTL_Reporting_Product__c = null;
            }
            
            
                
            //Clear Acccount Info if Account lookup to Referral is null
            if(newReferral.RTL_Account_Name__c == null){
                newReferral.RTL_Wealth_RM__c = null;
            	newReferral.RTL_Segment__c = null;
            	newReferral.RTL_TMB_Customer_ID__c = null;
            	newReferral.RTL_Privilege__c = null;
            	newReferral.RTL_Customer_Type__c = null;
                newReferral.RTL_RM_Owner__c = null;
                newReferral.RTL_RM_Owner_Id__c = null;
                newReferral.RTL_Wealth_RM_Id__c = null;
                newReferral.RTL_EMP_BR_Code__c = null;
                newReferral.RTL_Is_BM__c = false;
            }
        }
        
        
        //If Owner Changed
        if(referralOwnerChanged.size() > 0){
            Map<Id, User> userMap = queryUser(ownerIdSet);
            Map<Id, Group> queueMap = queryQueue(ownerIdSet);
            updateOwnerInfo(referralOwnerChanged,userMap,queueMap);
            
        }
        
        if(referralAssign.size() > 0){
            updateLastAssignedInfo(referralAssign);
        }

        
        if(referralWithCust.size() > 0){
            //query account information to stamp on referral record
        	Map<Id,Account> accountMap = queryAccount(accountIdSet);
            setAccountInfo(referralWithCust,accountMap);
        }
        
        
        
        if(referralToFirstContacted.size() > 0){
        	updateFirstContactedInfo(referralToFirstContacted);
        }
        
        if(referralToClosed.size() > 0){
        	updateClosedInfo(referralToClosed);
        }
        
        if(retailProductIdSet.size() > 0){
            Map<Id,RTL_Product_Master__c> productMap = queryRetailProduct(retailProductIdSet);
            for(RTL_Referral__c referral : referralWithRetailProduct){
           		referral.RTL_Reporting_Product__c = productMap.get(referral.RTL_Product_Name__c).Product_Group__c;
            }
        }

    }
    
    public static void handleAfterUpdate(Map<Id,RTL_Referral__c> oldMap,Map<Id,RTL_Referral__c> newMap){
        List<RTL_Referral__c> referralToShare = new List<RTL_Referral__c>();
        for(Id referralId : newMap.keySet()){
            RTL_Referral__c newReferral = newMap.get(referralId);
            RTL_Referral__c oldReferral = oldMap.get(referralId);
            //If owner changed
            if(newReferral.OwnerId != oldReferral.OwnerId){
                referralToShare.add(oldReferral);
            }
        }
        //share record back as 'Read' to last owner
        if(referralToShare.size() > 0){
            manualShareLastOwnerRecord(referralToShare);
        }
        
    }
    
    private static void setAccountInfo(List<RTL_Referral__c> referralList , Map<Id,Account> accountMap){
        Set<String> employeeIdSet = new Set<String>();
        
        for(Account acct : accountMap.values()){
            if(acct.RTL_RM_Name__c!= null && !acct.RTL_RM_Name__c.startsWith('00')){
                employeeIdSet.add(acct.RTL_RM_Name__c);
            }
        }
        queryUser(employeeIdSet);
        
        for(RTL_Referral__c referral : referralList){
            Account acct = accountMap.get(referral.RTL_Account_Name__c);
            //get wealth rm user information
            User rmUser = getRMUser(acct.RTL_RM_Name__c);
            //assign wealth rm information 
            if(rmUser != null){
                referral.RTL_Wealth_RM__c = rmUser.Name;
                referral.RTL_Wealth_RM_ID__c = rmUser.id;
                if(rmUser.IsActive == false){
                    referral.RTL_Wealth_RM__c += ' (INACTIVE)';
                }
                
            }

            referral.RTL_Segment__c = acct.Core_Banking_Suggested_Segment__c;
            referral.RTL_TMB_Customer_ID__c = acct.TMB_Customer_ID_PE__c;
            referral.RTL_Privilege__c = acct.RTL_Privilege1__c;
            referral.RTL_Customer_Type__c = acct.Account_Type__c;
            referral.RTL_Owner_Role__c = acct.Owner.UserRole.Name;
            referral.RTL_EMP_BR_Code__c = acct.RTL_RM_Name__c;
            if(acct.RTL_RM_Name__c != null){
                string empBRCode = acct.RTL_RM_Name__c;
                referral.RTL_Is_BM__c = empBRCode.startsWith('00');
            }
            if(referral.RTL_RecordType_Name__c == 'Retail Cross Channel Referral'){
                referral.RTL_FirstName__c = referral.RTL_FirstName__c==null?acct.First_Name_PE__c:referral.RTL_FirstName__c;
            	referral.RTL_LastName__c = referral.RTL_LastName__c==null?acct.Last_name_PE__c:referral.RTL_LastName__c;
            }
            
            if(acct.Account_Type__c != 'Retail Customer' && acct.Account_Type__c != 'Retail Prospect'){
                referral.RTL_RM_Owner__c = acct.Owner.Name;
            	referral.RTL_RM_Owner_Id__c = acct.Owner.Id;
                
            }
            //Map Sales amount per year with segment of existing customer
            if(acct.Account_Type__c == 'Existing Customer'){
                referral.RTL_Sales_Amount_Per_Year__c = getSalesAmountPerYear(referral.RTL_Segment__c);
            }
        }
    }
    
    private static void manualShareCreatorRecord(List<RTL_Referral__c> referralList){
    	System.debug('Start Share for Creator');
    	List<RTL_Referral__Share> referralShares  = new List<RTL_Referral__Share>();

    	// For each of the Job records being inserted, do the following:
    	for(RTL_Referral__c referral : referralList){
            if(referral.RTL_RecordType_Name__c != 'Retail Cross Channel Referral'){
                System.debug('Share to '+referral.CreatedById);
                // Create a new RTL_Referral__Share record to be inserted in to the RTL_Referral__Share table.
        		RTL_Referral__Share priorOwnerShare = createManualShare(referral.id,referral.CreatedById,'Edit',Schema.RTL_Referral__Share.RowCause.Creator_Sharing__c);
        		// Add the new Share record to the list of new Share records.
        		referralShares.add(priorOwnerShare);
            }
        
    	}
        
        if(referralShares.size() > 0){
            // Insert all of the newly created Share records and capture save result 
    		Database.SaveResult[] referralShareInsertResult = Database.insert(referralShares,false); 
        	System.debug(referralShareInsertResult);
        }
    	
    }
    
    private static void manualShareLastOwnerRecord(List<RTL_Referral__c> referralList){
    	List<RTL_Referral__Share> referralShares  = new List<RTL_Referral__Share>();

    	// For each of the Job records being inserted, do the following:
    	for(RTL_Referral__c referral : referralList){
            if(string.valueOf(referral.OwnerId).startsWith('005') && (referral.RTL_RecordType_Name__c == 'Refer to Commercial' || referral.RTL_RecordType_Name__c == 'Refer Within Commercial' )){
                // Create a new RTL_Referral__Share record to be inserted in to the RTL_Referral__Share table.
        		RTL_Referral__Share priorOwnerShare = createManualShare(referral.id,referral.OwnerId,'Edit',Schema.RTL_Referral__Share.RowCause.Prior_Owner_Sharing__c);
        		// Add the new Share record to the list of new Share records.
        		referralShares.add(priorOwnerShare);
            }
        
    	}
        
        if(referralShares.size() > 0){
            // Insert all of the newly created Share records and capture save result 
    		Database.SaveResult[] referralShareInsertResult = Database.insert(referralShares,false);   
        }
    	     
    }
    
    private static RTL_Referral__Share createManualShare(String parentId,String userOrGroupId,String accessLevel,String cause){
        RTL_Referral__Share priorOwnerShare = new RTL_Referral__Share();
        // Populate the RTL_Referral__Share record with the ID of the record to be shared.
        priorOwnerShare.ParentId = parentId;
        priorOwnerShare.UserOrGroupId = userOrGroupId;
       	priorOwnerShare.AccessLevel = accessLevel;
        priorOwnerShare.RowCause = cause;
        return priorOwnerShare;
    }
    private static void updateFirstContactedInfo(List<RTL_Referral__c> referralList){
        for(RTL_Referral__c referral : referralList){
            referral.RTL_First_Contacted_Date__c = System.now();/*Type : Datetime*/
            referral.RTL_First_Contacted_Date2__c = System.today();/*CR SLA Exclude Weekend*/
            referral.RTL_First_Contacted_Name__c = currentUser.Name;
            referral.RTL_First_Contacted_Branch_Team_NameCode__c  = currentUser.RTL_Branch__c;
            referral.RTL_First_Contacted_Branch_Code__c = currentUser.RTL_Branch_Code__c;
            referral.RTL_First_Contacted_Region__c = currentUser.Region__c;
            referral.RTL_First_Contacted_Region_Code__c = currentUser.Region_Code__c;
            referral.RTL_First_Contacted_Zone__c  = currentUser.Zone__c;
            referral.RTL_First_Contacted_Zone_Code__c = currentUser.Zone_Code__c;
            referral.RTL_First_Contacted_Segment__c  = currentUser.RTL_Channel__c==null?currentUser.Segment__c:currentUser.RTL_Channel__c;
            referral.RTL_First_Contacted_Reporting_Segment__c  = currentUser.Reporting_Segment__c;     
        }
    }
    
    private static void updateClosedInfo(List<RTL_Referral__c> referralList){

        for(RTL_Referral__c referral : referralList){
            referral.RTL_Closed_Date_Time__c  = System.now();
            referral.RTL_Closed_by_Name__c = currentUser.Name;
            referral.RTL_Closed_Date_Branch_Team_Name_Code__c = currentUser.RTL_Branch__c;
            referral.RTL_Closed_Branch_Code__c = currentUser.RTL_Branch_Code__c;
            referral.RTL_Closed_Date_Region__c = currentUser.Region__c;
            referral.RTL_Closed_Date_Region_Code__c = currentUser.Region_Code__c;
            referral.RTL_Closed_Date_Zone__c  = currentUser.Zone__c;
            referral.RTL_Closed_Date_Zone_Code__c = currentUser.Zone_Code__c;
            referral.RTL_Closed_Date_Segment__c  = currentUser.RTL_Channel__c==null?currentUser.Segment__c:currentUser.RTL_Channel__c;
            referral.RTL_Closed_Date_Reporting_Segment__c  = currentUser.Reporting_Segment__c;
      
        }
    }
    
    private static void updateOwnerInfo(List<RTL_Referral__c> referralList,Map<Id,User> userMap,Map<Id,Group> queueMap){
    	User user = null;
		Group queue = null;
        String branchCode = null;
        Branch_and_Zone__c branch = null;
        for(RTL_Referral__c referral : referralList){
        	user = userMap.get(referral.OwnerId);
            if (user != null){
            	referral.RTL_Owner_Branch__c = user.RTL_Branch__c;
                referral.RTL_Owner_Branch_Code__c = user.RTL_Branch_Code__c;
                referral.RTL_Owner_Region__c = user.Region__c;
                referral.RTL_Owner_Region_Code__c = user.Region_Code__c;
                referral.RTL_Owner_Zone__c = user.Zone__c;
                referral.RTL_Owner_Zone_Code__c = user.Zone_Code__c;
                referral.RTL_Owner_Segment__c = user.RTL_Channel__c==null?user.Segment__c:user.RTL_Channel__c;
                referral.RTL_Owner_Reporting_Segment__c = user.Reporting_Segment__c;
                referral.RTL_Owner_Role__c = user.UserRole.Name; 
                
            } else {
                //the referral owner could be a branch queue or wealth queue
                queue = queueMap.get(referral.OwnerId);
                branchCode = queue.DeveloperName;
               	if (branchCode.indexOf('_') != -1) {
                	branchCode = branchCode.substring(branchCode.indexOf('_') + 1, branchCode.length());
                }
                branch = branchCodeMap.get(branchCode);
                if (branch != null) {//branch queue
                    referral.RTL_Owner_Branch__c = branch.Name;
                    referral.RTL_Owner_Branch_Code__c = branch.branch_Code__c;
              		referral.RTL_Owner_Region__c = branch.RTL_Region_Retail__c ;
                    referral.RTL_Owner_Region_Code__c = branch.RTL_Region_Code__c; 
                	referral.RTL_Owner_Zone__c = branch.RTL_Zone__c;
                    referral.RTL_Owner_Zone_Code__c = branch.RTL_Zone_Code__c;
                	referral.RTL_Owner_Segment__c = null;
                	referral.RTL_Owner_Reporting_Segment__c = null;
                	referral.RTL_Owner_Role__c = null;        
                } else {//wealth queue
                	referral.RTL_Owner_Branch__c = null;
                    referral.RTL_Owner_Branch_Code__c = null;
              		referral.RTL_Owner_Region__c = null;
                    referral.RTL_Owner_Region_Code__c = null;  
                	referral.RTL_Owner_Zone__c = null;
                    referral.RTL_Owner_Zone_Code__c = null;
                	referral.RTL_Owner_Segment__c = null;
                	referral.RTL_Owner_Reporting_Segment__c = null;   
                    referral.RTL_Owner_Role__c = null; 
                }
            	
            }
        }
        
        
    }
    
    private static void updateLastAssignedInfo(List<RTL_Referral__c> referralList){
        for(RTL_Referral__c referral : referralList){
        	referral.RTL_Last_Assigned_Date_time__c = System.now();
            referral.RTL_Last_Assigned_Name__c = currentUser.Name;
            referral.RTL_Last_Assigned_Branch_Team_NameCode__c  = currentUser.RTL_Branch__c;
            referral.RTL_Last_Assigned_Branch_Code__c = currentUser.RTL_Branch_Code__c;
            referral.RTL_Last_Assigned_Region__c = currentUser.Region__c;
            referral.RTL_Last_Assigned_Region_Code__c = currentUser.Region_Code__c;
            referral.RTL_Last_Assigned_Zone__c  = currentUser.Zone__c;
            referral.RTL_Last_Assigned_Zone_Code__c = currentUser.Zone_Code__c;
            referral.RTL_Last_Assigned_Segment__c  = currentUser.RTL_Channel__c==null?currentUser.Segment__c:currentUser.RTL_Channel__c;
            referral.RTL_Last_Assigned_Reporting_Segment__c  = currentUser.Reporting_Segment__c;    
        }
        
        
    }
    
    private static String sortMultiInterestedProduct(String products){
        List<String> multiInterestedProduct = new List<String>();
        String reportingProduct = '';
        
        //get value from multipicklist field , split and put into list to do sorting
        multiInterestedProduct.addAll(products.split(';'));
        if(multiInterestedProduct.size() > 0){
        	//sort 
            multiInterestedProduct.sort();
            for(String product:multiInterestedProduct) {
            	reportingProduct+=product+'; ';
                System.debug(product);
            } 
            reportingProduct = reportingProduct.left(reportingProduct.length()-2);
        }
        return reportingProduct;
    }
    
    private static void queryUser(Set<String> employeeIdSet) {
        //get branch title from custom setting
        branchTitle = RTL_Branch_Manager_Title__c.getValues('Branch Manager');
        //Loop Through all user which employee id is not null or branch code is not null and is a branch manager
        for(User user : [SELECT ID,Name,ManagerId,Manager.Name,RTL_Branch_Code__c,Employee_Id__c,title,IsActive FROM User 
                         WHERE  (RTL_Branch_Code__c != null AND title =:branchTitle.RTL_Value__c) 
                                 OR Employee_ID__c IN:employeeIdSet ])
        {
            //if Branch Code is not null and is branch manager
            if(user.RTL_Branch_Code__c != null && user.title == branchTitle.RTL_Value__c){
                branchCodeUserMap.put(user.RTL_Branch_Code__c,user);
            }
            //if Employee Id is not null
            if(user.Employee_ID__c != null){
                empCodeUserMap.put(user.Employee_ID__c,user);
            }
        }
    }
    
    public static User getRMUser(String empBrCode){
        //If EMP/BR Code starts with 00 , find User with this branch code and is a branch manager
        if(empBrCode != null && empBrCode.startsWith('00')){
            empBrCode = empBrCode.substring(2);
            if(branchCodeUserMap.containsKey(empBrCode)){
                return branchCodeUserMap.get(empBrCode);
            }
        //else find a User with EMP/BR = Employee_Id   
        }else{
            if(empCodeUserMap.containsKey(empBrCode)){
                return empCodeUserMap.get(empBrCode);
            }
            
        }
        return null;
    }
        
    private static Map<Id,Account> queryAccount(Set<Id> accountIds){
        Map<Id,Account> accountMap = new Map<ID,Account>([SELECT Id,Name,First_name_PE__c ,Last_Name_Pe__c,
                                                          		 TMB_Customer_ID_PE__c,RTL_RM_Name__c,Owner.Name,
                                                          		 Core_Banking_Suggested_Segment__c,RTL_Privilege1__c,
                                                          		 Account_Type__c,Owner.UserRole.Name,Owner.Id
                                                          FROM Account WHERE ID IN : accountIds]);
        
        return accountMap;
    }
    
    private static Map<ID,RTL_Product_Master__c> queryRetailProduct(Set<Id> productIds){
        Map<Id,RTL_Product_Master__c> productMap = new Map<ID,RTL_Product_Master__c>([SELECT Id,Name,Product_Group__c,
                                                                         Product_Sub_Group__c
                                                                         FROM RTL_Product_Master__c
                                                                         WHERE ID IN : productIds]);
        return productMap;
    }
    
    private static Map<Id,User> queryUser (Set<Id> ownerIdSet){
        Map<Id, User> userMap = new Map<Id, User>();
        for(User u: [Select Id,Name, RTL_Branch__c, RTL_Branch_Code__c, Region_Code__c, 
                         Zone_Code__c, RTL_Channel__c,Region__c,Segment__c,Zone__c,
                         Reporting_Segment__c,UserRole.Name from User where id in :ownerIdSet])
            	userMap.put(u.Id, u);
		return userMap;      
    }
    
    private static Map<Id,Group> queryQueue (Set<Id> ownerIdSet){          
    	//Keep the list of the referral owner as queues
       	Map<Id, Group> queueMap = new Map<Id, Group>();
        for(Group q : [Select Id, DeveloperName from Group where Type = 'Queue' and id in :ownerIdSet])
        	queueMap.put(q.Id, q);
        return queueMap;
        
    }
    
    private static Map<Id,Opportunity> queryOpportunity (Set<Id> referralIdSet){          
    	//Keep the list of the referral with opportunity
       	Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>();
        for(Opportunity opp : [Select Id, Name,RTL_Referral__c from Opportunity WHERE id in :referralIdSet])
        	opportunityMap.put(opp.RTL_Referral__c, opp);
        return opportunityMap;
        
    }
               
    private static Map<Id,UserRecordAccess> queryRecordAccess(Set<Id> referralIdSet){
        Map<Id,UserRecordAccess> recordAccesssMap = new Map<Id,UserRecordAccess>();
        for(UserRecordAccess recordAccess : [SELECT RecordId,HasEditAccess, HasReadAccess, HasTransferAccess, MaxAccessLevel
     											FROM UserRecordAccess
     											WHERE UserId =: System.UserInfo.getUserId()
     											AND RecordId IN:referralIdSet])
            recordAccesssMap.put(recordAccess.recordId, recordAccess);
        
        return recordAccesssMap;
            

    }
    
    private static String getSalesAmountPerYear(String segment){
        if(segment == null){
            return null;
        }
        if(segment == 'SE'){
            return 'SE : <100 MB';
        }
        if(segment == 'BB'){
            return 'BB : 100-1,000 MB';
        }
        if(segment == 'CB'){
            return 'CB : 1,001-5,000 MB';
        }
        if(segment == 'MB'){
            return 'MB : >5,000 MB';
        }
        return null;

    }
    
}