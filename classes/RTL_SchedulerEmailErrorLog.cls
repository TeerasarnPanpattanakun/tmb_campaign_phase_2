global class RTL_SchedulerEmailErrorLog implements Schedulable{
	global void execute(SchedulableContext sc){	
        String query = 'select ID from RTL_Online_Service_Log__c where CreatedDate = YESTERDAY';
        RTL_BatchEmailErrorLog logPurge = new RTL_BatchEmailErrorLog(query);
        Id BatchProcessId = Database.ExecuteBatch(logPurge);
    }
}