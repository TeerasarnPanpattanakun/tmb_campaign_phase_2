@isTest
public class RTL_SchedulerEmailErrorLogTest {
	static {
        TestUtils.createAppConfig();
        RTL_TestUtility.createEmailConfig();
    } 	
	
	//use createddate to test since it's not able to change lastmodifieddate
    public static String query = 'select ID from RTL_Online_Service_Log__c where CreatedDate = TODAY';
    
    public static testMethod void scheduleEmailErrorLogTest(){
        //create retail online service log
        RTL_TestUtility.createOnlineServiceLog(100,true);  
        
        Test.startTest();
      	// Schedule the test job, but after Test.stopTest, the purge hasn't started.
      	String jobId = System.schedule('EmailErrorLogTest',
                        RTL_TestUtility.CRON_EXP, 
                        new RTL_SchedulerEmailErrorLog());
                        
      	// Get the information from the CronTrigger API object
      	CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

      	// Verify the expressions are the same
      	System.assertEquals(RTL_TestUtility.CRON_EXP, ct.CronExpression);
        
      	Test.stopTest();
    }
    
    public static testMethod void batchEmailErrorLogTest(){
        //create retail online service log
        RTL_TestUtility.createOnlineServiceLog(100,true);  
        
        Test.startTest();
		
        // Call the batch class in order to check the batch result
      	RTL_BatchEmailErrorLog emailLog = new RTL_BatchEmailErrorLog(query);
        Id BatchProcessId = Database.ExecuteBatch(emailLog);
                
      	Test.stopTest();
        
        
    }
}