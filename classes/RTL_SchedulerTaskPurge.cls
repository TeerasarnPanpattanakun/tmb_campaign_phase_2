global class RTL_SchedulerTaskPurge implements Schedulable{
	global void execute(SchedulableContext sc){
		//query retail task older than 3 months
        String query = 'select ID, RTL_NBO_History__c from Task where lastmodifieddate < LAST_N_MONTHS:3 and (recordtype.developername = \'RTL_Retail_Task\' or recordtype.developername = \'RTL_Retail_Outbound_Phone_Call\')';
        RTL_BatchTaskPurge taskPurge = new RTL_BatchTaskPurge(query);
        Id BatchProcessId = Database.ExecuteBatch(taskPurge);
    }   
}