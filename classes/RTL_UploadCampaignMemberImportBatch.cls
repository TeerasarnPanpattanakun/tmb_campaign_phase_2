global class RTL_UploadCampaignMemberImportBatch implements Queueable
{
	String ucmbId;
	RTL_Upload_Campaign_Member_Batch__c ucmb;
	RTL_Upload_Campaign_Member_Logs__c ucml;

	public RTL_UploadCampaignMemberImportBatch(RTL_Upload_Campaign_Member_Logs__c uploadCampaignMemberLog)
	{
		ucml = uploadCampaignMemberLog;

		String ucmlId = uploadCampaignMemberLog.id;

		// Get lowest sequnce of batch that status is inqueue to process
		RTL_Upload_Campaign_Member_Batch__c[] ucmbArray = [SELECT  ID,RTL_Status__c,RTL_Upload_Campaign_Member_Logs__c,
		 	RTL_Upload_Campaign_Member_Logs__r.RTL_CampaignID__c,
		 	RTL_Batch_No__c,RTL_Start_at_Row_No__c,RTL_End_at_Row_No__c
				FROM RTL_Upload_Campaign_Member_Batch__c 
				WHERE RTL_Status__c = 'In Queue'
				AND RTL_Upload_Campaign_Member_Logs__c = :ucmlId
				ORDER BY RTL_Batch_No__c
				LIMIT 1
				];

		if( ucmbArray.size() > 0 )
		{
			ucmbId = ucmbArray[0].id;
			ucmb = ucmbArray[0];
		}
		else 
		{
			ucmbId = null;
		}
		
	}

	private void sendNotificationEmail()
    {
        Attachment bodyfile =[SELECT ID,Name,ParentID from Attachment WHERE ParentID =:ucml.id LIMIT 1];
        String originalFileName = bodyfile.Name;
        String rawFileName = '';
        if (originalFileName.indexOf('.') > 0)
        {
            rawFileName = originalFileName.substring(0, originalFileName.lastIndexOf('.'));
        }
        else
        {
            rawFileName = originalFileName;
        }
        
        String logFileName = rawFileName + '[x-result log].csv';

        String userName = UserInfo.getUserName();
        User activeUser = [Select Email From User where Username = : userName limit 1];
        String userEmail = activeUser.Email;
        
        //PageReference pageRef = new PageReference('/apex/UploadCampaignMember');
        String tagerUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/ui/support/servicedesk/ServiceDeskPage#/apex/RTL_UploadCampaignMember' + '#logview';
        
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new String[] { userEmail };
        //message.optOutPolicy = 'FILTER';
        message.subject = Label.RTL_UploadCampaignMember_Email_Header;

        List<String> fillers = new String[]{ logFileName, tagerUrl };
        String content = String.format( Label.RTL_UploadCampaignMember_Email_Content , fillers );
        message.setHtmlBody(content);
        //message.setHtmlBody('Run Batch Import Upload Campaign Member Success, Please click below to view result<br />' +
        //                        '<b>File Name :<b> ' + logFileName + ' <br />' +
        //                        '<b>View:<b> <a href="'+ tagerUrl +'">click here</a><br />');
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        
    }

	//public void process()
	public void execute(QueueableContext context) {
				
		if( ucmbId == null )
		{
			// This mean no In Queue Campaing Batch for processing , send email here

			ucmL.RTL_IsInProgress__c = false; 
			ucmL.RTL_UploadStatus__c = 'Complete';
	    	update ucmL;

	    	sendNotificationEmail();

		}
		else 
		{
			Attachment bodyfile =[SELECT ID,Body,ParentID from Attachment WHERE ParentID =: ucmbId LIMIT 1];	

			String csvdetail = bodyfile.body.tostring();
                  	
          	// Prepare data from split CSV
            List<String> csvdetail2 = new list<String>();
            csvdetail2.addAll( RTL_UploadCampaignMemberService.safeSplit(csvdetail, '\r\n') ); 

            List<RTL_Upload_Campaign_Members__c> UploadCampMemList = new List<RTL_Upload_Campaign_Members__c>();

            // First loop to get all Product Name , Assigned Anget and Assigned Branch

            Set<String> productNameSet = new Set<String>();
            Set<String> assignedBranchSet = new Set<String>();
            Set<String> assignedAgentSet = new Set<String>();
            

            for(Integer i=1;i<csvdetail2.size();i++){
                List<String> csvRecordDataTemp = csvdetail2[i].trim().split((',(?=([^\"]*\"[^\"]*\")*[^\"]*$)'),-1);

                List<String> csvRecordData = new List<String>();
                // Remove quote from column CSV string
                for( Integer j=0 ; j<csvRecordDataTemp.size();j++){
                    csvRecordData.add(RTL_UploadCampaignMemberService.processCSVdata(csvRecordDataTemp[j]));
                }

                if(csvRecordData.size() == RTL_UploadCampaignMemberService.csvColumnNo) 
                {

                    String offerProduct = csvRecordData[35];
                    List<List<String>> productsOffer = RTL_UploadCampaignMemberCreateLead.processProductOfferString(offerProduct);

                    for( List<String> productItem : productsOffer  )
                    {
                        if( productItem.get(0) == '3' )
                        {
                            productNameSet.add(productItem.get(1));
                        }
                        
                    }
                    
                    String branchCode =  csvRecordData[9];
                    if( branchCode != '' )
                    {
                        assignedBranchSet.add(branchCode);
                    }

                    String agentCode =  csvRecordData[10];
                    if( agentCode != '' )
                    {
                        assignedAgentSet.add(agentCode);
                    }
                    
                }

            }

            
            List<RTL_product_master__c> productList = [SELECT ID,Name FROM RTL_product_master__c WHERE name in :productNameSet];

            Set<String> existingProductName = new Set<String>();
            for( RTL_product_master__c productObj : productList )
            {
                existingProductName.add(productObj.name);
            }

            Set<String> existingSubGroupName = new Set<String>();
            Schema.DescribeFieldResult fieldResult = CampaignMember.RTL_Sub_Group_1__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple){
                existingSubGroupName.add(pickListVal.getLabel().tolowercase() );
            }     
        

            Set<String> existingGroupName = new Set<String>();
            Schema.DescribeFieldResult fieldResult2 = CampaignMember.RTL_Product_Group_1__c.getDescribe();
            List<Schema.PicklistEntry> ple2 = fieldResult2.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple2){
                existingGroupName.add(pickListVal.getLabel().tolowercase() );
            }     

            Set<String> existingBranchCode = new Set<String>();
            for( Branch_and_Zone__c bc : [SELECT id,Branch_Code__c 
                    FROM Branch_and_Zone__c 
                    WHERE Branch_Code__c IN :assignedBranchSet])
            {
                existingBranchCode.add(bc.Branch_Code__c);
            }

            Set<String> existingAgentCode = new Set<String>();
            for(User us : [SELECT ID,Employee_ID__c
                        FROM User 
                        WHERE Employee_ID__c IN:assignedAgentSet])
            {
                existingAgentCode.add(us.Employee_ID__c);
            }

			// Loop to each CSV line and set up upload member
			//strart from row 2
            for(Integer i=1;i<csvdetail2.size();i++){

                    List<String> csvRecordDataTemp = csvdetail2[i].trim().split((',(?=([^\"]*\"[^\"]*\")*[^\"]*$)'),-1);

                    List<String> csvRecordData = new List<String>();

                    // Remove quote from column CSV string
					for( Integer j=0 ; j<csvRecordDataTemp.size();j++){
						csvRecordData.add(RTL_UploadCampaignMemberService.processCSVdata(csvRecordDataTemp[j]));
					}

					// If CSV column equal to 44 , process
					if(csvRecordData.size() == RTL_UploadCampaignMemberService.csvColumnNo) {	

                        boolean isAllBlank = true;
                        for(String recorddate : csvRecordData){
                            if(recorddate !=null && recorddate !=''){
                               isAllBlank = false;
                            }
                        }

						// prepare data for Upload_Campaign_Members__c
                        RTL_Upload_Campaign_Members__c ucm = new RTL_Upload_Campaign_Members__c();                               
                        
                        // check if record not blank in CSV
                        if(!isAllBlank){
                        	
                        	// Assign data to object
                            ucm.RTL_Row_No__c = i + ( ucmb.RTL_Start_at_Row_No__c - 1 );
                            ucm.RTL_CampaignID__c = ucmb.RTL_Upload_Campaign_Member_Logs__r.RTL_CampaignID__c;
                            ucm.RTL_Upload_Campaign_Member_Batch__c = ucmb.ID;
                            ucm.RTL_Upload_Campaign_Member_Logs__c = ucmb.RTL_Upload_Campaign_Member_Logs__c;

                            ucm.RTL_leadOwner__c = csvRecordData[0];

                            ucm.RTL_CHANNEL_DM__c = csvRecordData[1];
                            ucm.RTL_CHANNEL_OB__c = csvRecordData[2];
                            ucm.RTL_CHANNEL_SMS__c = csvRecordData[3];
                            ucm.RTL_CHANNEL_EDM__c = csvRecordData[4];
                            ucm.RTL_CHANNEL_BRANCH__c = csvRecordData[5];
                            ucm.RTL_CHANNEL_MIB__c = csvRecordData[6];
                            ucm.RTL_CHANNEL_OBD__c = csvRecordData[7];
                            ucm.RTL_CHANNEL_OTHER__c  = csvRecordData[8];
                            ucm.RTL_AssignedBranch__c  = csvRecordData[9];
                            ucm.RTL_AssignedRM__c = csvRecordData[10];

                            ucm.RTL_TMBCustID__c = csvRecordData[11];
                            ucm.RTL_Title__c = csvRecordData[12];
                            ucm.RTL_FirstName__c = csvRecordData[13];
                            ucm.RTL_LastName__c = csvRecordData[14];
                            ucm.RTL_GENDER__c = csvRecordData[15];
                            ucm.RTL_AGE__c = csvRecordData[16];
                            ucm.RTL_ID_Type__c = csvRecordData[17];
                            ucm.RTL_Citizen_ID__c = csvRecordData[18];
                            ucm.RTL_ADDRESS1__c = csvRecordData[19];
                            ucm.RTL_ADDRESS2__c = csvRecordData[20];

                            ucm.RTL_ADDRESS3__c = csvRecordData[21];
                            ucm.RTL_PROVINCE__c = csvRecordData[22];
                            ucm.RTL_ZIPCODE__c = csvRecordData[23];
                            ucm.RTL_MOBILEPHONE__c = csvRecordData[24];
                            ucm.RTL_HOMEPHONE__c = csvRecordData[25];
                            ucm.RTL_INCOME__c = csvRecordData[26];
                            ucm.RTL_OCCUPATION__c = csvRecordData[27];
                            ucm.RTL_Email_Address__c = csvRecordData[28];
                            ucm.RTL_FB_ID__c = csvRecordData[29];
                            ucm.RTL_FB_EMAIL__c = csvRecordData[30];

                            ucm.RTL_Customer_Segment__c = csvRecordData[31];
                            ucm.RTL_COST_PER_LEAD__c = csvRecordData[32];
                            ucm.RTL_SOURCE__c = csvRecordData[33];
                            ucm.RTL_UPDATE_DATE__c = csvRecordData[34];
                            ucm.RTL_PRODUCT_OFFER__c = csvRecordData[35];
                            ucm.RTL_REGISTER_DATE__c = csvRecordData[36];
                            ucm.RTL_DATA_SOURCE__c = csvRecordData[37];
                            ucm.RTL_PROMOTION__c = csvRecordData[38];
                            ucm.RTL_TARGET__c = csvRecordData[39];
                            ucm.RTL_PRODUCT_FEATURE__c = csvRecordData[40];

                            ucm.RTL_REMARK1__c = csvRecordData[41];
                            ucm.RTL_REMARK2__c = csvRecordData[42];
                            ucm.RTL_Rating__c = csvRecordData[43];

                            // if tmb cust id not defined  . name + surname and mobile phone within current job must unique
                            if(  ucm.RTL_TMBCustID__c == '' ||  ucm.RTL_TMBCustID__c == null )
                            {
                                //Verify data
                                if( ucm.RTL_MOBILEPHONE__c == '' )
                                {
                                    ucm.RTL_ImportStatus__c = 'Failed';
                                    ucm.RTL_ImportStatusDescription__c = 'Mobile Phone Cannot empty.';
                                    ucm.RTL_Batch_Name__c = RTL_UploadCampaignMemberService.guid();
                                    ucm.RTL_Batch_Telephone__c = RTL_UploadCampaignMemberService.guid();
                                }
                                else if( ucm.RTL_FirstName__c == '' &&  ucm.RTL_LastName__c == '')
                                {
                                    ucm.RTL_ImportStatus__c = 'Failed';
                                    ucm.RTL_ImportStatusDescription__c = 'Firstname and Lastname cannot empty.';
                                    ucm.RTL_Batch_Name__c = RTL_UploadCampaignMemberService.guid();
                                    ucm.RTL_Batch_Telephone__c = RTL_UploadCampaignMemberService.guid();
                                }
                                // If name and phone pass verify , create unique string for futher check duplicate
                                else 
                                {
                                    ucm.RTL_Batch_Name__c = ucm.RTL_FirstName__c +ucm.RTL_LastName__c  + ucmb.RTL_Upload_Campaign_Member_Logs__c;
                                    ucm.RTL_Batch_Telephone__c =  ucm.RTL_MOBILEPHONE__c + ucmb.RTL_Upload_Campaign_Member_Logs__c;
                                }
                            	
                            }
                            // Else , this field will empty , so set uuid to make it unique
                            else 
                            {
                            	ucm.RTL_Batch_Name__c = RTL_UploadCampaignMemberService.guid();
                            	ucm.RTL_Batch_Telephone__c = RTL_UploadCampaignMemberService.guid();
                            }

                            //======================= Verify Product ===================   
                            List<List<String>> productsOffer = RTL_UploadCampaignMemberCreateLead.processProductOfferString(csvRecordData[35]);

                            List<String> missingPorduct = new List<String>();
                            List<String> missingSubGroup = new List<String>();
                            List<String> missingGroup = new List<String>();

                            for( List<String> productItem : productsOffer  )
                            {
                                if( productItem.get(0) == '3' )
                                {
                                    String productName = productItem.get(1);
                                    if( !existingProductName.contains( productName ) )
                                    {
                                        missingPorduct.add(productName);
                                    }
                                }
                                else if( productItem.get(0) == '2' )
                                {
                                    String subGroupName = productItem.get(1);
                                    if( !existingSubGroupName.contains( subGroupName.tolowercase() ) )
                                    {
                                        missingSubGroup.add(subGroupName);
                                    }
                                    
                                }
                                else if( productItem.get(0) == '1' )
                                {
                                    String groupName = productItem.get(1);
                                    if( !existingGroupName.contains( groupName.tolowercase() ) )
                                    {
                                        missingGroup.add(groupName);
                                    }

                                    
                                }
                                
                            }

                            if( missingPorduct.size() > 0 || missingSubGroup.size() > 0 || missingGroup.size() > 0 )
                            {
                                ucm.RTL_ImportStatus__c = 'Failed';

                                String errorMsg = '';

                                if(  missingPorduct.size() > 0 )
                                {
                                    errorMsg += 'Product not found: {' + String.join(missingPorduct, ',')+'},';
                                }

                                if(  missingSubGroup.size() > 0 )
                                {
                                    errorMsg += 'SubGroup not found: {' + String.join(missingSubGroup, ',')+'},';
                                }

                                if(  missingGroup.size() > 0 )
                                {
                                    errorMsg += 'Group not found: {' + String.join(missingGroup, ',')+'},';
                                }

                                ucm.RTL_ImportStatusDescription__c = errorMsg;
                            }

                            // If both Agent and Branch not found
                            if( !existingBranchCode.contains( csvRecordData[9] ) &&  !existingAgentCode.contains( csvRecordData[10] ) )
                            {
                                ucm.RTL_ImportStatus__c = 'Failed';
                                ucm.RTL_ImportStatusDescription__c = 'Invalid both Agent Code and Branch Code';
                            }

                            //existingBranchCode
                            //existingAgentCode

                            //**** This List can cause max heap size ******
                            UploadCampMemList.add(ucm);
                        }   
                    }  
                }
		        //Attemp to insert campaign member to temp table
				Database.SaveResult[] ucmResultList = Database.insert(UploadCampMemList,false);

//================ do not check here for error ===========================================
		List<RTL_Upload_Campaign_Members__c> errorList = new List<RTL_Upload_Campaign_Members__c>();
		for (Integer i = 0; i < ucmResultList.size(); i++) {

			if( !ucmResultList.get(i).isSuccess() )
			{
				//RTL_Upload_Campaign_Members__c ucmEror = new RTL_Upload_Campaign_Members__c();
				RTL_Upload_Campaign_Members__c ucmEror = UploadCampMemList.get(i);

				//Database.Error error = ucmResultList.get(i).getErrors().get(0);
                //String errorMessage = 'Campaign Member Error: ' + error.getMessage();

                String errorMessage = '';
                ucmEror.RTL_ImportStatus__c = 'Duplicate';
                errorMessage = 'Duplicate Firstname/Lastname or Mobile Phone with previous record.';    
                

				
				ucmEror.RTL_ImportStatusDescription__c = errorMessage;

				ucmEror.RTL_Batch_Name__c = RTL_UploadCampaignMemberService.guid();
                ucmEror.RTL_Batch_Telephone__c = RTL_UploadCampaignMemberService.guid();

				// Add to error
				errorList.add(ucmEror);
			}

		}

		// Chain to other job to process error records (Avoid limitation exceed)

		RTL_UploadCampaignMemberImportError importErrorQueue = new RTL_UploadCampaignMemberImportError(ucml,ucmb,errorList);
        System.enqueueJob(importErrorQueue); 

		

		}

	}
}