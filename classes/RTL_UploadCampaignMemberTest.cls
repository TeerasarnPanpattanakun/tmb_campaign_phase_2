@isTest
private class RTL_UploadCampaignMemberTest {
	
    @testSetup static void setupData() {

        RTL_Campaign_Running_No__c rn = new RTL_Campaign_Running_No__c( 
                        Name ='Local Exclusive Campaign Running No.' , 
                        DateValue__c='170717',
                        Day__c='17',
                        Month__c='07',
                        Year__c='17',
                        Running_No__c = '01' );
        insert rn;

        List<AppConfig__c> mc = new List<AppConfig__c> ();
        mc.Add(new AppConfig__c(Name = 'runtrigger', Value__c = 'false'));
        insert mc;

        RecordType rt = [SELECT id FROM recordType WHERE DeveloperName   = 'Master_Campaign' ];

        List<User> u = TestUtils.createUsers(1, 'UserTest' ,'User1' , 'test@email.com' , false);
        //u[0].Employee_ID__c ='41735';
        insert u;


        Campaign c = TestUtils.createCampaign(1,'CB',false).get(0);
        c.RecordTypeId = rt.id;
        c.RTL_Campaign_Type__c  = 'Local Exclusive';
        insert c;

        Group gp = new Group();
        gp.Type = 'Queue';
        gp.Name = 'RTLQ_001';
        insert gp;

        Branch_and_Zone__c branch = new Branch_and_Zone__c();
        branch.RTL_Zone_Code__c = '6711';
        branch.Name = 'Test Branch';

        insert branch;

        List<Account> ac = TestUtils.createAccounts(1, 'TestAccount','Individual',false);
        ac[0].TMB_Customer_ID_PE__c = '001100000000000000000011279374';
        ac[0].recordTypeId = '01290000000iZYGAA2';
        insert ac;

        RTL_product_master__c pm1 = new RTL_product_master__c();
        pm1.RTL_Product2_Name__c = 'INVESTMENT';
        insert pm1;


        List<Contact> ct = TestUtils.createContacts(1,ac[0].id,false);
        ct[0].TMB_Customer_ID__c = '001100000000000000000011279374';
        insert ct;

        RTL_product_master__c p = new RTL_product_master__c(Name='TESTP',
                            Active__c=true);
        insert p;



    }
	
	@isTest 
	static void testUploadCSVWithoutCampaign() {

		
		List<String> listData = new list<String>{};
        listData.add('"LEAD_OWNER_QUEUE","CHANNEL_DM","CHANNEL_OB","CHANNEL_SMS","CHANNEL_EDM","CHANNEL_BRANCH","CHANNEL_MIB","CHANNEL_OBD","CHANNEL_OTHER","ASSIGN_BRANCH_CD","ASSIGN_EMP_ID","TMB_CUST_ID","TITLE","FIRSTNAME_TH","LASTNAME_TH","GENDER","AGE","ID TYPE","ID NUMBER","ADDRESS1","ADDRESS2","ADDRESS3","PROVINCE","ZIPCODE","MOBILEPHONE","HOMEPHONE","INCOME","OCCUPATION","EMAIL","FB_ID","FB_EMAIL","SEGMENT","COST_PER_LEAD","SOURCE","UPDATE_DATE","PRODUCT_OFFER","REGISTER_DATE","DATA_SOURCE","PROMOTION","TARGET","PRODUCT_FEATURE","REMARK1","REMARK2","RATING"');
        listData.add('"MM010","Y","Y","Y","Y","Y","Y","Y","Y","001",,,"Mr.","Lead 1","Last Name 1","M","27 – 40 years","Citizen ID","3560500414041","78 ม5 ","ดอนแก้ว","แม่ริม","เชียงใหม่","50180","0861839332","026339010","500","โปรแกรมเมอร์","123@mm.com","TeerasarnP","321@fb.com",,"50",,,"2:COMBO;",,,,,,"หมายเหตุ","หมายเหตุ 2","Hot "');
        String csvStringBody = '';
        
        for(integer j=0;j<listData.size();j++){             	         
            csvStringBody += listData.get(j)+'\r\n';
        }

        // Prepare CSV data
 		//String csvDataUTF8 = EncodingUtil.urlEncode(csvStringBody, 'UTF-8');
        Blob csvFile = Blob.valueof(csvStringBody);

        User u = [SELECT id FROM User WHERE Email= 'test@email.com'];
        Campaign c = [SELECT id FROM Campaign WHERE Name='Campaign_0'];
        Group g = [SELECT id FROM Group WHERE Name='RTLQ_001'];
        Branch_and_Zone__c b = [SELECT id FROM Branch_and_Zone__c WHERE RTL_Zone_Code__c = '6711'];

 		Test.startTest();

            System.runAs(u) 
            {
                RTL_UploadCampaignMemberService uc = new RTL_UploadCampaignMemberService();
                uc.csvFileBody = csvFile;
                uc.csvAsString = 'upload.csv';
                uc.importCSVFile();
                uc.getUploadLogs();
                uc.getIsAllowedUpload();

                
                uc.getPageMessage();

                List<SelectOption> CampaignSelectOptionList = uc.CampaignSelectOptionList;
                RecordType retailRCT = RTL_UploadCampaignMemberService.retailRCT;
                

                RTL_UploadCampaignMemberService.guid();
                RTL_UploadCampaignMemberService.processCSVdata( listData.get(1) );


            }

        Test.stopTest();
	}
	
	@isTest 
	static void testUploadCSVWithCampaign() {
		List<String> listData = new list<String>{};
        listData.add('"LEAD_OWNER_QUEUE","CHANNEL_DM","CHANNEL_OB","CHANNEL_SMS","CHANNEL_EDM","CHANNEL_BRANCH","CHANNEL_MIB","CHANNEL_OBD","CHANNEL_OTHER","ASSIGN_BRANCH_CD","ASSIGN_EMP_ID","TMB_CUST_ID","TITLE","FIRSTNAME_TH","LASTNAME_TH","GENDER","AGE","ID TYPE","ID NUMBER","ADDRESS1","ADDRESS2","ADDRESS3","PROVINCE","ZIPCODE","MOBILEPHONE","HOMEPHONE","INCOME","OCCUPATION","EMAIL","FB_ID","FB_EMAIL","SEGMENT","COST_PER_LEAD","SOURCE","UPDATE_DATE","PRODUCT_OFFER","REGISTER_DATE","DATA_SOURCE","PROMOTION","TARGET","PRODUCT_FEATURE","REMARK1","REMARK2","RATING"');
        listData.add('"MM010","Y","Y","Y","Y","Y","Y","Y","Y","001",,,"Mr.","Lead 1","Last Name 1","M","27 – 40 years","Citizen ID","3560500414041","78 ม5 ","ดอนแก้ว","แม่ริม","เชียงใหม่","50180","0861839332","026339010","500","โปรแกรมเมอร์","123@mm.com","TeerasarnP","321@fb.com",,"50",,,"2:COMBO;",,,,,,"หมายเหตุ","หมายเหตุ 2","Hot "');
        String csvStringBody = '';
        
        for(integer j=0;j<listData.size();j++){                          
            csvStringBody += listData.get(j)+'\r\n';
        }

        // Prepare CSV data
        //String csvDataUTF8 = EncodingUtil.urlEncode(csvStringBody, 'UTF-8');
        Blob csvFile = Blob.valueof(csvStringBody);

        User u = [SELECT id FROM User WHERE Email= 'test@email.com'];
        Campaign c = [SELECT id FROM Campaign WHERE Name='Campaign_0'];
        Group g = [SELECT id FROM Group WHERE Name='RTLQ_001'];
        Branch_and_Zone__c b = [SELECT id FROM Branch_and_Zone__c WHERE RTL_Zone_Code__c = '6711'];

        Test.startTest();

            System.runAs(u) 
            {
                RTL_UploadCampaignMemberService uc = new RTL_UploadCampaignMemberService();
                uc.csvFileBody = csvFile;
                uc.csvAsString = 'upload.csv';
                uc.selectedCampaignID = c.id;
                uc.importCSVFile();

            }
        Test.stopTest();
	}

    @isTest 
    static void testUploadCSVwrongFile() {
        List<String> listData = new list<String>{};
        listData.add('"LEAD_OWNER_QUEUE","CHANNEL_DM","CHANNEL_OB","CHANNEL_SMS","CHANNEL_EDM","CHANNEL_BRANCH","CHANNEL_MIB","CHANNEL_OBD","CHANNEL_OTHER","ASSIGN_BRANCH_CD","ASSIGN_EMP_ID","TMB_CUST_ID","TITLE","FIRSTNAME_TH","LASTNAME_TH","GENDER","AGE","ID TYPE","ID NUMBER","ADDRESS1","ADDRESS2","ADDRESS3","PROVINCE","ZIPCODE","MOBILEPHONE","HOMEPHONE","INCOME","OCCUPATION","EMAIL","FB_ID","FB_EMAIL","SEGMENT","COST_PER_LEAD","SOURCE","UPDATE_DATE","PRODUCT_OFFER","REGISTER_DATE","DATA_SOURCE","PROMOTION","TARGET","PRODUCT_FEATURE","REMARK1","REMARK2","RATING","MoreFiled"');
        listData.add('"MM010","Y","Y","Y","Y","Y","Y","Y","Y","001",,,"Mr.","Lead 1","Last Name 1","M","27 – 40 years","Citizen ID","3560500414041","78 ม5 ","ดอนแก้ว","แม่ริม","เชียงใหม่","50180","0861839332","026339010","500","โปรแกรมเมอร์","123@mm.com","TeerasarnP","321@fb.com",,"50",,,"2:COMBO;",,,,,,"หมายเหตุ","หมายเหตุ 2","Hot "');
        String csvStringBody = '';
        
        for(integer j=0;j<listData.size();j++){                          
            csvStringBody += listData.get(j)+'\r\n';
        }

        // Prepare CSV data
        //String csvDataUTF8 = EncodingUtil.urlEncode(csvStringBody, 'UTF-8');
        Blob csvFile = Blob.valueof(csvStringBody);

        User u = [SELECT id FROM User WHERE Email= 'test@email.com'];
        Campaign c = [SELECT id FROM Campaign WHERE Name='Campaign_0'];
        Group g = [SELECT id FROM Group WHERE Name='RTLQ_001'];
        Branch_and_Zone__c b = [SELECT id FROM Branch_and_Zone__c WHERE RTL_Zone_Code__c = '6711'];

        Test.startTest();

            System.runAs(u) 
            {
                RTL_UploadCampaignMemberService uc = new RTL_UploadCampaignMemberService();
                uc.csvFileBody = csvFile;
                uc.csvAsString = 'upload.csv';
                uc.importCSVFile();

            }
        Test.stopTest();
    }

    // @isTest 
    //static void testUploadCSVRowLimitFile() {
    //    List<String> listData = new list<String>{};
    //    listData.add('"LEAD_OWNER_QUEUE","CHANNEL_DM","CHANNEL_OB","CHANNEL_SMS","CHANNEL_EDM","CHANNEL_BRANCH","CHANNEL_MIB","CHANNEL_OBD","CHANNEL_OTHER","ASSIGN_BRANCH_CD","ASSIGN_EMP_ID","TMB_CUST_ID","TITLE","FIRSTNAME_TH","LASTNAME_TH","GENDER","AGE","ID TYPE","ID NUMBER","ADDRESS1","ADDRESS2","ADDRESS3","PROVINCE","ZIPCODE","MOBILEPHONE","HOMEPHONE","INCOME","OCCUPATION","EMAIL","FB_ID","FB_EMAIL","SEGMENT","COST_PER_LEAD","SOURCE","UPDATE_DATE","PRODUCT_OFFER","REGISTER_DATE","DATA_SOURCE","PROMOTION","TARGET","PRODUCT_FEATURE","REMARK1","REMARK2","RATING"');
        
    //    for(Integer x = 0;x<20002;x++)
    //    {
    //        listData.add('"MM010","Y","Y","Y","Y","Y","Y","Y","Y","00'+x+'",,,"Mr.","Lead 1","Last Name 1","M","27 – 40 years","Citizen ID","3560500414041","78 ม5 ","ดอนแก้ว","แม่ริม","เชียงใหม่","50180","0861839332","026339010","500","โปรแกรมเมอร์","123@mm.com","TeerasarnP","321@fb.com",,"50",,,"2:COMBO;",,,,,,"หมายเหตุ","หมายเหตุ 2","Hot "');
    //    }
    //    String csvStringBody = '';
        
    //    for(integer j=0;j<listData.size();j++){                          
    //        csvStringBody += listData.get(j)+'\r\n';
    //    }

    //    // Prepare CSV data
    //    //String csvDataUTF8 = EncodingUtil.urlEncode(csvStringBody, 'UTF-8');
    //    Blob csvFile = Blob.valueof(csvStringBody);

    //    User u = [SELECT id FROM User WHERE Email= 'test@email.com'];
    //    Campaign c = [SELECT id FROM Campaign WHERE Name='Campaign_0'];
    //    Group g = [SELECT id FROM Group WHERE Name='RTLQ_001'];
    //    Branch_and_Zone__c b = [SELECT id FROM Branch_and_Zone__c WHERE RTL_Zone_Code__c = '6711'];

    //    Test.startTest();

    //        System.runAs(u) 
    //        {
    //            RTL_UploadCampaignMemberService uc = new RTL_UploadCampaignMemberService();
    //            uc.csvFileBody = csvFile;
    //            uc.csvAsString = 'upload.csv';
    //            uc.importCSVFile();

    //        }
    //    Test.stopTest();
    //}


    @isTest
    static void testQueueWithCampaing()
    {

        List<String> listData = new list<String>{};
        listData.add('"LEAD_OWNER_QUEUE","CHANNEL_DM","CHANNEL_OB","CHANNEL_SMS","CHANNEL_EDM","CHANNEL_BRANCH","CHANNEL_MIB","CHANNEL_OBD","CHANNEL_OTHER","ASSIGN_BRANCH_CD","ASSIGN_EMP_ID","TMB_CUST_ID","TITLE","FIRSTNAME_TH","LASTNAME_TH","GENDER","AGE","ID TYPE","ID NUMBER","ADDRESS1","ADDRESS2","ADDRESS3","PROVINCE","ZIPCODE","MOBILEPHONE","HOMEPHONE","INCOME","OCCUPATION","EMAIL","FB_ID","FB_EMAIL","SEGMENT","COST_PER_LEAD","SOURCE","UPDATE_DATE","PRODUCT_OFFER","REGISTER_DATE","DATA_SOURCE","PROMOTION","TARGET","PRODUCT_FEATURE","REMARK1","REMARK2","RATING"');
        listData.add('"MM010","Y","Y","Y","Y","Y","Y","Y","Y","001",,,"Mr.","Lead 1","Last Name 1","M","27 – 40 years","Citizen ID","3560500414041","78 ม5 ","ดอนแก้ว","แม่ริม","เชียงใหม่","50180","0861839332","026339010","500","โปรแกรมเมอร์","123@mm.com","TeerasarnP","321@fb.com",,"50",,,"3:TESTP;",,,,,,"หมายเหตุ","หมายเหตุ 2","Hot "');
        //listData.add('"MM011","Y","Y","Y","Y","Y","Y","Y","Y","001",,,"Mr.","Lead 2","Last Name 2","M","27 – 40 years","Citizen ID","3560500414041","78 ม5 ","ดอนแก้ว","แม่ริม","เชียงใหม่","50180","0861839333","026339011","500","โปรแกรมเมอร์","123@mm.com","TeerasarnP","321@fb.com",,"50",,,"2:COMBO;",,,,,,"หมายเหตุ","หมายเหตุ 2","Hot "');
        listData.add('"MM012","Y","Y","Y","Y","Y","Y","Y","Y","002",,,"Mr.","Lead 1","Last Name 1","M","27 – 40 years","Citizen ID","3560500414041","78 ม5 ","ดอนแก้ว","แม่ริม","เชียงใหม่","50180","","026339010","500","โปรแกรมเมอร์","123@mm.com","TeerasarnP","321@fb.com",,"50",,,"2:COMBO;",,,,,,"หมายเหตุ","หมายเหตุ 2","Hot "');
        listData.add('"MM013","Y","Y","Y","Y","Y","Y","Y","Y","003",,,"","","","M","27 – 40 years","Citizen ID","3560500414041","78 ม5 ","ดอนแก้ว","แม่ริม","เชียงใหม่","50180","","026339010","500","โปรแกรมเมอร์","123@mm.com","TeerasarnP","321@fb.com",,"50",,,"2:COMBO;",,,,,,"หมายเหตุ","หมายเหตุ 2","Hot "');
        listData.add(',"Y","Y","Y",,"Y",,,,"001",,"001100000000000000000011279374",,,,,,"Citizen ID",,,,,,,,,,,,,,,,,,,,,,,,,,');
        listData.add('"RTLQ_001","Y",,,,,,,,"001",,,"Mr.","Lead 5","Last Name 5","M",,"Passport ID ","3560500414041",,,,,,"0861839336","026339014",,,"123@mm.com",,,,,,,,,,,,,,,"Low "');
        String csvStringBody = '';
        
        for(integer j=0;j<listData.size();j++){                          
            csvStringBody += listData.get(j)+'\r\n';
        }

        // Prepare CSV data
        //String csvDataUTF8 = EncodingUtil.urlEncode(csvStringBody, 'UTF-8');
        Blob csvFileBody = Blob.valueof(csvStringBody);

        User u = [SELECT id FROM User WHERE Email= 'test@email.com'];
        Campaign c = [SELECT id,Name FROM Campaign WHERE Name='Campaign_0'];
        Group g = [SELECT id FROM Group WHERE Name='RTLQ_001'];
        Branch_and_Zone__c b = [SELECT id FROM Branch_and_Zone__c WHERE RTL_Zone_Code__c = '6711'];

        Test.startTest(); 
            System.runAs(u) 
            {
                // new Upload_Campaign_Member_Logs__c
                RTL_Upload_Campaign_Member_Logs__c ucmL = new RTL_Upload_Campaign_Member_Logs__c();
                String batchName = '';
            
                ucmL.RTL_CampaignID__c = c.id;
                ucmL.RTL_Campaign__c = c.id;
                batchName = c.Name;
                
                ucmL.RTL_IsInProgress__c = true;
                ucmL.RTL_UploadStatus__c = 'Starting';
                // Insert new Upload_Campaign_Member_Logs__c
                insert ucmL;
                
                // Create new attachement from uploaded CSV 
                
                Attachment importattachmentfile = new Attachment();
                importattachmentfile.parentId = ucml.id;
                //importattachmentfile.name = 'Import Csv file : '+batchName+'.csv';
                importattachmentfile.name = 'Test';
                
                
                importattachmentfile.IsPrivate = true;
                importattachmentfile.body = csvFileBody;
                insert importattachmentfile;

                // Assign attacthment (CSV) to Upload_Campaign_Member_Logs__c
                ucml.RTL_Import_CSV_file_ID__c = importattachmentfile.id;
                
                // Update Upload_Campaign_Member_Logs__c (Add attachment)
                update ucml;

                RTL_UploadCampaignMemberSplitQueueable splitFileQueue = new RTL_UploadCampaignMemberSplitQueueable( ucml.id );
                splitFileQueue.execute(null);

                RTL_UploadCampaignMemberImportBatch importQueue = new RTL_UploadCampaignMemberImportBatch(ucml);
                importQueue.execute(null);

                



                //========== Make Recursive Queue end ===============
                RTL_Upload_Campaign_Member_Batch__c[] ucmbArray = [SELECT  ID,RTL_Status__c,RTL_Upload_Campaign_Member_Logs__c,
                    RTL_Upload_Campaign_Member_Logs__r.RTL_CampaignID__c,
                    RTL_Batch_No__c,RTL_Start_at_Row_No__c,RTL_End_at_Row_No__c
                        FROM RTL_Upload_Campaign_Member_Batch__c 
                        WHERE RTL_Status__c = 'In Queue'
                        AND RTL_Upload_Campaign_Member_Logs__c = :ucml.id
                        ORDER BY RTL_Batch_No__c
                        ];

                for(RTL_Upload_Campaign_Member_Batch__c ucmb : ucmbArray )
                {
                    ucmb.RTL_Status__c = 'Completed';
                }
                update ucmbArray;
                RTL_UploadCampaignMemberImportBatch importQueue2 = new RTL_UploadCampaignMemberImportBatch(ucml);
                importQueue2.execute(null);




            }
        Test.stopTest();

    }

    @isTest
    static void testQueueWithoutCampaing()
    {

        List<String> listData = new list<String>{};
        listData.add('"LEAD_OWNER_QUEUE","CHANNEL_DM","CHANNEL_OB","CHANNEL_SMS","CHANNEL_EDM","CHANNEL_BRANCH","CHANNEL_MIB","CHANNEL_OBD","CHANNEL_OTHER","ASSIGN_BRANCH_CD","ASSIGN_EMP_ID","TMB_CUST_ID","TITLE","FIRSTNAME_TH","LASTNAME_TH","GENDER","AGE","ID TYPE","ID NUMBER","ADDRESS1","ADDRESS2","ADDRESS3","PROVINCE","ZIPCODE","MOBILEPHONE","HOMEPHONE","INCOME","OCCUPATION","EMAIL","FB_ID","FB_EMAIL","SEGMENT","COST_PER_LEAD","SOURCE","UPDATE_DATE","PRODUCT_OFFER","REGISTER_DATE","DATA_SOURCE","PROMOTION","TARGET","PRODUCT_FEATURE","REMARK1","REMARK2","RATING"');
        listData.add('"MM010","Y","Y","Y","Y","Y","Y","Y","Y","001",,,"Mr.","Lead 1","Last Name 1","M","27 – 40 years","Citizen ID","3560500414041","78 ม5 ","ดอนแก้ว","แม่ริม","เชียงใหม่","50180","0861839332","026339010","500","โปรแกรมเมอร์","123@mm.com","TeerasarnP","321@fb.com",,"50",,,"2:COMBO;3:TESTP;1:INVESTMENT;3:TESTP;3:TESTP;3:TESTP;",,,,,,"หมายเหตุ","หมายเหตุ 2","Hot "');
        listData.add('"MM010","Y","Y","Y","Y","Y","Y","Y","Y","001",,,"Mr.","Lead 1","Last Name 1","M","27 – 40 years","Citizen ID","3560500414041","78 ม5 ","ดอนแก้ว","แม่ริม","เชียงใหม่","50180","0861839332","026339010","500","โปรแกรมเมอร์","123@mm.com","TeerasarnP","321@fb.com",,"50",,,"2:COMBO;",,,,,,"หมายเหตุ","หมายเหตุ 2","Hot "');
        listData.add('"MM010","Y","Y","Y","Y","Y","Y","Y","Y","002",,,"Mr.","Lead 1","Last Name 1","M","27 – 40 years","Citizen ID","3560500414041","78 ม5 ","ดอนแก้ว","แม่ริม","เชียงใหม่","50180","","026339010","500","โปรแกรมเมอร์","123@mm.com","TeerasarnP","321@fb.com",,"50",,,"2:COMBO;",,,,,,"หมายเหตุ","หมายเหตุ 2","Hot "');
        listData.add('"MM010","Y","Y","Y","Y","Y","Y","Y","Y","003",,,"","","","M","27 – 40 years","Citizen ID","3560500414041","78 ม5 ","ดอนแก้ว","แม่ริม","เชียงใหม่","50180","","026339010","500","โปรแกรมเมอร์","123@mm.com","TeerasarnP","321@fb.com",,"50",,,"2:COMBO;",,,,,,"หมายเหตุ","หมายเหตุ 2","Hot "');
        listData.add(',"Y","Y","Y",,"Y",,,,"001",,"001100000000000000000011279374",,,,,,"Citizen ID",,,,,,,,,,,,,,,,,,,,,,,,,,');
        listData.add(',"Y",,,"Y",,,,,"6711","12345","001100000000000000000011279384",,,,,,"Alien ID ",,,,,,,,,,,,,,,,,,,,,,,,,,');
        listData.add(',"Y",,,"Y",,,,,"6711","12345","001100000000000000000011279381",,,,,,"Work permit ID ",,,,,,,,,,,,,,,,,,,,,,,,,,');
        listData.add('"41735","Y","Y","Y","Y","Y","Y","Y","Y","001",,,"Mr.","Lead 1","Last Name 1","M","27 – 40 years","Citizen ID","3560500414041","78 ม5 ","ดอนแก้ว","แม่ริม","เชียงใหม่",50180,"0861839332","026339010","500","โปรแกรมเมอร์","123@mm.com","TeerasarnP","321@fb.com",,50,,,"1:INVESTMENT;2:MM;3:TESTP;3:TESTP;3:TESTP;3:TESTP;",,,,,,"หมายเหตุ","หมายเหตุ 2","Hot "');
        String csvStringBody = '';
        
        for(integer j=0;j<listData.size();j++){                          
            csvStringBody += listData.get(j)+'\r\n';
        }

        // Prepare CSV data
        //String csvDataUTF8 = EncodingUtil.urlEncode(csvStringBody, 'UTF-8');
        Blob csvFileBody = Blob.valueof(csvStringBody);

        User u = [SELECT id FROM User WHERE Email= 'test@email.com'];
        Campaign c = [SELECT id,Name FROM Campaign WHERE Name='Campaign_0'];
        Group g = [SELECT id FROM Group WHERE Name='RTLQ_001'];
        Branch_and_Zone__c b = [SELECT id FROM Branch_and_Zone__c WHERE RTL_Zone_Code__c = '6711'];

        Test.startTest(); 
            System.runAs(u) 
            {
                // new Upload_Campaign_Member_Logs__c
                RTL_Upload_Campaign_Member_Logs__c ucmL = new RTL_Upload_Campaign_Member_Logs__c();
                //String batchName = '';
            
                ucmL.RTL_CampaignID__c = null;
                ucmL.RTL_Campaign__c = null;
                //batchName = c.Name;
                
                ucmL.RTL_IsInProgress__c = true;
                ucmL.RTL_UploadStatus__c = 'Starting';
                // Insert new Upload_Campaign_Member_Logs__c
                insert ucmL;
                
                // Create new attachement from uploaded CSV 
                
                Attachment importattachmentfile = new Attachment();
                importattachmentfile.parentId = ucml.id;
                importattachmentfile.name = 'Test';
                
                
                importattachmentfile.IsPrivate = true;
                importattachmentfile.body = csvFileBody;
                insert importattachmentfile;

                // Assign attacthment (CSV) to Upload_Campaign_Member_Logs__c
                ucml.RTL_Import_CSV_file_ID__c = importattachmentfile.id;
                
                // Update Upload_Campaign_Member_Logs__c (Add attachment)
                update ucml;

                RTL_UploadCampaignMemberSplitQueueable splitFileQueue = new RTL_UploadCampaignMemberSplitQueueable( ucml.id );
                splitFileQueue.execute(null);

                RTL_UploadCampaignMemberImportBatch importQueue = new RTL_UploadCampaignMemberImportBatch(ucml);
                importQueue.execute(null);

                
            }
        Test.stopTest();

    }
	
}