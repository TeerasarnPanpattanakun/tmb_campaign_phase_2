public with sharing class RTL_Utility {
	public static String NBO_STATUS_ACCEPTEDALL = System.Label.RTL_NBO_Status_Accepted_All;
	public static String NBO_STATUS_INVALID = System.Label.RTL_NBO_Status_Invalid;
	public static String NBO_STATUS_NEW = System.Label.RTL_NBO_Status_New;
	public static String NBO_STATUS_REJECTEDALL = System.Label.RTL_NBO_Status_Rejected_All;
	/*
	*This method is used to retrieve a list of record type Ids based on the SObject and record type Developer Name prefix
	*/
	public static Set<Id> getObjectRecordTypeIdsByDevNamePrefix(SObjectType sObjectType, String recordTypeDevNamePrefix) {
		Set<Id> recordTypeIds = new Set<Id>();
		
        //Generate a map of tokens for all the Record Types for the desired object
        Map<String,Schema.RecordTypeInfo> rtMapByName = sObjectType.getDescribe().getRecordTypeInfosByName();

		for (String rtName : rtMapByName.keySet()){
			if (rtName.startsWith(recordTypeDevNamePrefix)) {
				recordTypeIds.add(rtMapByName.get(rtName).getRecordTypeId());
			}
		}

        //Retrieve the record type id by name
        return recordTypeIds;
	} 
	
	public static Map<String, Id> getObjectRecordTypeMap(String objectType) {//the key is record type developer name, the value is record type id
		Map<String, Id> rtMap = new Map<String, Id>();
		for (RecordType rt: [select Id, developername from RecordType where SobjectType = :objectType]) {
			rtMap.put(rt.developername, rt.Id);
		}
		return rtMap;
	}
	
	public static List<RTL_NBO_History_Product__c> validNBOProduct(List<RTL_NBO_History_Product__c> nboHistoryProductList, boolean isUpdate) {
        //Store the list of retail product master
        Set<String> retailProductbyGroupList = new Set<String>();//the key is product group
        Map<String, RTL_product_master__c> retailProductbySubGroupMap = new Map<String, RTL_product_master__c>();//the key is product subgroup
        Map<String, RTL_product_master__c> retailProductbyNameMap = new Map<String, RTL_product_master__c> (); //the key is product name
        for(RTL_product_master__c retailProduct : [Select Name, Product_Group__c, Product_Sub_group__c, RTL_Product_Smart_Info__c from RTL_product_master__c where Active__c = true]) {
            retailProductbyGroupList.add(toLowerCase(retailProduct.Product_Group__c));
            if (retailProductbySubGroupMap.get(toLowerCase(retailProduct.Product_Sub_group__c)) == null)
            	retailProductbySubGroupMap.put(toLowerCase(retailProduct.Product_Sub_group__c), retailProduct);
            if (retailProductbyNameMap.get(toLowerCase(retailProduct.Name)) == null)	
            	retailProductbyNameMap.put(toLowerCase(retailProduct.Name), retailProduct);
        }

        // get the list of NBO External ID to retrieve the related NBO History
        Set<String> nboExternalIdList = new Set<String>();
        for (RTL_NBO_History_Product__c nboHisProduct: nboHistoryProductList) {
            nboExternalIdList.add(nboHisProduct.RTL_NBO_ID__c);
        }
        
        //get the list of NBO History related with NBO History Product by exteranl NBO ID
        Map<String, RTL_NBO_History__c> nboHistoryExistingList = new Map<String, RTL_NBO_History__c>();//the key is NBO external Id
        for(RTL_NBO_History__c nboHis : [Select RTL_NBO_ID__c, RTL_Status__c from RTL_NBO_History__c where RTL_NBO_ID__c in : nboExternalIdList]) {
            nboHistoryExistingList.put(nboHis.RTL_NBO_ID__c, nboHis);
        }
        
        //get the list of NBO History product by exteranl NBO ID, for checking of duplication
        Set<String> nboHistoryProdExistingList = new Set<String>();
       	if (!isUpdate) {
        	for(RTL_NBO_History_Product__c nboHisProduct : [Select RTL_NBO_ID__c, RTL_Product__c, RTL_Product_Group__c,  RTL_Product_SubGroup__c from RTL_NBO_History_Product__c where RTL_NBO_ID__c in : nboExternalIdList]) {
            	nboHistoryProdExistingList.add(nboHisProduct.RTL_NBO_ID__c + nboHisProduct.RTL_Product__c + nboHisProduct.RTL_Product_Group__c + nboHisProduct.RTL_Product_SubGroup__c);
        	}
       	} 
       	
		Map<String, Id> rtMap = RTL_Utility.getObjectRecordTypeMap('RTL_NBO_History_Product__c');//key is recordtype developer name
		Map<String, String> productGroupMap = RTL_Utility.getNBOProductRecoredTypeMapping();//key is NBO product group       	
       	
       	//Id userId = UserInfo.getUserId();
       	//User currentUser = [select Region_Code__c, Zone_Code__c, RTL_Branch_Code__c from User where Id = :userId];
          
        String nboProductNameOrig, nboProductGroupOrig, nboProductSubGroupOrig = null;         
        String nboProductName, nboProductGroup, nboProductSubGroup = null;   
        RTL_NBO_History__c nboHis = null;
        RTL_product_master__c retailProduct = null;

        for (RTL_NBO_History_Product__c nboHisProduct: nboHistoryProductList) {//validate each NBO product
        	//populate current user branch/region/zone information to NBO Product
        	//nboHisProduct.RTL_User_Branch__c = currentUser.RTL_Branch_Code__c;//update user info by task object
        	//nboHisProduct.RTL_User_Region__c = currentUser.Region_Code__c;//update user info by task object
        	//nboHisProduct.RTL_User_Zone__c = currentUser.Zone_Code__c;//update user info by task object
        	//validate each NBO product/group/subgroup (assume backend will pass only one of them)
        	nboProductNameOrig = nboHisProduct.RTL_Product__c;
            nboProductName = toLowerCase(nboProductNameOrig);
            nboProductGroupOrig = RTL_Utility.convertProductGroup(nboHisProduct.RTL_Product_Group__c);
            nboProductGroup = toLowerCase(nboProductGroupOrig);
            nboProductSubGroupOrig = nboHisProduct.RTL_Product_SubGroup__c;
            nboProductSubGroup = toLowerCase(nboProductSubGroupOrig);  	 	
        	nboHis =  nboHistoryExistingList.get(nboHisProduct.RTL_NBO_ID__c);//check if the NBO exists in DB
            if (nboHis == null) {//this NBO History Product doesn't have related NBO History
                nboHisProduct.RTL_Invalid_Product_Reason__c = System.Label.RTL_NBO_ERR001;
                nboHisProduct.RTL_Status__c = 'Invalid';
            } else if (nboHistoryProdExistingList.contains(nboHisProduct.RTL_NBO_ID__c + nboProductName + nboProductGroup + nboProductSubGroup)) {
                //for insertion, skip if the NBO History product exists based on the combination of RTL_NBO_ID__c+RTL_Product__c+RTL_Product_Group__c+RTL_Product_SubGroup__c
                nboHisProduct.RTL_Invalid_Product_Reason__c = System.Label.RTL_NBO_ERR002 + ' NBO ID=' + nboHisProduct.RTL_NBO_ID__c + ', Product Group=' + nboProductGroupOrig + ', Product Subgroup=' + nboProductSubGroupOrig + ', Product Name=' + nboProductNameOrig + '.';     
                nboHisProduct.RTL_Status__c = 'Invalid';
            } else {
            	if (!isUpdate) nboHisProduct.RTL_Related_NBO__c = nboHis.Id; //map parent/child relationship
            	if (nboProductGroup == null && nboProductSubGroup == null && nboProductName == null) {//all 3 parameters are null
                	nboHisProduct.RTL_Invalid_Product_Reason__c = System.Label.RTL_NBO_ERR003;             
                	nboHisProduct.RTL_Status__c = 'Invalid';
            	} else if (nboProductGroup != null && nboProductSubGroup == null && nboProductName == null) { //only product group is provided
                    if (!retailProductbyGroupList.contains(nboProductGroup)) {//invalid NBO product group
                        //valid NBO product will be persisted automatically                    
                    	nboHisProduct.RTL_Invalid_Product_Reason__c = System.Label.RTL_NBO_ERR004 + ' Product Group=' + nboProductGroupOrig + '.';  
						nboHisProduct.RTL_Status__c = 'Invalid';
					}    
                } else if (nboProductGroup == null && nboProductSubGroup != null && nboProductName == null) {//only product subgroup is provided
                	retailProduct = retailProductbySubGroupMap.get(nboProductSubGroup);
                    if (retailProduct == null) {//invalid NBO product subgroup
                        //valid NBO product will be persisted automatically                    
                    	nboHisProduct.RTL_Invalid_Product_Reason__c = System.Label.RTL_NBO_ERR004 + ' Product Subgroup=' + nboProductSubGroupOrig + '.';  
						nboHisProduct.RTL_Status__c = 'Invalid';
					} else {//valid NBO product subgroup, find the NBO product group
						nboHisProduct.RTL_Product_Group__c = RTL_Utility.convertProductGroup(retailProduct.Product_Group__c);
					}
                } else if (nboProductGroup == null && nboProductSubGroup == null && nboProductName != null) {//only product name is provided
                	retailProduct = retailProductbyNameMap.get(nboProductName);
                    if (retailProduct == null) {//invalid NBO product name
                        //valid NBO product will be persisted automatically                    
                    	nboHisProduct.RTL_Invalid_Product_Reason__c = System.Label.RTL_NBO_ERR004 + ' Product Name=' + nboProductNameOrig + '.';  
						nboHisProduct.RTL_Status__c = 'Invalid';
					} else {//valid NBO product name, find the NBO product group and subgroup
						nboHisProduct.RTL_Product_Group__c = RTL_Utility.convertProductGroup(retailProduct.Product_Group__c);
						nboHisProduct.RTL_Product_SubGroup__c = retailProduct.Product_Sub_group__c;
						nboHisProduct.RTL_Product_Smart_Info__c = retailProduct.RTL_Product_Smart_Info__c;//update smart info only when Product Name is provided
					}											                       
                } else {
                	nboHisProduct.RTL_Invalid_Product_Reason__c = System.Label.RTL_NBO_ERR004 + ' Product Group=' + nboProductGroupOrig + ', Product Subgroup=' + nboProductSubGroupOrig + ', Product Name=' + nboProductNameOrig + '.';             
                	nboHisProduct.RTL_Status__c = 'Invalid';
                } 
            }  
			if (nboHisProduct != null && nboHisProduct.RTL_Product_Group__c != null && productGroupMap.get(nboHisProduct.RTL_Product_Group__c.toLowerCase()) != null)//set record type based on product group
				nboHisProduct.recordTypeId = rtMap.get(productGroupMap.get(nboHisProduct.RTL_Product_Group__c.toLowerCase()));                  
        }
        return nboHistoryProductList;	
	}
	
	public static String toLowerCase(String input){
		return input != null ? input.toLowerCase() : null;
	}
	
    public static Map<String, String> getNBOProductRecoredTypeMapping() {//the key is NBO product group, the value is NBO product record type developer name
        Map<String, String> mdtMap = new Map<String, String>();
        for (RTL_NBO_Product_Record_Type_Mapping__mdt mdt : [SELECT RTL_Product_Group__c, RTL_Record_Type_DevName__c FROM RTL_NBO_Product_Record_Type_Mapping__mdt]){
            mdtMap.put(mdt.RTL_Product_Group__c.toLowerCase(), mdt.RTL_Record_Type_DevName__c);
        }
        
        return mdtMap;
    }	
    
    /* This method is to convert product group to match with Opportunity Product Group picklist value */
    public static String convertProductGroup(String input) {
    	String output = null;
    	if (input != null) {
    		List<String> productGrpOptions = getOpptProductGrpOptions();
    		for (String option: productGrpOptions) {
    			if (input.toLowerCase() == option.toLowerCase()) {
    				return option;
    			}
    		}
    		return input;
    	}
    	return input;
    }
    
    /* This method returns the list of Opportunity Product Group picklist values */
    public static List<String> getOpptProductGrpOptions()
    {
        List<String> productGrpOptions = new List<String>();
        Schema.DescribeFieldResult fieldResult = Opportunity.RTL_Product_Group__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry f : ple)
        {
			productGrpOptions.add(f.getValue());
        }  
        return productGrpOptions;      
    } 
    
    /* This method returns the list of NBO Product Field translation mapping */
    public static Map<String, String> getNBOProductFieldTranslationMapping() {//the key is field value in english, the value is field value label
        Map<String, String> mdtMap = new Map<String, String>();
        for (RTL_NBO_Product_Field_Tracking__mdt mdt : [SELECT RTL_Value_English__c, RTL_Value_Label__c FROM RTL_NBO_Product_Field_Tracking__mdt]){
            mdtMap.put(mdt.RTL_Value_English__c, mdt.RTL_Value_Label__c);
        }
        
        return mdtMap;
    }
    
    public static void InsertErrorTransaction(String cName , String rmid,String uName,String muleLogId,String errorMessages,String cSegment){
    	RTL_Online_Service_Log__c Log = new RTL_Online_Service_Log__c ();
    	Log.RTL_Customer_Name__c  = cName;
    	Log.RTL_RM_ID__c = rmid;
        Log.RTL_Name_of_User__c = uName;
    	Log.RTL_Mulesoft_Log_Id__c = muleLogId;
    	Log.RTL_Error_Message__c = errorMessages;
        Log.RTL_Customer_Segment__c = cSegment;
        insert Log;          
    }

    //Added on 29 June 2017
    public static String queryAllField(String objName) {
        String strQuery = '';
        List<String> q = new List<String>();
        Map<String, Schema.SObjectType> m  = Schema.getGlobalDescribe() ;
        Schema.DescribeSObjectResult r =  m.get(objName).getDescribe();
        List<String>apiNames =  new list<String>();
        for(string apiName : r.fields.getMap().keySet()){
            q.add(apiName);
        }
        for( String s : q ){
            strQuery += s+',';
        }
        return strQuery;
    }

    public static Map<Id,RecordType> getRecordTypeById(List<Id> recordtypeIdList){
        return new Map<Id,RecordType>([ select Id,DeveloperName 
                from RecordType 
                where Id IN :recordtypeIdList  ]);
    }

    public static Boolean isNotNull(String cond){
        if( cond == null || cond == '' ){
            return false;
        }
        return true;
    }
    //Added on 29 June 2017


    //get label values picklist
    public static string getLabelPicklist (String picklistValues,String apiName,String objectName){
        string label;

        // Get the describe for the object
        DescribeSObjectResult objResult = Schema.getGlobalDescribe().get(objectName).getDescribe();
        // Get the field dynamically
        DescribeFieldResult fieldResult = objResult.fields.getMap().get(apiName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            if( picklistValues != null && picklistValues.equals(f.getValue())){
                label = f.getLabel();
                break;
            }
        }

        if(label == null){
            label = (picklistValues!=null)?picklistValues:'';
        }

        return label;
    }
}