public class UserTriggerHandler {
    static Boolean detectError = false;
    static String errorException = '';
    static String STR_INSERT = 'insert';
    static String STR_UPDATE = 'update';
    static String STR_DELETE = 'delete';
    static Id sfadmId = [SELECT Id FROM User WHERE Name = 'sfadm System Account'].Id;
    Static List<Trigger_Msg__c> TriggerMsgList = [SELECT NAME,Description__c FROM Trigger_Msg__c 
                                                  WHERE Name ='User_trigger_error_change'
                                                  OR Name = 'User_trigger_error_insert'];
    // NOTE :  When we codding in Before Context we not use update in Accout list because it will update autometically!!!
    
    public static void handleBeforeInsert(List<User> usersNew)
    {
        boolean ch = false;
        for (ObjUserPermission__c oup : ObjUserPermission__c.getAll().values())
        {
            if(oup.Set_ID__c == UserInfo.getUserId() || oup.Set_ID__c == UserInfo.getProfileId() || oup.Name == UserInfo.getUserName())
            {
                ch = true;
                break;
            }
        }
        
        if(!ch)
        {
            system.debug('Get Data from Custom setting 1');
            for(User uses : usersNew)
            {
                for(Trigger_Msg__c msg : TriggerMsgList){
                    if(msg.Name == 'User_trigger_error_insert'){
                      uses.addError(msg.Description__c); 
                    }
                }
               
                break;
            }
        }
        
    }
    
    public static void handleBeforeUpdate(List<User> usersNew,Map<Id,User> usersOldMap)
    {
        boolean ch = false;
        for (ObjUserPermission__c oup : ObjUserPermission__c.getAll().values())
        {
            if(oup.Set_ID__c == UserInfo.getUserId() || oup.Set_ID__c == UserInfo.getProfileId() || oup.Name == UserInfo.getUserName())
            {
                ch = true;
                break;
            }
        }
        if(!ch)
        {
            system.debug('Get Data from Custom setting 2');
            for(User uses : usersNew)
            {
				for(Trigger_Msg__c msg : TriggerMsgList){
                    if(msg.Name == 'User_trigger_error_change'){
                      uses.addError(msg.Description__c); 
                    }
                }
                break;
            }
        }
        updateUserByIDM(usersNew);
        
        /*** CANCLE AS OF 10/03/2017 : BACK TO IDM RECONCILE SOLUTION***/
        //updateUserforInactive(usersNew,usersOldMap);
    }
    
    public static void handleAfterUpdate(List<User> usersNew,List<User> usersOld){

    }
    
	/*** HCM Project : Keep ManagerID in Manager_Employee_ID__c for trigger IDMUserBatch***/    
    public static void updateUserByIDM(List<User> usersNew){
        List<String> managerList = new List<String>();
        List<User> userList = new List<User>();
        //run as sfadm
        if(UserInfo.getUserId() == sfadmId){
            system.debug('## update by sfadm user');        
            for(User user : usersNew){
                //get temp manager id == null 
                if(user.Manager_Employee_ID__c == null && user.ManagerId != null){
                    userList.add(user);
                    managerList.add(user.ManagerId);
                }
            }
            //find manager employee Id 
            List<User> managerUser = new List<User>([SELECT Id,Employee_ID__c FROM User WHERE Id IN: managerList]);
            for(User user : userList){
                for(User manager : managerUser){
                    //update Manager_Employee_ID__c
                    user.Manager_Employee_ID__c = manager.Employee_ID__c;
                    system.debug('## user.Manager_Employee_ID__c : '+user.Manager_Employee_ID__c);
                }
            }
        }
        else{
            system.debug('## update by other user');
        }
        
    }
    
    
    /*** CANCLE AS OF 10/03/2017 : BACK TO IDM RECONCILE SOLUTION***/
    /*** HCM Project for rehire case : put unix timestamp to these fields : UserName , Nickname ***/
    /*public static void updateUserforInactive(List<User> usersNew,Map<Id,User> usersOldMap){
        List<User> userToInactive = new List<User>();
		String unixTime = String.valueOf(System.now().getTime()/1000);
        system.debug('## unixTime : '+unixTime);
        //run as sfadm
        if(UserInfo.getUserId() == sfadmId){
            for(User user : usersNew){
                //check inactive user
                if(user.IsActive == false && ((usersOldMap.get(user.Id).CreatedDate != usersOldMap.get(user.Id).LastModifiedDate)) 
                   											&& usersOldMap.get(user.Id).Manager_Employee_ID__c == null){
                    user.Username += unixTime; //username with unix timestamp
                    user.CommunityNickname = user.Alias + unixTime; //replace with unix timestamp
                    user.Employee_ID__c = '';
                    user.FederationIdentifier = '';
                }
            }
        }
    }*/
    
}