/*
     Create By : Yao Jun
     Email : jyao@salesforce.com
     Create Date : 2016-08-11 
     
     This trigger is used to create/update Retail Contact for the batch interfaces of Update New/Existing Customers.
*/
trigger RTL_AccountTrigger on Account (before insert,before update,after insert, after update) {
    
    System.debug('START RTL ACCOUNT TRIGGER');
    if(AppConfig__c.getValues('runtrigger').Value__c == 'true') {
        Id acctRT = Schema.Sobjecttype.Account.getRecordTypeInfosByName().get('Retail Customer').getRecordTypeId();
        Id contactRT = Schema.Sobjecttype.Contact.getRecordTypeInfosByName().get('Retail Contact').getRecordTypeId();
        List<Account> newRetailCust = new List<Account>();
        List<Contact> uContacts = new List<Contact>{};
        List<Contact> nContacts = new List<Contact>{};
        Map<Id,Account> uAccounts = new Map<Id,Account>{};
        Map<Id,Account> newAccountMap = new Map<Id,Account>{};
        for(Account acct : trigger.new) {
            // Only process Retail Customer
            if(acct.RecordTypeId == acctRT) {
                if(trigger.isBefore && trigger.isInsert){
                    //newRetailCust.add(acct);
                }else if(trigger.isBefore && trigger.isUpdate){
                    newRetailCust.add(acct);
                }
                if(trigger.isAfter && trigger.isInsert) {                  
                    Contact contact = new Contact();
                    contact.AccountId = acct.Id;
                    contact.RecordTypeId = contactRT;
                    contact.TMB_Customer_ID__c = acct.TMB_Customer_ID_PE__c;                 
                    contact.Email = acct.Email_Address_PE__c;
                    contact.MobilePhone = acct.Mobile_Number_PE__c;
                    contact.LastName = (acct.Last_name_PE__c != null? acct.Last_name_PE__c : acct.First_name_PE__c);
                    nContacts.add(contact);
                    newRetailCust.add(acct);
                }
                else if(trigger.isAfter && trigger.IsUpdate) {
                    Account nAcct = trigger.newMap.get(acct.Id);
                    Account oAcct = trigger.oldMap.get(acct.Id);
                    boolean updateContact = nAcct.Email_Address_PE__c != oAcct.Email_Address_PE__c || nAcct.First_name_PE__c != oAcct.First_name_PE__c || nAcct.Last_name_PE__c != oAcct.Last_name_PE__c || nAcct.Mobile_Number_PE__c != oAcct.Mobile_Number_PE__c;
                    if(updateContact) {
                        uAccounts.put(nAcct.Id, nAcct);
                    }
                }
                
            }
            if(trigger.isAfter && trigger.isUpdate){
                Account nAcct = trigger.newMap.get(acct.Id);
                Account oAcct = trigger.oldMap.get(acct.Id);
                boolean rmUpdated = nAcct.RTL_RM_Name__c != oAcct.RTL_RM_Name__c;
                if(rmUpdated){
                    newAccountMap.put(acct.id,nAcct);
                }
                
            }
            
        }  // end for
        
        //Update Household RM Manager 
        if(newAccountMap.size() > 0){
            new RTL_HouseholdRelationshipManager().updateHouseholdRM(newAccountMap);
        }
        
        if(nContacts.size() > 0)
            insert nContacts;       
        
        if(uAccounts.size() > 0){
            uContacts = [select Id, AccountId, Email, MobilePhone, LastName from Contact where AccountId in :uAccounts.keySet()];
            for(Contact contact: uContacts) {
                Account account = uAccounts.get(contact.AccountId);
                contact.Email = account.Email_Address_PE__c;
                contact.MobilePhone = account.Mobile_Number_PE__c;
                contact.LastName = (account.Last_name_PE__c != null? account.Last_name_PE__c : account.First_name_PE__c);
            }
            update uContacts;
        }
        
        if(newRetailCust.size() > 0){
            RTL_DataQualityCheck dataQualityCheck = new RTL_DataQualityCheck();
            dataQualityCheck.start(newRetailCust);
        }
           
    }
    //---------------------
}