trigger RTL_CampaignMemberTrigger on CampaignMember (before insert,before update,after update,after insert ) {
    Boolean RunTrigger = AppConfig__c.getValues('runtrigger').Value__c == 'true' ;
    List<CampaignMember> newList = new List<CampaignMember>();
    List<CampaignMember> campaignMembToAssign = new List<CampaignMember>();
    Map<ID,CampaignMember> oldMap = new Map<Id,CampaignMember>();
    
    if(Trigger.New != null){
        for(CampaignMember campaignMemb : Trigger.New){
            if(RTL_Utility.getObjectRecordTypeIdsByDevNamePrefix(CampaignMember.SObjectType, 'Retail').contains(campaignMemb.RecordTypeId)){
                newList.add(campaignMemb);
                if(campaignMemb.RTL_TMB_Campaign_Source__c == 'Web' || campaignMemb.RTL_TMB_Campaign_Source__c == 'ATM Request'){
                    campaignMembToAssign.add(campaignMemb);
                }
            }
        }  
    }

    if(Trigger.Old !=null){
        for(CampaignMember campaignRecord: Trigger.Old){
            if(RTL_Utility.getObjectRecordTypeIdsByDevNamePrefix(CampaignMember.SObjectType, 'Retail').contains(campaignRecord.RecordTypeId)){
                oldMap.put(campaignRecord.id,campaignRecord);
            }
        } 
    }
    
    if(Trigger.isBefore && Trigger.isInsert){
        if( RunTrigger || Test.isRunningTest() ){
            RTL_CampaignMemberTriggerHandler.beforeInsert(campaignMembToAssign);
            RTL_CampaignStatusGenerator.RTL_UpdateCampaignMemberStatus(newList);
            RTL_CampaignMemberUtil.RTL_SaveProductsOffer(newList);
            RTL_CampaignMemberUtil.RTL_UpdateCampaignMemberAssigned(newList,oldMap);
            RTL_CampaignMemberUtil.RTL_UpdateMarkettingCode(newList);
            RTL_CampaignMemberUtil.RTL_UpdateCustomer(newList);
           
        }
    }

    if(Trigger.isBefore && Trigger.isUpdate){
        if( RunTrigger || Test.isRunningTest() ){
            RTL_CampaignMemberTriggerHandler.beforeUpdate(newList,oldMap);
            RTL_CampaignStatusGenerator.RTL_UpdateCampaignMemberStatus(newList);
            RTL_CampaignMemberUtil.RTL_SaveProductsOffer(newList);
            RTL_CampaignMemberUtil.RTL_UpdateCampaignMemberAssigned(newList,oldMap);
            
        }
        
    }

    if(Trigger.isAfter && Trigger.isUpdate){
        if( RunTrigger || Test.isRunningTest() ){
            RTL_CampaignMemberTriggerHandler.OnAfterUpdate(Trigger.New,trigger.oldmap);
        }
    }

    if(Trigger.isAfter && Trigger.isInsert){
        if( RunTrigger || Test.isRunningTest() ){
            RTL_CampaignMemberTriggerHandler.OnAfterInsert(newList);
        }
    }




}