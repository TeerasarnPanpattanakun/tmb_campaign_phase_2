trigger RTL_OpportunityTrigger on Opportunity (before insert, after insert,
                                               before update, after update, 
                                               after delete, after undelete) {
    new RTL_OpportunityTriggerHandler().run();
}