trigger RTL_ReferralTrigger on RTL_Referral__c (before insert,before update,after update,after insert) {
	Boolean RunTrigger = AppConfig__c.getValues('runtrigger').Value__c == 'true' ; 
    
    System.debug('is before '+ Trigger.isBefore);
    System.debug('is after '+ Trigger.isAfter);
    System.debug('is insert '+ Trigger.isInsert);
    System.debug('is update '+ Trigger.isUpdate);
    
    if (Trigger.isBefore && Trigger.isInsert)
    {
        system.debug('RTL_ReferralTrigger before insert');
        if( RunTrigger || Test.isRunningTest() )
        {
           RTL_ReferralTriggerHandler.handleBeforeInsert(Trigger.New);
        }
    }
    
    if (Trigger.isAfter && Trigger.isInsert)
    {
        system.debug('RTL_ReferralTrigger after insert');
        if( RunTrigger || Test.isRunningTest() )
        {
           RTL_ReferralTriggerHandler.handleAfterInsert(Trigger.New);
        }
    }
    
    if (Trigger.isBefore && Trigger.isUpdate)
    {
        system.debug('RTL_ReferralTrigger before update');
        if( RunTrigger || Test.isRunningTest() )
        {
           RTL_ReferralTriggerHandler.handleBeforeUpdate(Trigger.oldMap,Trigger.NewMap);
        }
    } 

    if (Trigger.isAfter && Trigger.isUpdate)
    {
        system.debug('RTL_ReferralTrigger after update');
        if ( RunTrigger || Test.isRunningTest() )
        {
            RTL_ReferralTriggerHandler.handleAfterUpdate(Trigger.oldMap,Trigger.NewMap);
        }
    }

    
}